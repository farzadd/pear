Pear - A student service centre by PearProgrammers  
  
Quick-Reference Sheet:  
  
================ Symfony Directory Structure ================  
app/        The application configuration, templates and translations.  
bin/        The framework's executables.  
nbproject/  The Netbeans Project meta data.  
sql/        All SQL scripts required to set-up a working database.  
src/        The project's PHP code. Main code lives here.  
vendor/     The third-party dependencies.  
web/        The web root directory used for images, CSS, etc. All files here will be accessible publicly.  
  
================ Reverse-Engineering Model MetaData ================  
$ php app/console doctrine:mapping:import --force AppBundle xml  
$ php app/console doctrine:mapping:convert annotation ./src  
$ php app/console doctrine:generate:entities AppBundle  
  
================ Generating Model MetaData ================  
$ php app/console doctrine:generate:entities AppBundle  
$ php app/console doctrine:schema:update --complete --force  
  
================ Checking User Permissions ================  
Function details are available in: src/AppBundle/Controller/PearController.php  
public function checkPermission($access)  
Example of use in: Controller/Admin/SchoolsController.php
  
public function hasPermission($access)  
Example of use in: Controller/Admin/SchoolsController.php + views/Admin/Schools/showSchool.html.twig
  
=================== Coding Conventions ====================  
4 spaces instead of Tabs  
Variable and functions names are camelCase : ex. isUserOnline, userLogs  
Controller names should be TitleCase : AdminController, StudentController  
Twig templates titles should be camelCase and should directly reference controller action: ex. showSchool.html.twig (showAction in SchoolsController)  
Twig template directory structure should mimic controller directory structure.  
No extra spaces after end of line: "if (isUserOnline) {   " <-- bad  
Open/close brackets should be on their own line  
Single line bodies should also have brackets  

Each twig template can only be rendered in one action.  
  
Docstring to proceed most functions:  
/****************************************************  
 Description of function  
 **\@param** name_of_param description_of_param  
**\@returns** return_type return_description  
 **\@throws** exception when_its_thrown  
 ****************************************************/

**Acceptance test scenarios:**  
*src/AppBundle/Features* - place your .feature files here  
*features/bootstrap/AppBundle/Features/Context* - place your context classes here  
*features/bootstrap/AppBundle/Features/Context/FeatureContext.php* - place your definitions, transformations and hooks here  
**Behat documentation**  
[**Click here**](http://docs.behat.org/en/v2.5/)