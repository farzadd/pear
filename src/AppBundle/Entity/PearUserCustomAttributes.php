<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PearCustomAttributes
 *
 * @ORM\Table(name="pear_user_custom_attributes")
 * @ORM\Entity
 */
class PearUserCustomAttributes
{
    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=64, nullable=true)
     */
    private $value;
        
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearUsers", inversedBy="customAttributes")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;
    
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearCustomAttributes", inversedBy="userCustomAttributes")
     * @ORM\JoinColumn(name="attribute_id", referencedColumnName="id", nullable=false)
     */
    private $customAttribute;

    /**
     * Constructor
     */
    public function __construct()
    {
        
    }
    
    /**
     * Set user
     *
     * @param \AppBundle\Entity\PearUsers $user
     * @return PearUserCustomAttributes
     */
    public function setUser(\AppBundle\Entity\PearUsers $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\PearUsers 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set customAttribute
     *
     * @param \AppBundle\Entity\PearCustomAttributes $customAttribute
     * @return PearUserCustomAttributes
     */
    public function setCustomAttribute(\AppBundle\Entity\PearCustomAttributes $customAttribute)
    {
        $this->customAttribute = $customAttribute;

        return $this;
    }

    /**
     * Get customAttribute
     *
     * @return \AppBundle\Entity\PearCustomAttributes 
     */
    public function getCustomAttribute()
    {
        return $this->customAttribute;
    }
    
    public function __toString()
    {
        try
        {
            return $this->customAttribute->getName();
        }
        catch (Exception $e) {}
        return null;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return PearUserCustomAttributes
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }
}
