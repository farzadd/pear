<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PearAttendance
 *
 * @ORM\Table(name="pear_attendance")
 * @ORM\Entity
 */
class PearAttendance
{
    /**
     * @var string
     *
     * @ORM\Column(name="date", type="string", length=25, nullable=false)
     */
    private $date;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearUsers", inversedBy="lateAttendance")
     * @ORM\JoinTable(name="pear_attendance_late")
     **/
    private $lateUsers;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearUsers", inversedBy="absentAttendance")
     * @ORM\JoinTable(name="pear_attendance_absent")
     **/
    private $absentUsers;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearUsers", inversedBy="presentAttendance")
     * @ORM\JoinTable(name="pear_attendance_present")
     **/
    private $presentUsers;
    
     /**
      * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearClassSections", inversedBy="attendance")
      * @ORM\JoinColumn(name="section_id", referencedColumnName="id", nullable=false)
     */
    private $section;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        
    }

    /**
     * Set section
     *
     * @param string $section
     * @return PearUserAttendance
     */
    public function setSection($section)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return string 
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set date
     *
     * @param \string $date
     * @return PearUserAttendance
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \string 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set lateUsers
     *
     * @param \AppBundle\Entity\PearUsers $lateUsers
     * @return PearAttendance
     */
    public function setLateUsers(\AppBundle\Entity\PearUsers $lateUsers)
    {
        $this->lateUsers = $lateUsers;

        return $this;
    }

    /**
     * Get lateUsers
     *
     * @return \AppBundle\Entity\PearUsers 
     */
    public function getLateUsers()
    {
        return $this->lateUsers;
    }

    /**
     * Set absentUsers
     *
     * @param \AppBundle\Entity\PearUsers $absentUsers
     * @return PearAttendance
     */
    public function setAbsentUsers(\AppBundle\Entity\PearUsers $absentUsers)
    {
        $this->absentUsers = $absentUsers;

        return $this;
    }

    /**
     * Get absentUsers
     *
     * @return \AppBundle\Entity\PearUsers 
     */
    public function getAbsentUsers()
    {
        return $this->absentUsers;
    }
    /**
     * @var boolean
     */
    private $late;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add lateUsers
     *
     * @param \AppBundle\Entity\PearUsers $lateUsers
     * @return PearAttendance
     */
    public function addLateUser(\AppBundle\Entity\PearUsers $lateUsers)
    {
        $this->lateUsers[] = $lateUsers;

        return $this;
    }

    /**
     * Remove lateUsers
     *
     * @param \AppBundle\Entity\PearUsers $lateUsers
     */
    public function removeLateUser(\AppBundle\Entity\PearUsers $lateUsers)
    {
        $this->lateUsers->removeElement($lateUsers);
    }

    /**
     * Add absentUsers
     *
     * @param \AppBundle\Entity\PearUsers $absentUsers
     * @return PearAttendance
     */
    public function addAbsentUser(\AppBundle\Entity\PearUsers $absentUsers)
    {
        $this->absentUsers[] = $absentUsers;

        return $this;
    }

    /**
     * Remove absentUsers
     *
     * @param \AppBundle\Entity\PearUsers $absentUsers
     */
    public function removeAbsentUser(\AppBundle\Entity\PearUsers $absentUsers)
    {
        $this->absentUsers->removeElement($absentUsers);
    }

    /**
     * Add presentUsers
     *
     * @param \AppBundle\Entity\PearUsers $presentUsers
     * @return PearAttendance
     */
    public function addPresentUser(\AppBundle\Entity\PearUsers $presentUsers)
    {
        $this->presentUsers[] = $presentUsers;

        return $this;
    }

    /**
     * Remove presentUsers
     *
     * @param \AppBundle\Entity\PearUsers $presentUsers
     */
    public function removePresentUser(\AppBundle\Entity\PearUsers $presentUsers)
    {
        $this->presentUsers->removeElement($presentUsers);
    }

    /**
     * Get presentUsers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPresentUsers()
    {
        return $this->presentUsers;
    }
}
