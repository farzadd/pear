<?php

namespace AppBundle\Entity;;

use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="pear_roles")
 * @ORM\Entity()
 */
class PearRoles implements RoleInterface
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=30)
     */
    private $name;

    /**
     * @ORM\Column(name="role", type="string", length=20, unique=true)
     */
    private $role;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearPermissions", mappedBy="role")
     **/
    private $permissions;
    
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearAnnouncements", mappedBy="roles")
     **/
    private $announcements;
    
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearCustomAttributes", mappedBy="roles")
     **/
    private $customAttributes;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->customAttributes = new ArrayCollection();
    }

    /**
     * @see RoleInterface
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PearRoles
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return PearRoles
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Add users
     *
     * @param \AppBundle\Entity\PearUsers $users
     * @return PearRoles
     */
    public function addUser(\AppBundle\Entity\PearUsers $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \AppBundle\Entity\PearUsers $users
     */
    public function removeUser(\AppBundle\Entity\PearUsers $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add customAttributes
     *
     * @param \AppBundle\Entity\PearCustomAttributes $customAttributes
     * @return PearRoles
     */
    public function addCustomAttribute(\AppBundle\Entity\PearCustomAttributes $customAttributes)
    {
        $this->customAttributes[] = $customAttributes;

        return $this;
    }

    /**
     * Remove customAttributes
     *
     * @param \AppBundle\Entity\PearCustomAttributes $customAttributes
     */
    public function removeCustomAttribute(\AppBundle\Entity\PearCustomAttributes $customAttributes)
    {
        $this->customAttributes->removeElement($customAttributes);
    }

    /**
     * Get customAttributes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCustomAttributes()
    {
        return $this->customAttributes;
    }

    /**
     * Add permissions
     *
     * @param \AppBundle\Entity\PearPermissions $permissions
     * @return PearRoles
     */
    public function addPermission(\AppBundle\Entity\PearPermissions $permissions)
    {
        $this->permissions[] = $permissions;

        return $this;
    }

    /**
     * Remove permissions
     *
     * @param \AppBundle\Entity\PearPermissions $permissions
     */
    public function removePermission(\AppBundle\Entity\PearPermissions $permissions)
    {
        $this->permissions->removeElement($permissions);
    }

    /**
     * Get permissions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * Add announcements
     *
     * @param \AppBundle\Entity\PearAnnouncements $announcements
     * @return PearRoles
     */
    public function addAnnouncement(\AppBundle\Entity\PearAnnouncements $announcements)
    {
        $this->announcements[] = $announcements;

        return $this;
    }

    /**
     * Remove announcements
     *
     * @param \AppBundle\Entity\PearAnnouncements $announcements
     */
    public function removeAnnouncement(\AppBundle\Entity\PearAnnouncements $announcements)
    {
        $this->announcements->removeElement($announcements);
    }

    /**
     * Get announcements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAnnouncements()
    {
        return $this->announcements;
    }
}
