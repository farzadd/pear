<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PearPayments
 *
 * @ORM\Table(name="pear_payments")
 * @ORM\Entity
 */
class PearPayments
{
    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=8, scale=2, nullable=false)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="receipt_id", type="string", length=24, nullable=true)
     */
    private $receiptId;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_type", type="string", length=24, nullable=true)
     */
    private $paymentType;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=64, nullable=true)
     */
    private $note;

    /**
     * @var datetime
     *
     * @ORM\Column(type="datetime", name="created", nullable=false)
     */
    private $created;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     * 
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearUsers", inversedBy="payments")
     * @ORM\JoinTable(name="pear_payments_users")
     */
    private $users;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->created = new \DateTime();
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return PearPayments
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set receiptId
     *
     * @param string $receiptId
     * @return PearPayments
     */
    public function setReceiptId($receiptId)
    {
        $this->receiptId = $receiptId;

        return $this;
    }

    /**
     * Get receiptId
     *
     * @return string 
     */
    public function getReceiptId()
    {
        return $this->receiptId;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return PearPayments
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return PearPayments
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add users
     *
     * @param \AppBundle\Entity\PearUsers $users
     * @return PearPayments
     */
    public function addUser(\AppBundle\Entity\PearUsers $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \AppBundle\Entity\PearUsers $users
     */
    public function removeUser(\AppBundle\Entity\PearUsers $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set paymentType
     *
     * @param string $paymentType
     * @return PearPayments
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * Get paymentType
     *
     * @return string 
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }
}
