<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PearClassAssignments
 *
 * @ORM\Table(name="pear_class_assignments")
 * @ORM\Entity
 */
class PearClassAssignments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=256, nullable=false)
     */
    private $description;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer", nullable=false)
     */
    private $weight;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="created", type="integer", nullable=false)
     */
    private $created;
    
    /**
      * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearUsers", inversedBy="assignmentsCreated")
      * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $createdBy;
    
     /**
      * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearClasses", inversedBy="assignments")
      * @ORM\JoinColumn(name="class_id", referencedColumnName="id", nullable=false)
     */
    private $class;
    
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearClassSections", mappedBy="assignments")
     **/
    private $sections;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearUserGrades", mappedBy="assignment")
     **/
    private $grades;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearDocuments", mappedBy="assignment")
     */
    private $documents;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sections = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PearClassAssignments
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return PearClassAssignments
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     * @return PearClassAssignments
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set created
     *
     * @param integer $created
     * @return PearClassAssignments
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return integer 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\PearUsers $createdBy
     * @return PearClassAssignments
     */
    public function setCreatedBy(\AppBundle\Entity\PearUsers $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\PearUsers 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set class
     *
     * @param \AppBundle\Entity\PearClasses $class
     * @return PearClassAssignments
     */
    public function setClass(\AppBundle\Entity\PearClasses $class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class
     *
     * @return \AppBundle\Entity\PearClasses 
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Add sections
     *
     * @param \AppBundle\Entity\PearClassSections $sections
     * @return PearClassAssignments
     */
    public function addSection(\AppBundle\Entity\PearClassSections $sections)
    {
        $this->sections[] = $sections;

        return $this;
    }

    /**
     * Remove sections
     *
     * @param \AppBundle\Entity\PearClassSections $sections
     */
    public function removeSection(\AppBundle\Entity\PearClassSections $sections)
    {
        $this->sections->removeElement($sections);
    }

    /**
     * Get sections
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * Add grades
     *
     * @param \AppBundle\Entity\PearUserGrades $grades
     * @return PearClassAssignments
     */
    public function addGrade(\AppBundle\Entity\PearUserGrades $grades)
    {
        $this->grades[] = $grades;

        return $this;
    }

    /**
     * Remove grades
     *
     * @param \AppBundle\Entity\PearUserGrades $grades
     */
    public function removeGrade(\AppBundle\Entity\PearUserGrades $grades)
    {
        $this->grades->removeElement($grades);
    }

    /**
     * Get grades
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGrades()
    {
        return $this->grades;
    }

    /**
     * Add documents
     *
     * @param \AppBundle\Entity\PearDocuments $documents
     * @return PearClassAssignments
     */
    public function addDocument(\AppBundle\Entity\PearDocuments $documents)
    {
        $this->documents[] = $documents;

        return $this;
    }

    /**
     * Remove documents
     *
     * @param \AppBundle\Entity\PearDocuments $documents
     */
    public function removeDocument(\AppBundle\Entity\PearDocuments $documents)
    {
        $this->documents->removeElement($documents);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocuments()
    {
        return $this->documents;
    }
}
