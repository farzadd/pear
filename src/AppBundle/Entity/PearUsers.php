<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * PearUsers
 *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PearUserRepository")
 * @ORM\Table(name="pear_users")
 */
class PearUsers implements AdvancedUserInterface, \Serializable
{
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=45, unique=true, nullable=true)
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password_hash", type="string", length=129, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="password_salt", type="string", length=22, nullable=true)
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=25, nullable=false)
     */
    private $firstName;
    
    /**
     * @var string
     *
     * @ORM\Column(name="middle_name", type="string", length=25, nullable=true)
     */
    private $middleName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=25, nullable=false)
     */
    private $lastName;

    /**
     * @var integer
     *
     * @ORM\Column(name="gender", type="smallint", nullable=false)
     */
    private $gender;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_date", type="date", nullable=false)
     */
    private $birthDate;

    /**
     * @var string
     *
     * @ORM\Column(name="home_address", type="string", length=45, nullable=true)
     */
    private $homeAddress;
    
    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=21, nullable=true)
     */
    private $city;
    
    /**
     * @var string
     *
     * @ORM\Column(name="parent_name", type="string", length=30, nullable=true)
     */
    private $parentName;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_number", type="string", length=18, nullable=true)
     */
    private $parentNumber;
    
    /**
     * @var string
     *
     * @ORM\Column(name="parent_email", type="string", length=45, nullable=true)
     */
    private $parentEmail;
    
    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=6, nullable=true)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=18, nullable=true)
     */
    private $phone;
    
    /**
     * @var string
     *
     * @ORM\Column(name="allergies", type="string", length=120, nullable=true)
     */
    private $allergies;
    
    /**
     * @var string
     *
     * @ORM\Column(name="tshirt_size", type="string", length=4, nullable=true)
     */
    private $tshirtSize;

    /**
     * @var string
     *
     * @ORM\Column(name="emergency_name", type="string", length=30, nullable=true)
     */
    private $emergencyName;

    /**
     * @var string
     *
     * @ORM\Column(name="emergency_contact", type="string", length=18, nullable=true)
     */
    private $emergencyContact;

    /**
     * @var boolean
     *
     * @ORM\Column(name="email_verfied", type="boolean", nullable=false)
     */
    private $emailVerfied;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * 
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearRoles")
     * @ORM\JoinTable(name="pear_user_roles")
     */
    private $roles;

    /**
     * @var string
     *
     * @ORM\Column(name="locked_ip", type="string", length=45, nullable=true)
     */
    private $lockedIp;

    /**
     * @var string
     *
     * @ORM\Column(name="last_ip", type="string", length=45, nullable=true)
     */
    private $lastIp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="locked", type="boolean", nullable=false)
     */
    private $locked;

    /**
     * @var integer
     *
     * @ORM\Column(name="failed_attempts", type="smallint", nullable=false)
     */
    private $failedAttempts;

    /**
     * @var integer
     *
     * @ORM\Column(name="created", type="integer", nullable=false)
     */
    private $created;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_login", type="integer", nullable=false)
     */
    private $lastLogin;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearClassSections", mappedBy="teacher")
     **/
    private $teaches;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearClassSections", mappedBy="teachingAssistant")
     **/
    private $teachingAssists;
    
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearClassSections", inversedBy="students")
     * @ORM\JoinTable(name="pear_user_enrolls")
     **/
    private $enrolls;
    
    /**
      * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearCampuses", inversedBy="users")
      * @ORM\JoinColumn(name="campus_id", referencedColumnName="id", nullable=false)
     */
    private $campus;
    
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearClasses", inversedBy="user_prereqs")
     * @ORM\JoinTable(name="pear_user_prereqs")
     **/
    private $prereqs;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearUserGrades", mappedBy="user")
     **/
    private $grades;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearClassAssignments", mappedBy="createdBy")
     **/
    private $assignmentsCreated;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearPrereqExemptionRequests", mappedBy="user")
     **/
    private $prereqExemptionRequests;
    
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearAttendance", mappedBy="lateUsers")
     **/
    private $lateAttendance;
    
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearAttendance", mappedBy="absentUsers")
     **/
    private $absentAttendance;
    
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearAttendance", mappedBy="presentUsers")
     **/
        private $presentAttendance;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearUserCustomAttributes", mappedBy="user")
     **/
    private $customAttributes;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearPayments", mappedBy="users")
     **/
    private $payments;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="lastAnnouncementSeen", type="smallint", nullable=true)
     */
    private $lastAnnouncementSeen = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\File(maxSize="6000000")
    */
    private $file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->teaches = new ArrayCollection();
        $this->enrolls = new ArrayCollection();
        $this->grades = new ArrayCollection();
        $this->attendance = new ArrayCollection();
        $this->campuses = new ArrayCollection();
        $this->emailVerfied = false;
        $this->locked = false;
        $this->failedAttempts = 0;
        $this->salt = md5(uniqid("p_", true) . time());
        $this->payments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        $roles = $this->roles->toArray();
        return $roles;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
            $this->salt,
            $this->locked,
            $this->emailVerfied,
            ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            $this->salt,
            $this->locked,
            $this->emailVerfied
            ) = unserialize($serialized);
    }
    
    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        //return !$this->locked;
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        //return $this->emailVerfied;
        return true;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return PearUsers
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }
    
    /**
     * Set firstName
     *
     * @param string $firstName
     * @return PearUsers
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return PearUsers
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set gender
     *
     * @param integer $gender
     * @return PearUsers
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return integer 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     * @return PearUsers
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime 
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set homeAddress
     *
     * @param string $homeAddress
     * @return PearUsers
     */
    public function setHomeAddress($homeAddress)
    {
        $this->homeAddress = $homeAddress;

        return $this;
    }

    /**
     * Get homeAddress
     *
     * @return string 
     */
    public function getHomeAddress()
    {
        return $this->homeAddress;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return PearUsers
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set emergencyName
     *
     * @param string $emergencyName
     * @return PearUsers
     */
    public function setEmergencyName($emergencyName)
    {
        $this->emergencyName = $emergencyName;

        return $this;
    }

    /**
     * Get emergencyName
     *
     * @return string 
     */
    public function getEmergencyName()
    {
        return $this->emergencyName;
    }

    /**
     * Set emergencyContact
     *
     * @param string $emergencyContact
     * @return PearUsers
     */
    public function setEmergencyContact($emergencyContact)
    {
        $this->emergencyContact = $emergencyContact;

        return $this;
    }

    /**
     * Get emergencyContact
     *
     * @return string 
     */
    public function getEmergencyContact()
    {
        return $this->emergencyContact;
    }

    /**
     * Set emailVerfied
     *
     * @param boolean $emailVerfied
     * @return PearUsers
     */
    public function setEmailVerfied($emailVerfied)
    {
        $this->emailVerfied = $emailVerfied;

        return $this;
    }

    /**
     * Get emailVerfied
     *
     * @return boolean 
     */
    public function getEmailVerfied()
    {
        return $this->emailVerfied;
    }

    /**
     * Set lockedIp
     *
     * @param string $lockedIp
     * @return PearUsers
     */
    public function setLockedIp($lockedIp)
    {
        $this->lockedIp = $lockedIp;

        return $this;
    }

    /**
     * Get lockedIp
     *
     * @return string 
     */
    public function getLockedIp()
    {
        return $this->lockedIp;
    }

    /**
     * Set lastIp
     *
     * @param string $lastIp
     * @return PearUsers
     */
    public function setLastIp($lastIp)
    {
        $this->lastIp = $lastIp;

        return $this;
    }

    /**
     * Get lastIp
     *
     * @return string 
     */
    public function getLastIp()
    {
        return $this->lastIp;
    }

    /**
     * Set locked
     *
     * @param boolean $locked
     * @return PearUsers
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean 
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Set failedAttempts
     *
     * @param integer $failedAttempts
     * @return PearUsers
     */
    public function setFailedAttempts($failedAttempts)
    {
        $this->failedAttempts = $failedAttempts;

        return $this;
    }

    /**
     * Get failedAttempts
     *
     * @return integer 
     */
    public function getFailedAttempts()
    {
        return $this->failedAttempts;
    }

    /**
     * Set created
     *
     * @param integer $created
     * @return PearUsers
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return integer 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set lastLogin
     *
     * @param integer $lastLogin
     * @return PearUsers
     */
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * Get lastLogin
     *
     * @return integer 
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return PearUsers
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return PearUsers
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Add roles
     *
     * @param \AppBundle\Entity\PearRoles $roles
     * @return PearUsers
     */
    public function addRole(\AppBundle\Entity\PearRoles $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \AppBundle\Entity\PearRoles $roles
     */
    public function removeRole(\AppBundle\Entity\PearRoles $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return PearUsers
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Add teaches
     *
     * @param \AppBundle\Entity\PearClassSections $teaches
     * @return PearUsers
     */
    public function addTeach(\AppBundle\Entity\PearClassSections $teaches)
    {
        $this->teaches[] = $teaches;

        return $this;
    }

    /**
     * Remove teaches
     *
     * @param \AppBundle\Entity\PearClassSections $teaches
     */
    public function removeTeach(\AppBundle\Entity\PearClassSections $teaches)
    {
        $this->teaches->removeElement($teaches);
    }

    /**
     * Get teaches
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTeaches()
    {
        return $this->teaches;
    }

    /**
     * Add enrolls
     *
     * @param \AppBundle\Entity\PearClassSections $enrolls
     * @return PearUsers
     */
    public function addEnroll(\AppBundle\Entity\PearClassSections $enrolls)
    {
        $this->enrolls[] = $enrolls;

        return $this;
    }

    /**
     * Remove enrolls
     *
     * @param \AppBundle\Entity\PearClassSections $enrolls
     */
    public function removeEnroll(\AppBundle\Entity\PearClassSections $enrolls)
    {
        $this->enrolls->removeElement($enrolls);
    }

    /**
     * Get enrolls
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEnrolls()
    {
        return $this->enrolls;
    }

    /**
     * Add grades
     *
     * @param \AppBundle\Entity\PearUserGrades $grades
     * @return PearUsers
     */
    public function addGrade(\AppBundle\Entity\PearUserGrades $grades)
    {
        $this->grades[] = $grades;

        return $this;
    }

    /**
     * Remove grades
     *
     * @param \AppBundle\Entity\PearUserGrades $grades
     */
    public function removeGrade(\AppBundle\Entity\PearUserGrades $grades)
    {
        $this->grades->removeElement($grades);
    }

    /**
     * Get grades
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGrades()
    {
        return $this->grades;
    }

    /**
     * Add attendance
     *
     * @param \AppBundle\Entity\PearAttendance $attendance
     * @return PearUsers
     */
    public function addAttendance(\AppBundle\Entity\PearAttendance $attendance)
    {
        $this->attendance[] = $attendance;

        return $this;
    }

    /**
     * Remove attendance
     *
     * @param \AppBundle\Entity\PearAttendance $attendance
     */
    public function removeAttendance(\AppBundle\Entity\PearAttendance $attendance)
    {
        $this->attendance->removeElement($attendance);
    }

    /**
     * Get attendance
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttendance()
    {
        return $this->attendance;
    }

    /**
     * Add prereqs
     *
     * @param \AppBundle\Entity\PearClasses $prereqs
     * @return PearUsers
     */
    public function addPrereq(\AppBundle\Entity\PearClasses $prereqs)
    {
        $this->prereqs[] = $prereqs;

        return $this;
    }

    /**
     * Remove prereqs
     *
     * @param \AppBundle\Entity\PearClasses $prereqs
     */
    public function removePrereq(\AppBundle\Entity\PearClasses $prereqs)
    {
        $this->prereqs->removeElement($prereqs);
    }

    /**
     * Get prereqs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrereqs()
    {
        return $this->prereqs;
    }

    /**
     * Add customAttributes
     *
     * @param \AppBundle\Entity\PearUserCustomAttributes $customAttributes
     * @return PearUsers
     */
    public function addCustomAttribute(\AppBundle\Entity\PearUserCustomAttributes $customAttributes)
    {
        $this->customAttributes[] = $customAttributes;

        return $this;
    }

    /**
     * Remove customAttributes
     *
     * @param \AppBundle\Entity\PearUserCustomAttributes $customAttributes
     */
    public function removeCustomAttribute(\AppBundle\Entity\PearUserCustomAttributes $customAttributes)
    {
        $this->customAttributes->removeElement($customAttributes);
    }

    /**
     * Get customAttributes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCustomAttributes()
    {
        return $this->customAttributes;
    }

    /**
     * Add lateAttendance
     *
     * @param \AppBundle\Entity\PearAttendance $lateAttendance
     * @return PearUsers
     */
    public function addLateAttendance(\AppBundle\Entity\PearAttendance $lateAttendance)
    {
        $this->lateAttendance[] = $lateAttendance;

        return $this;
    }

    /**
     * Remove lateAttendance
     *
     * @param \AppBundle\Entity\PearAttendance $lateAttendance
     */
    public function removeLateAttendance(\AppBundle\Entity\PearAttendance $lateAttendance)
    {
        $this->lateAttendance->removeElement($lateAttendance);
    }

    /**
     * Get lateAttendance
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLateAttendance()
    {
        return $this->lateAttendance;
    }

    /**
     * Add absentAttendance
     *
     * @param \AppBundle\Entity\PearAttendance $absentAttendance
     * @return PearUsers
     */
    public function addAbsentAttendance(\AppBundle\Entity\PearAttendance $absentAttendance)
    {
        $this->absentAttendance[] = $absentAttendance;

        return $this;
    }

    /**
     * Remove absentAttendance
     *
     * @param \AppBundle\Entity\PearAttendance $absentAttendance
     */
    public function removeAbsentAttendance(\AppBundle\Entity\PearAttendance $absentAttendance)
    {
        $this->absentAttendance->removeElement($absentAttendance);
    }

    /**
     * Get absentAttendance
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAbsentAttendance()
    {
        return $this->absentAttendance;
    }

    /**
     * Add prereqExemptionRequests
     *
     * @param \AppBundle\Entity\PearPrereqExemptionRequests $prereqExemptionRequests
     * @return PearUsers
     */
    public function addPrereqExemptionRequest(\AppBundle\Entity\PearPrereqExemptionRequests $prereqExemptionRequests)
    {
        $this->prereqExemptionRequests[] = $prereqExemptionRequests;

        return $this;
    }

    /**
     * Remove prereqExemptionRequests
     *
     * @param \AppBundle\Entity\PearPrereqExemptionRequests $prereqExemptionRequests
     */
    public function removePrereqExemptionRequest(\AppBundle\Entity\PearPrereqExemptionRequests $prereqExemptionRequests)
    {
        $this->prereqExemptionRequests->removeElement($prereqExemptionRequests);
    }

    /**
     * Get prereqExemptionRequests
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrereqExemptionRequests()
    {
        return $this->prereqExemptionRequests;
    }
    
    /**
     * Add presentAttendance
     *
     * @param \AppBundle\Entity\PearAttendance $presentAttendance
     * @return PearUsers
     */
    public function addPresentAttendance(\AppBundle\Entity\PearAttendance $presentAttendance)
    {
        $this->presentAttendance[] = $presentAttendance;
    }

    /**
     * Remove presentAttendance
     *
     * @param \AppBundle\Entity\PearAttendance $presentAttendance
     */
    public function removePresentAttendance(\AppBundle\Entity\PearAttendance $presentAttendance)
    {
        $this->presentAttendance->removeElement($presentAttendance);
    }
    
    /**
     * Get presentAttendance
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPresentAttendance()
    {
        return $this->presentAttendance;
    }


    /**
     * Add assignmentsCreated
     *
     * @param \AppBundle\Entity\PearClassAssignments $assignmentsCreated
     * @return PearUsers
     */
    public function addAssignmentsCreated(\AppBundle\Entity\PearClassAssignments $assignmentsCreated)
    {
        $this->assignmentsCreated[] = $assignmentsCreated;

        return $this;
    }

    /**
     * Remove assignmentsCreated
     *
     * @param \AppBundle\Entity\PearClassAssignments $assignmentsCreated
     */
    public function removeAssignmentsCreated(\AppBundle\Entity\PearClassAssignments $assignmentsCreated)
    {
        $this->assignmentsCreated->removeElement($assignmentsCreated);
    }

    /**
     * Get assignmentsCreated
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAssignmentsCreated()
    {
        return $this->assignmentsCreated;
    }

    /**
     * Add payments
     *
     * @param \AppBundle\Entity\PearPayments $payments
     * @return PearUsers
     */
    public function addPayment(\AppBundle\Entity\PearPayments $payments)
    {
        $this->payments->add($payments);

        return $this;
    }

    /**
     * Remove payments
     *
     * @param \AppBundle\Entity\PearPayments $payments
     */
    public function removePayment(\AppBundle\Entity\PearPayments $payments)
    {
        $this->payments->removeElement($payments);
    }

    /**
     * Get payments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPayments()
    {
        return $this->payments;
    }
    
    public function __toString()
    {
        try
        {
            return $this->firstName .  ' ' . $this->lastName;
        }
        catch (Exception $e) {}
        return null;
    }

    /**
     * Set campus
     *
     * @param \AppBundle\Entity\PearCampuses $campus
     * @return PearUsers
     */
    public function setCampus(\AppBundle\Entity\PearCampuses $campus)
    {
        $this->campus = $campus;

        return $this;
    }

    /**
     * Get campus
     *
     * @return \AppBundle\Entity\PearCampuses 
     */
    public function getCampus()
    {
        return $this->campus;
    }

    /**
     * Set middleName
     *
     * @param string $middleName
     * @return PearUsers
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middleName
     *
     * @return string 
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return PearUsers
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set parentName
     *
     * @param string $parentName
     * @return PearUsers
     */
    public function setParentName($parentName)
    {
        $this->parentName = $parentName;

        return $this;
    }

    /**
     * Get parentName
     *
     * @return string 
     */
    public function getParentName()
    {
        return $this->parentName;
    }

    /**
     * Set parentNumber
     *
     * @param string $parentNumber
     * @return PearUsers
     */
    public function setParentNumber($parentNumber)
    {
        $this->parentNumber = $parentNumber;

        return $this;
    }

    /**
     * Get parentNumber
     *
     * @return string 
     */
    public function getParentNumber()
    {
        return $this->parentNumber;
    }

    /**
     * Set parentEmail
     *
     * @param string $parentEmail
     * @return PearUsers
     */
    public function setParentEmail($parentEmail)
    {
        $this->parentEmail = $parentEmail;

        return $this;
    }

    /**
     * Get parentEmail
     *
     * @return string 
     */
    public function getParentEmail()
    {
        return $this->parentEmail;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     * @return PearUsers
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string 
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set allergies
     *
     * @param string $allergies
     * @return PearUsers
     */
    public function setAllergies($allergies)
    {
        $this->allergies = $allergies;

        return $this;
    }

    /**
     * Get allergies
     *
     * @return string 
     */
    public function getAllergies()
    {
        return $this->allergies;
    }

    /**
     * Set tshirtSize
     *
     * @param string $tshirtSize
     * @return PearUsers
     */
    public function setTshirtSize($tshirtSize)
    {
        $this->tshirtSize = $tshirtSize;

        return $this;
    }

    /**
     * Get tshirtSize
     *
     * @return string 
     */
    public function getTshirtSize()
    {
        return $this->tshirtSize;
    }

    /**
     * Add teachingAssists
     *
     * @param \AppBundle\Entity\PearClassSections $teachingAssists
     * @return PearUsers
     */
    public function addTeachingAssist(\AppBundle\Entity\PearClassSections $teachingAssists)
    {
        $this->teachingAssists[] = $teachingAssists;
        return $this;
    }
    
    /**
     * Set path
     *
     */
    public function setPath($path)
    {
        $this->path = $path;
    }
    
    /*
     * Get path
     * 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Remove teachingAssists
     *
     * @param \AppBundle\Entity\PearClassSections $teachingAssists
     */
    public function removeTeachingAssist(\AppBundle\Entity\PearClassSections $teachingAssists)
    {
        $this->teachingAssists->removeElement($teachingAssists);
    }

    /**
     * Get teachingAssists
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTeachingAssists()
    {
        return $this->teachingAssists;
    }
    

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }
        
        $extension = explode('.',$this->getFile()->getClientOriginalName());
        $fileName = $this->id . '.' . end($extension);
        
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $fileName
        );
        
        // set the path property to the filename where you've saved the file
        $this->path = $fileName;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }


    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('file', new Assert\File(array(
            'maxSize' => 6000000,
            )));
    }


    public function getAbsolutePath()
    {
        return null === $this->path
        ? null
        : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path
        ? null
        : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'studentphotos';
    }

    public function remove()
    {
        if ($this->getFile() === null) {
            return;
        }
        $this->getFile()->unlink($this->getUploadRootDir(), $this->path );
        $this->file = null;
    }

    /**
     * Set lastAnnouncementSeen
     *
     * @param integer $lastAnnouncementSeen
     * @return PearUsers
     */
    public function setLastAnnouncementSeen($lastAnnouncementSeen)
    {
        $this->lastAnnouncementSeen = $lastAnnouncementSeen;

        return $this;
    }

    /**
     * Get lastAnnouncementSeen
     *
     * @return integer 
     */
    public function getLastAnnouncementSeen()
    {
        return $this->lastAnnouncementSeen;
    }
}
