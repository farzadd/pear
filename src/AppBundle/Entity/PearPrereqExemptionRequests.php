<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PearPrereqExemptionRequests
 *
 * @ORM\Table(name="pear_prereq_exemption_requests")
 * @ORM\Entity
 */
class PearPrereqExemptionRequests
{    
    /**
     * @var string
     *
     * @ORM\Column(name="date_scheduled", type="string", length=256, nullable=true)
     */
    private $dateScheduled;
    
    /**
     * @var string
     *
     * @ORM\Column(name="test_location", type="string", length=256, nullable=true)
     */
    private $testLocation;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=256, nullable=false)
     */
    private $description;
    
     /**
      * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearUsers", inversedBy="prereqExemptionRequests")
      * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;
    
     /**
      * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearClasses", inversedBy="prereqExemptionRequests")
      * @ORM\JoinColumn(name="class_id", referencedColumnName="id", nullable=false)
     */
    private $class;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Set dateScheduled
     *
     * @param \DateTime $dateScheduled
     * @return PearPrereqExemptionRequests
     */
    public function setDateScheduled($dateScheduled)
    {
        $this->dateScheduled = $dateScheduled;

        return $this;
    }

    /**
     * Get dateScheduled
     *
     * @return \DateTime 
     */
    public function getDateScheduled()
    {
        return $this->dateScheduled;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return PearPrereqExemptionRequests
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\PearUsers $user
     * @return PearPrereqExemptionRequests
     */
    public function setUser(\AppBundle\Entity\PearUsers $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\PearUsers 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set class
     *
     * @param \AppBundle\Entity\PearClasses $class
     * @return PearPrereqExemptionRequests
     */
    public function setClass(\AppBundle\Entity\PearClasses $class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class
     *
     * @return \AppBundle\Entity\PearClasses 
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set testLocation
     *
     * @param string $testLocation
     * @return PearPrereqExemptionRequests
     */
    public function setTestLocation($testLocation)
    {
        $this->testLocation = $testLocation;

        return $this;
    }

    /**
     * Get testLocation
     *
     * @return string 
     */
    public function getTestLocation()
    {
        return $this->testLocation;
    }
}
