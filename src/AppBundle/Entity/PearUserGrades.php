<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
* PearUserGrades
*
* @ORM\Table(name="pear_user_grades")
* @ORM\Entity
*/
class PearUserGrades
{
    /**
    * @var integer
    *
    * @ORM\Column(name="grade", type="integer", nullable=false)
    */
    private $grade;

    /**
    * @ORM\Id()
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearClassAssignments", inversedBy="grades")
    * @ORM\JoinColumn(name="assignment_id", referencedColumnName="id", nullable=false)
    */
    private $assignment;

    /**
    * @ORM\Id()
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearUsers", inversedBy="grades")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)

    */
    private $user;

    /**
    * @ORM\Id()
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearClassSections", inversedBy="grades")
    * @ORM\JoinColumn(name="section_id", referencedColumnName="id", nullable=false)
    */
    private $section;

    /**
    * @var boolean
    *
    * @ORM\Column(name="excused", type="boolean")
    */
    private $excused;

    /**
    * Constructor
    */
    public function __construct()
    {

    }

    /**
    * Set grade
    *
    * @param integer $grade
    * @return PearUserGrades
    */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
    * Get grade
    *
    * @return integer or string
    */
    public function getGrade()
    {
        if ($this->getExcused())
        {
            return 'Excused';
        }

        else
        {
            return $this->grade;
        }
    }

    /**
    * Set user
    *
    * @param string $user
    * @return PearUserGrades
    */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
    * Get user
    *
    * @return string
    */
    public function getUser()
    {
        return $this->user;
    }

    /**
    * Set section
    *
    * @param string $section
    * @return PearUserGrades
    */
    public function setSection($section)
    {
        $this->section = $section;

        return $this;
    }

    /**
    * Get section
    *
    * @return string
    */
    public function getSection()
    {
        return $this->section;
    }

    /**
    * Set assignment
    *
    * @param \AppBundle\Entity\PearClassAssignments $assignment
    * @return PearUserGrades
    */
    public function setAssignment(\AppBundle\Entity\PearClassAssignments $assignment)
    {
        $this->assignment = $assignment;

        return $this;
    }

    /**
    * Get assignment
    *
    * @return \AppBundle\Entity\PearClassAssignments
    */
    public function getAssignment()
    {
        return $this->assignment;
    }

    /**
     * Set excused
     *
     * @param boolean $excused
     *
     * @return PearUserGrades
     */
    public function setExcused($excused)
    {
        $this->excused = $excused;

        return $this;
    }

    /**
     * Get excused
     *
     * @return boolean
     */
    public function getExcused()
    {
        return $this->excused;
    }
}
