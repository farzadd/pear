<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PearClassSections
 *
 * @ORM\Table(name="pear_class_sections")
 * @ORM\Entity
 */
class PearClassSections
{   
    /**
      * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearClasses", inversedBy="sections")
      * @ORM\JoinColumn(name="class_id", referencedColumnName="id", nullable=false)
      */
    private $class;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearCampusTerms", inversedBy="sections")
     * @ORM\JoinColumn(name="term_id", referencedColumnName="id", nullable=false)
     **/
    private $term;
    
    /**
      * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearUsers", inversedBy="teaches")
      * @ORM\JoinColumn(name="teacher_id", referencedColumnName="id", nullable=false)
      */
    private $teacher;
    
    /**
      * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearUsers", inversedBy="teachingAssists")
      * @ORM\JoinColumn(name="ta_id", referencedColumnName="id", nullable=true)
      */
    private $teachingAssistant;
    
    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=64, unique=false)
     */
    private $location;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="max_seats", type="smallint", nullable=false)
     */
    private $maxStudents;

    /**
      * @var \DateTime
      *
      * @ORM\Column(name="start", type="time", nullable=false)
      */
    private $start;

     /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="time", nullable=false)
     */
    private $end;
    
    /**
     * @ORM\Column(name="daysOfWeek", type="smallint", unique=false)
     */
    private $daysOfWeek;
    
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearUsers", mappedBy="enrolls")
     **/
    private $students;
    
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearClassAssignments", inversedBy="sections")
     * @ORM\JoinTable(name="pear_class_sections_assignments")
     **/
    private $assignments;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearUserGrades", mappedBy="section")
     **/
    private $grades;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearAttendance", mappedBy="section")
     **/
    private $attendance;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->students = new ArrayCollection();
        $this->grades = new ArrayCollection();
        $this->attendance = new ArrayCollection();
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return PearClassSections
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return PearClassSections
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime 
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set class
     *
     * @param \AppBundle\Entity\PearClasses $class
     * @return PearClassSections
     */
    public function setClass(\AppBundle\Entity\PearClasses $class = null)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class
     *
     * @return \AppBundle\Entity\PearClasses 
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set teacher
     *
     * @param \AppBundle\Entity\PearUsers $teacher
     * @return PearClassSections
     */
    public function setTeacher(\AppBundle\Entity\PearUsers $teacher = null)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return \AppBundle\Entity\PearUsers 
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Add students
     *
     * @param \AppBundle\Entity\PearUsers $students
     * @return PearClassSections
     */
    public function addStudent(\AppBundle\Entity\PearUsers $students)
    {
        $this->students[] = $students;

        return $this;
    }

    /**
     * Remove students
     *
     * @param \AppBundle\Entity\PearUsers $students
     */
    public function removeStudent(\AppBundle\Entity\PearUsers $students)
    {
        $this->students->removeElement($students);
    }

    /**
     * Get students
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStudents()
    {
        return $this->students;
    }

    /**
     * Add grades
     *
     * @param \AppBundle\Entity\PearUserGrades $grades
     * @return PearClassSections
     */
    public function addGrade(\AppBundle\Entity\PearUserGrades $grades)
    {
        $this->grades[] = $grades;

        return $this;
    }

    /**
     * Remove grades
     *
     * @param \AppBundle\Entity\PearUserGrades $grades
     */
    public function removeGrade(\AppBundle\Entity\PearUserGrades $grades)
    {
        $this->grades->removeElement($grades);
    }

    /**
     * Get grades
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGrades()
    {
        return $this->grades;
    }

    /**
     * Add attendance
     *
     * @param \AppBundle\Entity\PearAttendance $attendance
     * @return PearClassSections
     */
    public function addAttendance(\AppBundle\Entity\PearAttendance $attendance)
    {
        $this->attendance[] = $attendance;

        return $this;
    }

    /**
     * Remove attendance
     *
     * @param \AppBundle\Entity\PearAttendance $attendance
     */
    public function removeAttendance(\AppBundle\Entity\PearAttendance $attendance)
    {
        $this->attendance->removeElement($attendance);
    }

    /**
     * Get attendance
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttendance()
    {
        return $this->attendance;
    }

    /**
     * Set term
     *
     * @param \AppBundle\Entity\PearCampusTerms $term
     * @return PearClassSections
     */
    public function setTerm(\AppBundle\Entity\PearCampusTerms $term)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * Get term
     *
     * @return \AppBundle\Entity\PearCampusTerms 
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return PearClassSections
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set maxStudents
     *
     * @param integer $maxStudents
     * @return PearClassSections
     */
    public function setMaxStudents($maxStudents)
    {
        $this->maxStudents = $maxStudents;

        return $this;
    }

    /**
     * Get maxStudents
     *
     * @return integer 
     */
    public function getMaxStudents()
    {
        return $this->maxStudents;
    }
    
    /**
     * Add assignments
     *
     * @param \AppBundle\Entity\PearClassAssignments $assignments
     * @return PearClassSections
     */
    public function addAssignment(\AppBundle\Entity\PearClassAssignments $assignments)
    {
        $this->assignments[] = $assignments;

        return $this;
    }

    /**
     * Remove assignments
     *
     * @param \AppBundle\Entity\PearClassAssignments $assignments
     */
    public function removeAssignment(\AppBundle\Entity\PearClassAssignments $assignments)
    {
        $this->assignments->removeElement($assignments);
    }

    /**
     * Get assignments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAssignments()
    {
        return $this->assignments;
    }
    
    /**
     * Set daysOfWeek
     *
     * @param integer $daysOfWeek
     * @return PearClassSections
     */
    public function setDaysOfWeek($daysOfWeek)
    {
        $this->daysOfWeek = $daysOfWeek;

        return $this;
    }

    /**
     * Get daysOfWeek
     *
     * @return integer 
     */
    public function getDaysOfWeek()
    {
        return $this->daysOfWeek;
    }

    /**
     * Set teachingAssistant
     *
     * @param \AppBundle\Entity\PearUsers $teachingAssistant
     * @return PearClassSections
     */
    public function setTeachingAssistant(\AppBundle\Entity\PearUsers $teachingAssistant)
    {
        $this->teachingAssistant = $teachingAssistant;

        return $this;
    }

    /**
     * Get teachingAssistant
     *
     * @return \AppBundle\Entity\PearUsers 
     */
    public function getTeachingAssistant()
    {
        return $this->teachingAssistant;
    }
}
