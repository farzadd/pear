<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PearAnnouncements
 *
 * @ORM\Table(name="pear_announcements")
 * @ORM\Entity
 */
class PearAnnouncements
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255)
     */
    private $message;

    /**
     * @var integer
     *
     * @ORM\Column(name="created", type="integer")
     */
    private $created;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pinned", type="boolean")
     */
    private $pinned;

    /**
      * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearUsers")
      * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $createdBy;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearRoles", inversedBy="announcements")
     * @ORM\JoinTable(name="pear_announcements_roles")
     **/
    private $roles;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearCampuses", inversedBy="announcements")
     * @ORM\JoinTable(name="pear_announcements_campuses")
     **/
    private $campuses;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return PearAnnouncements
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set created
     *
     * @param integer $created
     * @return PearAnnouncements
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return integer 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set pinned
     *
     * @param boolean $pinned
     * @return PearAnnouncements
     */
    public function setPinned($pinned)
    {
        $this->pinned = $pinned;

        return $this;
    }

    /**
     * Get pinned
     *
     * @return boolean 
     */
    public function getPinned()
    {
        return $this->pinned;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\PearUsers $createdBy
     * @return PearAnnouncements
     */
    public function setCreatedBy(\AppBundle\Entity\PearUsers $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\PearUsers 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->campuses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add roles
     *
     * @param \AppBundle\Entity\PearRoles $roles
     * @return PearAnnouncements
     */
    public function addRole(\AppBundle\Entity\PearRoles $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \AppBundle\Entity\PearRoles $roles
     */
    public function removeRole(\AppBundle\Entity\PearRoles $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Add campuses
     *
     * @param \AppBundle\Entity\PearCampuses $campuses
     * @return PearAnnouncements
     */
    public function addCampus(\AppBundle\Entity\PearCampuses $campuses)
    {
        $this->campuses[] = $campuses;

        return $this;
    }

    /**
     * Remove campuses
     *
     * @param \AppBundle\Entity\PearCampuses $campuses
     */
    public function removeCampus(\AppBundle\Entity\PearCampuses $campuses)
    {
        $this->campuses->removeElement($campuses);
    }

    /**
     * Get campuses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCampuses()
    {
        return $this->campuses;
    }
}
