<?php

namespace AppBundle\Entity;;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="pear_roles_permissions")
 * @ORM\Entity()
 */
class PearPermissions
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=30)
     */
    private $name;

    /**
     * @ORM\Column(name="flag", type="smallint", unique=false)
     */
    private $flag;
    
    /**
      * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearRoles", inversedBy="permissions")
      * @ORM\JoinColumn(name="role_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
      */
    private $role;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PearPermissions
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set role
     *
     * @param \AppBundle\Entity\PearRoles $role
     * @return PearPermissions
     */
    public function setRole(\AppBundle\Entity\PearRoles $role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return \AppBundle\Entity\PearRoles 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return PearPermissions
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer 
     */
    public function getFlag()
    {
        return $this->flag;
    }
}
