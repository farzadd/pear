<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PearClasses
 *
 * @ORM\Table(name="pear_classes")
 * @ORM\Entity
 */
class PearClasses {

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=40, nullable=false)
     */
    private $name;

     /**
      *
      * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearClassSections", mappedBy="class")
     */
    private $sections;
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=256, nullable=true)
     */
    private $description;

    /**
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearDepartments", inversedBy="classes")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id", nullable=false)
     */
    private $department;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * 
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearClasses")
     * @ORM\JoinTable(name="pear_class_prereqs")
     */
    private $prereqs;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearUsers", mappedBy="prereqs")
     **/
    private $user_prereqs;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearPrereqExemptionRequests", mappedBy="class")
     **/
    private $prereqExemptionRequests;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearClassAssignments", mappedBy="class")
     **/
    private $assignments;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearDocuments", mappedBy="class")
     */
    private $documents;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Constructor
     */
    public function __construct() {
        $this->prereqs = new ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PearClasses
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return PearClasses
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set department
     *
     * @param \AppBundle\Entity\PearDepartments $department
     * @return PearClasses
     */
    public function setDepartment(\AppBundle\Entity\PearDepartments $department = null) {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \AppBundle\Entity\PearDepartments 
     */
    public function getDepartment() {
        return $this->department;
    }

    /**
     * Add prereqs
     *
     * @param \AppBundle\Entity\PearClasses $prereqs
     * @return PearClasses
     */
    public function addPrereq(\AppBundle\Entity\PearClasses $prereqs) {
        $this->prereqs[] = $prereqs;

        return $this;
    }

    /**
     * Remove prereqs
     *
     * @param \AppBundle\Entity\PearClasses $prereqs
     */
    public function removePrereq(\AppBundle\Entity\PearClasses $prereqs) {
        $this->prereqs->removeElement($prereqs);
    }

    /**
     * Get prereqs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrereqs() {
        return $this->prereqs;
    }


    /**
     * Add sections
     *
     * @param \AppBundle\Entity\PearClassSections $sections
     * @return PearClasses
     */
    public function addSection(\AppBundle\Entity\PearClassSections $sections)
    {
        $this->sections[] = $sections;

        return $this;
    }

    /**
     * Remove sections
     *
     * @param \AppBundle\Entity\PearClassSections $sections
     */
    public function removeSection(\AppBundle\Entity\PearClassSections $sections)
    {
        $this->sections->removeElement($sections);
    }

    /**
     * Get sections
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * Add user_prereqs
     *
     * @param \AppBundle\Entity\PearUsers $userPrereqs
     * @return PearClasses
     */
    public function addUserPrereq(\AppBundle\Entity\PearUsers $userPrereqs)
    {
        $this->user_prereqs[] = $userPrereqs;

        return $this;
    }

    /**
     * Remove user_prereqs
     *
     * @param \AppBundle\Entity\PearUsers $userPrereqs
     */
    public function removeUserPrereq(\AppBundle\Entity\PearUsers $userPrereqs)
    {
        $this->user_prereqs->removeElement($userPrereqs);
    }

    /**
     * Get user_prereqs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserPrereqs()
    {
        return $this->user_prereqs;
    }

    /**
     * Add prereqExemptionRequests
     *
     * @param \AppBundle\Entity\PearPrereqExemptionRequests $prereqExemptionRequests
     * @return PearClasses
     */
    public function addPrereqExemptionRequest(\AppBundle\Entity\PearPrereqExemptionRequests $prereqExemptionRequests)
    {
        $this->prereqExemptionRequests[] = $prereqExemptionRequests;

        return $this;
    }

    /**
     * Remove prereqExemptionRequests
     *
     * @param \AppBundle\Entity\PearPrereqExemptionRequests $prereqExemptionRequests
     */
    public function removePrereqExemptionRequest(\AppBundle\Entity\PearPrereqExemptionRequests $prereqExemptionRequests)
    {
        $this->prereqExemptionRequests->removeElement($prereqExemptionRequests);
    }

    /**
     * Get prereqExemptionRequests
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrereqExemptionRequests()
    {
        return $this->prereqExemptionRequests;
    }

    /**
     * Add assignments
     *
     * @param \AppBundle\Entity\PearClassAssignments $assignments
     * @return PearClasses
     */
    public function addAssignment(\AppBundle\Entity\PearClassAssignments $assignments)
    {
        $this->assignments[] = $assignments;

        return $this;
    }

    /**
     * Remove assignments
     *
     * @param \AppBundle\Entity\PearClassAssignments $assignments
     */
    public function removeAssignment(\AppBundle\Entity\PearClassAssignments $assignments)
    {
        $this->assignments->removeElement($assignments);
    }

    /**
     * Get assignments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAssignments()
    {
        return $this->assignments;
    }

    /**
     * Add documents
     *
     * @param \AppBundle\Entity\PearDocuments $documents
     * @return PearClasses
     */
    public function addDocument(\AppBundle\Entity\PearDocuments $documents)
    {
        $this->documents[] = $documents;

        return $this;
    }

    /**
     * Remove documents
     *
     * @param \AppBundle\Entity\PearDocuments $documents
     */
    public function removeDocument(\AppBundle\Entity\PearDocuments $documents)
    {
        $this->documents->removeElement($documents);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocuments()
    {
        return $this->documents;
    }
}
