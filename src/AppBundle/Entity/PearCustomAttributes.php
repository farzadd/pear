<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PearCustomAttributes
 *
 * @ORM\Table(name="pear_custom_attributes")
 * @ORM\Entity
 */
class PearCustomAttributes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=25, nullable=false)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=25, nullable=false)
     */
    private $description;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="on_registeration", type="boolean", nullable=true)
     */
    private $onRegisteration;
    
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearRoles", inversedBy="customAttributes")
     * @ORM\JoinTable(name="pear_roles_customattrs")
     **/
    private $roles;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearUserCustomAttributes", mappedBy="customAttribute")
     **/
    private $userCustomAttributes;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PearCustomAttributes
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return PearCustomAttributes
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add roles
     *
     * @param \AppBundle\Entity\PearRoles $roles
     * @return PearCustomAttributes
     */
    public function addRole(\AppBundle\Entity\PearRoles $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \AppBundle\Entity\PearRoles $roles
     */
    public function removeRole(\AppBundle\Entity\PearRoles $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Add userCustomAttributes
     *
     * @param \AppBundle\Entity\PearUserCustomAttributes $userCustomAttributes
     * @return PearCustomAttributes
     */
    public function addUserCustomAttribute(\AppBundle\Entity\PearUserCustomAttributes $userCustomAttributes)
    {
        $this->userCustomAttributes[] = $userCustomAttributes;

        return $this;
    }

    /**
     * Remove userCustomAttributes
     *
     * @param \AppBundle\Entity\PearUserCustomAttributes $userCustomAttributes
     */
    public function removeUserCustomAttribute(\AppBundle\Entity\PearUserCustomAttributes $userCustomAttributes)
    {
        $this->userCustomAttributes->removeElement($userCustomAttributes);
    }

    /**
     * Get userCustomAttributes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserCustomAttributes()
    {
        return $this->userCustomAttributes;
    }

    /**
     * Set onRegisteration
     *
     * @param boolean $onRegisteration
     * @return PearCustomAttributes
     */
    public function setOnRegisteration($onRegisteration)
    {
        $this->onRegisteration = $onRegisteration;

        return $this;
    }

    /**
     * Get onRegisteration
     *
     * @return boolean 
     */
    public function getOnRegisteration()
    {
        return $this->onRegisteration;
    }
}
