<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PearUserNotifications
 *
 * @ORM\Table(name="pear_user_notifications", indexes={@ORM\Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity
 */
class PearUserNotifications
{
    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=256, nullable=false)
     */
    private $message;

    /**
     * @var boolean
     *
     * @ORM\Column(name="seen", type="boolean", nullable=false)
     */
    private $seen;

    /**
     * @var boolean
     *
     * @ORM\Column(name="can_expire", type="boolean", nullable=false)
     */
    private $canExpire;

    /**
     * @var integer
     *
     * @ORM\Column(name="created", type="integer", nullable=false)
     */
    private $created;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\PearUsers
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearUsers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Set message
     *
     * @param string $message
     * @return PearUserNotifications
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set seen
     *
     * @param boolean $seen
     * @return PearUserNotifications
     */
    public function setSeen($seen)
    {
        $this->seen = $seen;

        return $this;
    }

    /**
     * Get seen
     *
     * @return boolean 
     */
    public function getSeen()
    {
        return $this->seen;
    }

    /**
     * Set canExpire
     *
     * @param boolean $canExpire
     * @return PearUserNotifications
     */
    public function setCanExpire($canExpire)
    {
        $this->canExpire = $canExpire;

        return $this;
    }

    /**
     * Get canExpire
     *
     * @return boolean 
     */
    public function getCanExpire()
    {
        return $this->canExpire;
    }

    /**
     * Set created
     *
     * @param integer $created
     * @return PearUserNotifications
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return integer 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\PearUsers $user
     * @return PearUserNotifications
     */
    public function setUser(\AppBundle\Entity\PearUsers $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\PearUsers 
     */
    public function getUser()
    {
        return $this->user;
    }
}
