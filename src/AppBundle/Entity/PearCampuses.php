<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PearCampus
 *
 * @ORM\Table(name="pear_campuses")
 * @ORM\Entity
 */
class PearCampuses
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=25, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=256, nullable=true)
     */
    private $description;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="location", type="string", length=64, nullable=false)
     */
    private $location;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="phone", type="string", length=12, nullable=true)
     */
    private $phone;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearCampusTerms", mappedBy="campus")
     **/
    private $terms;
    
    /**
     * @ORM\OneToOne(targetEntity="PearCampusTerms")
     * @ORM\JoinColumn(name="activeTerm", referencedColumnName="id", nullable=true)
     **/
    private $activeTerm;
    
    /**
     * @ORM\OneToOne(targetEntity="PearCampusTerms")
     * @ORM\JoinColumn(name="enrollment_term", referencedColumnName="id", nullable=true)
     **/
    private $enrollmentTerm;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearUsers", mappedBy="campus")
     **/
    private $users;
    
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\PearAnnouncements", mappedBy="campuses")
     **/
    private $announcements;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="timezone", type="string", length=256, nullable=false)
     */
    private $timezone;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->terms = new ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PearCampuses
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return PearCampuses
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return PearCampuses
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return PearCampuses
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add terms
     *
     * @param \AppBundle\Entity\PearCampusTerms $terms
     * @return PearCampuses
     */
    public function addTerm(\AppBundle\Entity\PearCampusTerms $terms)
    {
        $this->terms[] = $terms;

        return $this;
    }

    /**
     * Remove terms
     *
     * @param \AppBundle\Entity\PearCampusTerms $terms
     */
    public function removeTerm(\AppBundle\Entity\PearCampusTerms $terms)
    {
        $this->terms->removeElement($terms);
    }

    /**
     * Get terms
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * Add users
     *
     * @param \AppBundle\Entity\PearUsers $users
     * @return PearCampuses
     */
    public function addUser(\AppBundle\Entity\PearUsers $users)
    {
        $this->users[] = $users;
    }

    /**
     * Remove users
     *
     * @param \AppBundle\Entity\PearUsers $users
     */
    public function removeUser(\AppBundle\Entity\PearUsers $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set activeTerm
     *
     * @param \AppBundle\Entity\PearCampusTerms $activeTerm
     * @return PearCampuses
     */
    public function setActiveTerm(\AppBundle\Entity\PearCampusTerms $activeTerm = null)
    {
        $this->activeTerm = $activeTerm;

        return $this;
    }

    /**
     * Get activeTerm
     *
     * @return \AppBundle\Entity\PearCampusTerms 
     */
    public function getActiveTerm()
    {
        return $this->activeTerm;
    }

    /**
     * Add announcements
     *
     * @param \AppBundle\Entity\PearAnnouncements $announcements
     * @return PearCampuses
     */
    public function addAnnouncement(\AppBundle\Entity\PearAnnouncements $announcements)
    {
        $this->announcements[] = $announcements;

        return $this;
    }

    /**
     * Remove announcements
     *
     * @param \AppBundle\Entity\PearAnnouncements $announcements
     */
    public function removeAnnouncement(\AppBundle\Entity\PearAnnouncements $announcements)
    {
        $this->announcements->removeElement($announcements);
    }

    /**
     * Get announcements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAnnouncements()
    {
        return $this->announcements;
    }
    
    public function __toString()
    {
        try
        {
            return $this->name;
        }
        catch (Exception $e) {}
        return null;
    }


    /**
     * Set timezone
     *
     * @param string $timezone
     *
     * @return PearCampuses
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone
     *
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set enrollmentTerm
     *
     * @param \AppBundle\Entity\PearCampusTerms $enrollmentTerm
     * @return PearCampuses
     */
    public function setEnrollmentTerm(\AppBundle\Entity\PearCampusTerms $enrollmentTerm = null)
    {
        $this->enrollmentTerm = $enrollmentTerm;

        return $this;
    }

    /**
     * Get enrollmentTerm
     *
     * @return \AppBundle\Entity\PearCampusTerms 
     */
    public function getEnrollmentTerm()
    {
        return $this->enrollmentTerm;
    }
}
