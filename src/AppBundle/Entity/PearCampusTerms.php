<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PearTerms
 *
 * @ORM\Table(name="pear_campus_terms")
 * @ORM\Entity
 */
class PearCampusTerms
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;
    
     /**
      * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PearCampuses", inversedBy="terms")
      * @ORM\JoinColumn(name="campus_id", referencedColumnName="id")
     */
    private $campus;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PearClassSections", mappedBy="term")
     **/
    private $sections;

    /**
     * @var boolean
     *
     * @ORM\Column(name="term_closed", type="boolean", nullable=false)
     */
    private $termClosed;
    
    /**
      * @var \DateTime
      *
      * @ORM\Column(name="start", type="date", nullable=false)
      */
    private $start;

     /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="date", nullable=false)
     */
    private $end;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Set campus
     *
     * @param \AppBundle\Entity\PearCampuses $campus
     * @return PearYears
     */
    public function setCampus(\AppBundle\Entity\PearCampuses $campus)
    {
        $this->campus = $campus;

        return $this;
    }

    /**
     * Get campus
     *
     * @return \AppBundle\Entity\PearCampuses
     */
    public function getCampus()
    {
        return $this->campus;
    }


    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Add sections
     *
     * @param \AppBundle\Entity\PearClassSections $sections
     * @return PearCampusTerms
     */
    public function addSection(\AppBundle\Entity\PearClassSections $sections)
    {
        $this->sections[] = $sections;

        return $this;
    }

    /**
     * Remove sections
     *
     * @param \AppBundle\Entity\PearClassSections $sections
     */
    public function removeSection(\AppBundle\Entity\PearClassSections $sections)
    {
        $this->sections->removeElement($sections);
    }

    /**
     * Get sections
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PearCampusTerms
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set start
     *
     * @param \DateTime $start
     * @return PearCampusTerm
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return PearCampusTerms
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime 
     */
    public function getEnd()
    {
        return $this->end;
    }


    /**
     * Set termClosed
     *
     * @param boolean $termClosed
     * @return PearCampusTerms
     */
    public function setTermClosed($termClosed)
    {
        $this->termClosed = $termClosed;

        return $this;
    }

    /**
     * Get termClosed
     *
     * @return boolean 
     */
    public function getTermClosed()
    {
        return $this->termClosed;
    }
}
