<?php

namespace AppBundle\Tests\Controller;

class RegisterationTest extends PearHelper
{
    // =========================
    //         Set-up
    // =========================
    public function setUp()
    {
        $this->client = static::createClient();
    }
    
    // =========================
    //         All Tests
    // =========================
    public function testRegistration()
    {
        // Go to the login page
        $mainRoute = $this->getRoute("_login", array());
        $crawler = $this->client->request('GET', $mainRoute);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET " . $mainRoute);
        $crawler = $this->client->click($crawler->selectLink('Sign up here')->link());

        // Fill in the form and submit it
        $form = $crawler->selectButton('Apply for Registration')->form(array(
            'u[email]' => 'test@pearprogrammers.com',
            'u[password][first]' => 'password',
            'u[password][second]' => 'password',
            'u[firstName]' => 'TestFirstName',
            'u[lastName]' => 'TestLastName',
            'u[birthDate]' => '01/01/1994',
            'u[gender]' => '1',
            'u[homeAddress]' => 'Test Land',
            'u[phone]' => '1234546253'
        ));

        $this->client->submit($form);
        
        // Check to see if everything passed other than human validation
        $this->assertGreaterThan(0, $crawler->filter('html:contains("Student Registration")')->count(), 'Missing element Student Registration")');
    }
}
