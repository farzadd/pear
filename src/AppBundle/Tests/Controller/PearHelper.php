<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class PearHelper extends WebTestCase
{
    protected $client;
    
    protected function logIn($username = "pearsuperadmin@pear.com", $password = "cupcakes1008", $firstName = "SuperAdmin")
    {
        // Request login page
        $crawler = $this->client->request('GET', '/login');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /");
        
        $form = $crawler->selectButton('Login')->form(array(
            '_username' => $username,
            '_password' => $password
            ));
        
        // Submit ID and PASS. Follow to redirect
        $this->client->submit($form);
        $crawler = $this->client->followRedirect();
        
        $user = $this->client->getContainer()->get('security.token_storage')->getToken()->getUser();
        $this->assertEquals($firstName, $user->getFirstName(), "Login Failed");
    }
    
    protected function getRoute($route, $params)
    {   
        return $this->client->getContainer()->get('router')->generate($route, $params);
    }
}
