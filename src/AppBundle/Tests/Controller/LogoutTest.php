<?php 
namespace AppBundle\Tests\Controller;

class LogoutTest extends PearHelper
{   
    // =========================
    //         Set-up
    // =========================
    public function setUp()
    {
        $this->client = static::createClient();
        $this->logIn();
    }
    
    // =========================
    //         All Tests
    // =========================
    public function testLogout()
    {
        // Perform logout
        $mainRoute = $this->getRoute("_logout", array());
        $crawler = $this->client->request('GET', $mainRoute);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET " . $mainRoute);
        
        $crawler = $this->client->followRedirect();
        
        // Go to the homepage
        $mainRoute = $this->getRoute("_homepage", array());
        $crawler = $this->client->request('GET', $mainRoute);
        $this->assertEquals(302, $this->client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET " . $mainRoute);
        
        $crawler = $this->client->followRedirect();
        
        // Check to see if we got redirected to login
        $this->assertRegExp('/Login/', $this->client->getResponse()->getContent());
    }
}
