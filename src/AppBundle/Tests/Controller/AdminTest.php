<?php

namespace AppBundle\Tests\Controller;

class AdminTest extends PearHelper
{   
    // =========================
    //         Set-up
    // =========================
    public function setUp()
    {
        $this->client = static::createClient();
        $this->logIn();
    }
    
    // =========================
    //         All Tests
    // =========================
    public function testCampuses()
    {
        // Create a new entry in the database
        $mainRoute = $this->getRoute("_admin_campuses", array());
        $crawler = $this->client->request('GET', $mainRoute);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET " . $mainRoute);
        $crawler = $this->client->click($crawler->selectLink('Create New Campus')->link());

        // Fill in the form and submit it
        $form = $crawler->selectButton('Submit')->form(array(
            'campus[name]' => 'Testing 1 2 3',
            'campus[description]' => 'I AM MASTER CREATOR 2',
            'campus[location]' => 'TESTVILLE 2',
            'campus[phone]' => '6046039045'
        ));

        $this->client->submit($form);
        $crawler = $this->client->followRedirect();

        // Check data in the show view
        $this->assertGreaterThan(0, $crawler->filter('div:contains("TESTINGCAMPUSCREATE")')->count(), 'Missing element div:contains("TESTINGCAMPUSCREATE")');

        // Edit the entity
        $crawler = $this->client->click($crawler->selectLink('Edit Information')->link());

        $form = $crawler->selectButton('Submit')->form(array(
            'campus[name]' => 'TESTINGCAMPUSEDIT',
            'campus[description]' => 'I AM MASTER CREATOR',
            'campus[location]' => 'TESTVILLE',
            'campus[phone]' => '6046039045'
        ));

        $this->client->submit($form);
        $crawler = $this->client->followRedirect();

        // Check the element contains an attribute with value equals "Foo"
        $this->assertGreaterThan(0, $crawler->filter('html:contains("TESTINGCAMPUSEDIT")')->count(), 'Missing element html:contains("TESTINGCAMPUSEDIT")');

        // Delete the entity
        $this->client->click($crawler->selectLink('Delete This Campus')->link());
        $crawler = $this->client->followRedirect();

        // Check the entity has been delete on the list
       $this->assertGreaterThan(0, $crawler->filter('html:contains("Campus Deleted!")')->count(), 'Missing element html:contains("Campus Deleted!")');

    }


    public function testAnnouncements()
    {
        // Create a new entry in the database
        $mainRoute = $this->getRoute("_admin_announcements", array());
        $crawler = $this->client->request('GET', $mainRoute);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET " . $mainRoute);
        $crawler = $this->client->click($crawler->selectLink('Create a new announcement')->link());
        
        // Fill in the form and submit it
        $form = $crawler->selectButton('Create')->form(array(
            'appbundle_pearannouncements[message]' => 'PEARTestMessageAfterCreate',
            'appbundle_pearannouncements[pinned]' => '1'
        ));

        $this->client->submit($form);
        $crawler = $this->client->followRedirect();
        
        // Check data in the show view
        $this->assertGreaterThan(0, $crawler->filter('td:contains("PEARTestMessageAfterCreate")')->count(), 'Missing element td:contains("PEARTestMessageAfterCreate")');

        // Edit the entity
        $crawler = $this->client->click($crawler->selectLink('Edit Announcement')->link());

        $form = $crawler->selectButton('Update')->form(array(
            'appbundle_pearannouncements[message]'  => 'PEARTestMessageAfterEdit',
            'appbundle_pearannouncements[pinned]' => '1',
        ));

        $this->client->submit($form);
        $crawler = $this->client->followRedirect();

        // Check the element contains an attribute with value equals "Foo"
        $this->assertGreaterThan(0, $crawler->filter('textarea:contains("PEARTestMessageAfterEdit")')->count(), 'Missing element textarea:contains("PEARTestMessageAfterEdit")');

        // Delete the entity
        $this->client->submit($crawler->selectButton('Delete')->form());
        $crawler = $this->client->followRedirect();

        // Check the entity has been delete on the list
        $this->assertNotRegExp('/PEARTestMessageAfterEdit/', $this->client->getResponse()->getContent());
    }
    
    public function testPermissionGroups()
    {
        // Create a new entry in the database
        $mainRoute = $this->getRoute("_admin_permission_group", array());
        $crawler = $this->client->request('GET', $mainRoute);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET " . $mainRoute);
        $crawler = $this->client->click($crawler->selectLink('Add Permission Group')->link());

        // Fill in the form and submit it
        $form = $crawler->selectButton('Submit')->form(array(
            'role[name]' => 'TEST ROLE - DONT UPVOTE',
            'role[role]' => 'ROLE_TEST'
        ));

        $this->client->submit($form);
        $crawler = $this->client->followRedirect();

        // Check data in the show view
        $this->assertGreaterThan(0, $crawler->filter('td:contains("ROLE_TEST")')->count(), 'Missing element td:contains("ROLE_TEST")');

        // Edit the entity
        $crawler = $this->client->click($crawler->filter('a:contains("Edit")')->last()->link());

        $form = $crawler->selectButton('Submit')->form(array(
            'role[name]'  => 'TEST ROLE - TO BE DELETED'
        ));

        $this->client->submit($form);
        $crawler = $this->client->followRedirect();

        // Check the element contains an attribute with value equals "Foo"
        $this->assertGreaterThan(0, $crawler->filter('html:contains("Success!")')->count(), 'Missing element html:contains("Success!")');
        
        // Delete the entity
        $crawler = $this->client->click($crawler->filter('a:contains("Delete")')->last()->link());

        // Check the entity has been delete on the list
        $this->assertNotRegExp('/ROLE_TEST/', $this->client->getResponse()->getContent());
//    }

    
    public function testDepartments() 
    {   
        // Go to Departments
        $mainRoute = $this->getRoute("_admin_departments", array());
        $crawler = $this->client->request('GET', $mainRoute);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET " . $mainRoute);
        // Create new Department
        $crawler = $this->client->click($crawler->selectLink('Create New Department')->link());
        
        // Fill in form
        $form = $crawler->selectbutton('Submit')->form(array(
            'department[name]' => 'TestDepartment_Creation',
            'department[description]' => 'TestDepartment_Desc'
        ));
        
        $this->client->submit($form);
        $crawler = $this->client->followRedirect();
        // Check for success message post department creation
        $this->assertGreaterThan(0, $crawler->filter('html:contains("Success!")')->count(), "Missing element: Success Message");
        
        
        // Edit and save department info
        $crawler = $this->client->click($crawler->selectLink('Edit Department Info')->link());
        
        $form = $crawler->selectButton('Submit')->form(array(
            'department[name]' => 'TestDepartment_Edit',
            'department[description]' => 'TestDepartment_Desc_Edit'
        ));
        
        $this->client->submit($form);
        $crawler = $this->client->followRedirect();
        
        // Check for TestDepartment_Edit
        $this->assertGreaterThan(0, $crawler->filter('h4:contains("TestDepartment_Edit")')->count(), "Missing element: TestDepartment_Edit");
       
        // Route to edited department and create a class
        $crawler = $this->client->click($crawler->selectLink('TestDepartment_Edit')->link());
        $crawler = $this->client->click($crawler->selectLink('Create New Class')->link());
        
        $form = $crawler->selectButton('Submit')->form(array(
            'course[name]' => 'TestClass_Create',
            'course[description]' => 'TestClass_Desc'
        ));
        
        $this->client->submit($form);
        $crawler = $this->client->followRedirect();
        $this->assertGreaterThan(0, $crawler->filter('td:contains("TestClass_Create")')->count(), "Missing element: TestClass_Create");
        
        // Edit Class
//        $crawler = $this->client->click($crawler->selectLink('Edit')->link());
//        
//        $form = $crawler->selectButton('Submit')->form(array(
//            'course[name]' => 'TestClass_Edit',
//            'course[description]' => 'TestClass_Desc_Edit'
//        ));
//      
//        $this->client->submit($form);
//        $crawler = $this->client->followRedirect();
//        $this->assertGreaterThan(0, $crawler->filter('td:contains("TestClass_Edit")')->count(), "Missing element: TestClass_Edit");
    
        
        // Delete course
        // Delete department
    }   
}
