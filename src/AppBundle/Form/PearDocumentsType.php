<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PearDocumentsType
 *
 * @author minwooyoon
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PearDocumentsType extends AbstractType {
    
    private $assignments;
    
    public function __construct($assignments)
    {
        $this->assignments = $assignments;
    }
    
    function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('name', 'text')
                ->add('file', 'file')
                ->add('assignment', 'entity', array(
                    'class' => 'AppBundle:PearClassAssignments',
                    'property' => 'name',
                    'choices' => $this->assignments))
                ->add('save', 'submit', array('label' => 'Submit'));
        
    }
    public function getName()
    {
        return 'documents';
    }
}
