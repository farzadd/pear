<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AddPaymentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('amount', 'integer', array('label' => 'Amount Paid' ))
        ->add('receiptId', 'text', array('label' => 'Receipt number' ))
        ->add('note', 'textarea', array('label' => 'Notes', 'required' => false))
        ->add('paymentType', 'choice', array(
            'label' => 'Payment Type', 
            'required' => false,
            'choices' => array(
                'Cash'   => 'Cash',
                'Cheque' => 'Cheque',
                'Debit'   => 'Debit',
                'Visa'   => 'Visa',
                'Mastercard'   => 'Mastercard',
                'Other'   => 'Other',
                )))
        ->add('save', 'submit', array('label' => 'Save Payment'));
    }

    public function getName()
    {
        return 'newPayment';
    }
}