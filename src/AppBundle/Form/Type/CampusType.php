<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CampusType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('description', 'text')
            ->add('location', 'text')
            ->add('phone', 'text')
            ->add('timezone', 'timezone', array(
                'required' => true,
                'expanded' => false,
                'multiple' => false
            )) 
            ->add('save', 'submit', array('label' => 'Submit'));
    }
    
    public function getName()
    {
        return 'campus';
    }
}