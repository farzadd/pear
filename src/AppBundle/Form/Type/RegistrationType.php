<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('file','file', array(
          'required' => false, 'label' => 'User Photo'))
        ->add('email', 'email', array(
          'required' => false))
        ->add('password', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'The password fields must match.',
            'required' => false,
            'first_options' => array('label' => 'Create a Password', 'error_bubbling' => true),
            'second_options' => array('label' => 'Confirm Your Password')))
        ->add('campus', 'entity', array(
            'class' => 'AppBundle:PearCampuses',
            'property' => 'name',
            'required' => true,
            'multiple' => false,
            ))
        ->add('firstName', 'text', array(
          'label' => 'First Name'))
        ->add('lastName', 'text', array(
          'label' => 'Last Name'))
        ->add('gender', 'choice', array(
            'placeholder' => 'I am...',
            'choices' => array('0' => 'Female', '1' => 'Male'),
            'required' => true,
            ))
        ->add('birthDate', 'birthday', array(
            'widget' => 'single_text',
            'label' => 'Birth Date'))
        ->add('homeAddress', 'text', array(
            'label' => 'Home Address',
            'required' => false))
        ->add('phone', 'text', array(
            'required' => false))
        ->add('emergencyName', 'text', array(
            'label' => 'Emergency Contact Name', 
            'required' => false))
        ->add('emergencyContact', 'text', array(
            'label' => 'Emergency Contact Phone Number', 
            'required' => false))

        ->add('city', 'text', array(
          'required' => false))
        ->add('middleName', 'text', array(
          'required' => false,
          'label' => 'Middle Name'))
        ->add('parentName', 'text', array(
          'required' => false,
          'label' => 'Parent Name'))
        ->add('parentEmail', 'email', array(
          'required' => false, 
          'label' => 'Parent Email'))
        ->add('parentNumber', 'text', array(
          'required' => false, 
          'label' => 'Parent Phone'))
        ->add('postalCode', 'text', array(
          'required' => false, 
          'label' => 'Postal Code'))
        ->add('allergies', 'textarea', array(
          'required' => false, 
          'label' => 'Allergies'))
        ->add('tshirtSize', 'choice', array(
            'required' => false,
            'placeholder' => 'Choose an T-Shirt Size',
            'choices' => array(
                'Yxs'   => 'Youth Extra Small',
                'Ys' => 'Youth Small',
                'Ym'   => 'Youth Medium',
                'Yl'   => 'Youth Large',
                'Yxl'   => 'Youth XL',
                'Yxxl'   => 'Youth XXL',
                'Axs'   => 'Adult Extra Small',
                'As' => 'Adult Small',
                'Am'   => 'Adult Medium',
                'Al'   => 'Adult Large',
                'Axl'   => 'Adult XL',
                'Axxl'   => 'Adult XXL',
            )
        ))
        ->add('apply', 'submit', array('label' => 'Apply for Registration'));
    }

    public function getName() {
        return 'u';
    }

}
