<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PermissionGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('role', 'text')
            ->add('customAttributes', 'collection', array(
                'type'   => 'text',
                'allow_add' => true,
                'options'  => array(
                    'required'  => true,
                    'attr'      => array('class' => 'form-control')
                ),
                    'mapped' => false
                ))
            ->add('apply', 'submit', array('label' => 'Submit'));
    }
    
    public function getName()
    {
        return 'role';
    }
}
