<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class GlobalAssignmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('required' => true))
            ->add('description', 'textarea', array('required' => true))
            ->add('weight', 'number', array('required' => true))
            ->add('save', 'submit', array('label' => 'Submit'));
    }
    
    public function getName()
    {
        return 'globalAssignment';
    }
}
