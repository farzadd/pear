<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class TermType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('campus_id', 'hidden', array('mapped' => false))
            ->add('name', 'text')
            ->add('start', 'date')
            ->add('end', 'date');
    }
    
    public function getName()
    {
        return 'term';
    }
}