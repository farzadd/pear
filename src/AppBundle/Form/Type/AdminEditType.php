<?php


namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AdminEditType extends AbstractType
{
    protected $hasSuper;
    protected $campus;
    
    public function __construct($hasSuper, $campus){
        $this->hasSuper = $hasSuper;
        $this->campus = $campus;
    }  
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('file','file', array(
          'required' => false,'label' => 'User Photo'))
        ->add('email', 'email', array(
          'required' => false))
        ->add('password', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'The password fields must match.',
            'required' => false,
            'first_options' => array('label' => 'Create a Password', 'error_bubbling' => true),
            'second_options' => array('label' => 'Confirm Your Password')))            
        ->add('roles', 'entity', array(
          'class' => 'AppBundle:PearRoles',
          'property' => 'name',
          'required' => true,
          'expanded' => false,
          'multiple' => true,
          ));
          
        if ($this->hasSuper)
        {
            $builder->add('campus', 'entity', array(
                'class' => 'AppBundle:PearCampuses',
                'property' => 'name',
                'required' => true,
                ));
        } else {
            $builder->add('campus', 'entity', array(
                'class' => 'AppBundle:PearCampuses',
                'property' => 'name',
                'required' => true,
                'choices' => array($this->campus),
                ));
        }
        $builder->add('firstName', 'text', array(
          'label' => 'First Name'))
        ->add('lastName', 'text', array(
          'label' => 'Last Name'))
        ->add('gender', 'choice', array(
            'placeholder' => 'I am...',
            'choices' => array('0' => 'Female', '1' => 'Male'),
            'required' => true,
            ))
        ->add('birthDate', 'birthday', array(
            'widget' => 'single_text',
            'label' => 'Birth Date'))
        ->add('homeAddress', 'text', array(
            'label' => 'Home Address',
            'required' => false))
        ->add('phone', 'text', array(
            'required' => false))
        ->add('emergencyName', 'text', array(
            'label' => 'Emergency Contact Name', 
            'required' => false))
        ->add('emergencyContact', 'text', array(
            'label' => 'Emergency Contact Phone Number', 
            'required' => false))
        ->add('customAttrs', 'collection', array(
            'type'   => 'text',
            'allow_add' => true,
            'options'  => array(
                'required'  => true,
                'data' => '__value__'
                ),
            'mapped' => false
            ))
        ->add('prereqs', 'entity', array(
            'class' => 'AppBundle:PearClasses',
            'property' => 'name',
            'required' => false,
            'expanded' => true,
            'multiple' => true,
            ))
        ->add('city', 'text', array(
          'required' => false))
        ->add('middleName', 'text', array(
          'required' => false,
          'label' => 'Middle Name'))
        ->add('parentName', 'text', array(
          'required' => false,
          'label' => 'Parent Name'))
        ->add('parentEmail', 'email', array(
          'required' => false, 
          'label' => 'Parent Email'))
        ->add('parentNumber', 'text', array(
          'required' => false, 
          'label' => 'Parent Phone'))
        ->add('postalCode', 'text', array(
          'required' => false, 
          'label' => 'Postal Code'))
        ->add('allergies', 'textarea', array(
          'required' => false, 
          'label' => 'Allergies'))
        ->add('tshirtSize', 'choice', array(
            'required' => false,
            'placeholder' => 'Choose an T-Shirt Size',
            'choices' => array(
                'Yxs'   => 'Youth Extra Small',
                'Ys' => 'Youth Small',
                'Ym'   => 'Youth Medium',
                'Yl'   => 'Youth Large',
                'Yxl'   => 'Youth XL',
                'Yxxl'   => 'Youth XXL',
                'Axs'   => 'Adult Extra Small',
                'As' => 'Adult Small',
                'Am'   => 'Adult Medium',
                'Al'   => 'Adult Large',
                'Axl'   => 'Adult XL',
                'Axxl'   => 'Adult XXL',
            )
        ))
        ->add('apply', 'submit', array('label' => 'Save Changes'));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\PearUsers',
            'validation_groups' => array('edit'),
            ));
    }
    
    public function getName()
    {
        return 'user';
    }
}