<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\PearUserAttendance;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AttendanceType extends AbstractType
{
//    private $section;
//        
//        public function __construct($section){
//    
//        $this->section = $section;
//    }  
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('late', 'collection', array(
//                'type'=>'choice',
//           
//                    'options'=> array(
//                        'expanded'=>true,
//                        'multiple'=>true,
//                        'choices'=> array(
//                            ''=> 'Present',
//                            true=> 'Late',
//                            false=> 'Absent',
//                            )
//
//                    )
//            ))
                ->add('attendances', 'collection', array(
                    //each item in the array will be an 'attendance' field
                    'type' => new PearUserAttendance(),
                    // these options are passed to each 'attendance' type
//                    'options' => array(
//                       'multiple' => true,
//                       'required' => false,
//                       'attr' => array(
//                           'choices' => array(
//                               true => 'Late',
//                               false => 'Absent',
//                               null => 'Present'
//                              'class' => 'late')
//                           ) 
//                       
//                    )
//                        )
                        )
                    );
    }

    public function getName() {
        
    }

}