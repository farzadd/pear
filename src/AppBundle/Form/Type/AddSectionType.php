<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;


class AddSectionType extends AbstractType
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('class', 'hidden', array('mapped' => false))
        ->add('maxStudents', 'number')
        ->add('location', 'text')
        ->add('start', 'time')
        ->add('end', 'time');

        $user = $this->tokenStorage->getToken()->getUser();
        if (!$user)
        {
            throw new \LogicException('Cannot create or update section without an authenticated user!');
        }
    $builder->addEventListener(
    FormEvents::PRE_SET_DATA,
    function (FormEvent $event) use ($user) {
        $form = $event->getForm();

        $formOptions = array(
            'class' => 'AppBundle:PearUsers',
            'query_builder' => function (\AppBundle\Entity\PearUserRepository $er) use ($user) {
                return $er->createQueryBuilder('u')
                ->innerJoin('u.roles', 'r')
                ->where('r.role = :role AND u.campus = :campus')
                ->setParameter('role', 'ROLE_TEACHER')
                ->setParameter('campus', $user->getCampus());
            },
            'required' => true,
            'expanded' => false,
            'multiple' => false
        );

        // create the field, this is similar the $builder->add()
        // field name, field type, data, options
        $form->add('teacher','entity', $formOptions);
    }
    )
    ->addEventListener(
    FormEvents::PRE_SET_DATA,
    function (FormEvent $event) use ($user) {
        $form = $event->getForm();

        $formOptions = array(
            'class' => 'AppBundle:PearUsers',
            'query_builder' => function (\AppBundle\Entity\PearUserRepository $er)  use ($user){
                return $er->createQueryBuilder('u')
                ->innerJoin('u.roles', 'r')
                ->where('r.role = :role AND u.campus = :campus')
                ->setParameter('role', 'ROLE_TEACHER')
                ->setParameter('campus', $user->getCampus());
            },
            'required' => true,
            'expanded' => false,
            'multiple' => false
        );

        // create the field, this is similar the $builder->add()
        // field name, field type, data, options
        $form->add('teachingAssistant','entity', $formOptions);
    }
);
}

public function getName()
{
    return 'addsection';
}
}
