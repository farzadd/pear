<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AnnouncementsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('message', 'textarea')
            ->add('campuses', 'entity', array(
                    'class' => 'AppBundle:PearCampuses',
                    'property' => 'name',
                    'required' => false,
                    'expanded' => false,
                    'multiple' => true,
                    'attr' => array('size' => '5'),
                ))
            ->add('roles', 'entity', array(
                    'class' => 'AppBundle:PearRoles',
                    'property' => 'name',
                    'required' => false,
                    'expanded' => false,
                    'multiple' => true,
                    'attr' => array('size' => '5'),
                ))
            ->add('pinned', 'checkbox',  array('required'  => false,))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\PearAnnouncements'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_pearannouncements';
    }
}
