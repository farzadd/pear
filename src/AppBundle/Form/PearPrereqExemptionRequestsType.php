<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PearPrereqExemptionRequestsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', 'textarea')
            ->add('testLocation', 'text')
            ->add('dateScheduled', 'text')
            ->add('save', 'submit', array('label' => 'Submit'));

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\PearPrereqExemptionRequests'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'appbundle_pearprereqexemptionrequests';
    }
}
