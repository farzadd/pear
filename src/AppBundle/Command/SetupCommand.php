<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Helper\ProgressBar;

use AppBundle\Entity\PearSettings;
use AppBundle\Entity\PearPermissions;
use AppBundle\Entity\PearRoles;
use AppBundle\Entity\PearCampuses;

class SetupCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('pear:setup')
            ->setDescription('Sets up the pear environment for the first time')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<question>** WARNING ** You should run this command only once. This may wipe all the data in the system. </question>');
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Are you sure you want to set-up PEAR? (y/n) ', false);

        if (!$helper->ask($input, $output, $question)) {
            return;
        }
        
        $question = new Question('Please enter the name of the first campus to set-up (TemporaryCampus): ', 'TemporaryCampus');
        $campusName = $helper->ask($input, $output, $question);
       
        $output->writeln('<info>Setting up PEAR for the first time.</info>');
        
        $progress = new ProgressBar($output, 6);    // 5 [ROLES] + 1 [CAMPUS]
        $progress->setFormat('%percent:3s%% %memory:6s% [%message%]');
        $progress->setMessage('Initializing');
        $progress->start();
        
        $em = $this->getContainer()->get('doctrine')->getManager();
        $progress->setMessage('Creating roles');
        
        $roles = array(
                        'ROLE_USER' => 'Unconfirmed User',
                        'ROLE_STUDENT' => 'Student',
                        'ROLE_TEACHER' => 'Teacher',
                        'ROLE_ADMIN' => 'Admin',
                        'ROLE_SUPERADMIN' => 'Super Admin');
        foreach ($roles as $role => $name)
        {
            $tRole = new PearRoles();
            $tRole->setName($name);
            $tRole->setRole($role);
            
            if ($role == "ROLE_SUPERADMIN")
            {
                $controllers = array(
                    "Search",
                    "Users",
                    "PrereqExemptionRequests",
                    "Admin\Announcements",
                    "Admin\Campuses",
                    "Admin\CampusTerms",
                    "Admin\Classes",
                    "Admin\Dashboard",
                    "Admin\Departments",
                    "Admin\Enrollment",
                    "Admin\ImportExport",
                    "Admin\Payments",
                    "Admin\PermissionGroup",
                    "Admin\Sections",
                    "Admin\Report",
                    "Admin\Settings",
                    "Admin\Users"
                );
                
                foreach ($controllers as $controller)
                {
                    $tPermission = new PearPermissions();
                    $tPermission->setName($controller);
                    $tPermission->setFlag(63);
                    $em->persist($tPermission);
                }
            }
            
            $em->persist($tRole);
            $progress->advance();
        }
        
        $em->flush();
        
        $progress->setMessage('Creating campus: ' . $campusName);
        $campus = new PearCampuses();
        $campus->setName($campusName);
        $campus->setDescription('This is a temporary campus, should be edited or deleted');
        $campus->setLocation('Cyberspace');
        $campus->setPhone('5551239229');
        $campus->setTimezone('America/Vancouver');
        $em->persist($campus);
        $em->flush();
        $progress->advance();
        
        
        $progress->finish();
    }
}