<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Controller\PearController;
use AppBundle\Entity\PearPrereqExemptionRequests;
use AppBundle\Form\PearPrereqExemptionRequestsType;


/**
 * PrereqExemptionRequests controller.
 *
 */
class PrereqExemptionRequestsController extends PearController
{

    /**
     * Lists all PrereqExemptionRequests entities.
     *
     */
    public function indexAction()
    {
        $this->checkPermission(self::PERMISSION_SUPER);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:PearPrereqExemptionRequests')->findAll();

        return $this->render('AppBundle:PrereqExemptionRequests:index.html.twig', array(
            'entities' => $entities,
            ));
    }

    public function indexByCampusAction($campus_id)
    {
        $campus = $this->getDoctrine()
       ->getRepository('AppBundle:PearCampuses')
       ->find($campus_id);

       if (!$campus) {
            throw $this->createNotFoundException(
                'No campus found for id '.$id
                );
        }

        $this->checkPermission(self::PERMISSION_VIEW, false, null, $campus);

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:PearPrereqExemptionRequests');
        $requests = $repo->createQueryBuilder('r')
                    ->innerJoin('r.user', 'u')
                    ->where('u.campus = :campus')
                    ->setParameter('campus', $campus)
                    ->getQuery()
                    ->getResult();

        return $this->render('AppBundle:PrereqExemptionRequests:index.html.twig', array(
            'entities' => $requests,
            ));
    }


    /**
     * Creates a new PrereqExemptionRequests entity.
     *
     */
    public function createAction(Request $request, $class_id, $user_id)
    {
        $this->checkPermission(self::PERMISSION_CREATE);
        $user = $this->getUser();
        $entity = new PearPrereqExemptionRequests();
        $form = $this->createCreateForm($entity, $class_id, $user_id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $class = $this->getDoctrine()
            ->getRepository('AppBundle:PearClasses')
            ->find($class_id);

            $user = $this->getDoctrine()
            ->getRepository('AppBundle:PearUsers')
            ->find($user_id);
        //int that represents "pending." This is default value.

            $entity->setClass($class);
            $entity->setUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->logAction("Exemption request ".$entity->getId()." has been created for ".$entity->getUser()->getFirstName()." ".$entity->getUser()->getLastName()." by user id ".$user->getId(), self::LOGTYPE_EDIT, $user->getId());
            $this->get('session')->getFlashBag()->add('notice', 'Your exemption request has been recorded. An administrator will contact you.');

            //if logged in user is (super)admin then redirect to admin_show
            //otherwise redirect to homepage
            $loggedInUserRoles = array();
            foreach ($this->getUser()->getRoles() as $role)
            {
              array_push($loggedInUserRoles, $role->getName());
            }
            if (in_array("Admin", $loggedInUserRoles) || in_array("Super Admin", $loggedInUserRoles))
            {
              return $this->redirect($this->generateUrl('_admin_users_show', array('id' => $user_id)));
            }
            else
            {
              return $this->redirect($this->generateUrl('_homepage'));
            }
        }

        return $this->render('AppBundle:PrereqExemptionRequests:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            ));
    }

    /**
     * Creates a form to create a PrereqExemptionRequests entity.
     *
     * @param PrereqExemptionRequests $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(PearPrereqExemptionRequests $entity, $class_id, $user_id)
    {
        $form = $this->createForm(new PearPrereqExemptionRequestsType(), $entity, array(
            'action' => $this->generateUrl('_exemption_request_new_post', array('class_id' => $class_id, 'user_id' => $user_id)),
            'method' => 'POST',
            ));

        return $form;
    }

    /**
     * Displays a form to create a new PrereqExemptionRequests entity.
     *
     */
    public function newAction($class_id, $user_id)
    {
        $this->checkPermission(self::PERMISSION_CREATE);

        $entity = new PearPrereqExemptionRequests();
        //get class to render class name in new form
        $em = $this->getDoctrine()->getManager();
        $className = $em->getRepository('AppBundle:PearClasses')->find($class_id)->getName();

        $form   = $this->createCreateForm($entity, $class_id, $user_id);

        return $this->render('AppBundle:PrereqExemptionRequests:new.html.twig', array(
            'entity' => $entity,
            'className' => $className,
            'form'   => $form->createView(),
            ));
    }

    /**
     * Finds and displays a PrereqExemptionRequests entity.
     *
     */
    public function showAction($id)
    {
        $this->checkPermission(self::PERMISSION_VIEW);

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:PearPrereqExemptionRequests')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PrereqExemptionRequests entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:PrereqExemptionRequests:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            ));
    }

    /**
     * Displays a form to edit an existing PrereqExemptionRequests entity.
     *
     */
    public function editAction($id)
    {
        $this->checkPermission(self::PERMISSION_EDIT);

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:PearPrereqExemptionRequests')->find($id);

        $prereqs = $entity->getClass()->getPrereqs()->toArray();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PrereqExemptionRequests entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:PrereqExemptionRequests:edit.html.twig', array(
            'entity'      => $entity,
            'prereqs'      => $prereqs,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            ));
    }

    /**
    * Creates a form to edit a PrereqExemptionRequests entity.
    *
    * @param PrereqExemptionRequests $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(PearPrereqExemptionRequests $entity)
    {
        $form = $this->createForm(new PearPrereqExemptionRequestsType(), $entity, array(
            'action' => $this->generateUrl('_exemption_request_edit_post', array('id' => $entity->getId())),
            'method' => 'PUT',
            ));

        //$form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing PearPrereqExemptionRequests entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $this->checkPermission(self::PERMISSION_EDIT);
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:PearPrereqExemptionRequests')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PrereqExemptionRequests entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->logAction("Exemption request ".$entity->getId()." updated by user ".$user->getId()."for ".$entity->getUser()->getFirstName()." ".$entity->getUser()->getLastName(), self::LOGTYPE_EDIT, $user->getId());
            $this->get('session')->getFlashBag()->add('notice', 'Exemption request updated');
            return $this->redirect($this->generateUrl('_exemption_request_show', array('id' => $id)));
        }
        $this->get('session')->getFlashBag()->add('notice', 'your update to the exemption request could not
            be processed. Please try again');
        return $this->render('AppBundle:PrereqExemptionRequests:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            ));
    }

    /**
     * Deletes a PrereqExemptionRequests entity.
     *
     */
    public function deleteAction($id)
    {
        $this->checkPermission(self::PERMISSION_DELETE);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:PearPrereqExemptionRequests')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PrereqExemptionRequests entity.');
        }

        $em->remove($entity);
        $em->flush();
        $this->get('session')->getFlashBag()->add('notice', 'Exemption request deleted.');

        return $this->redirect($this->generateUrl('_exemption_request_index'));
    }

    /**
     * Creates a form to delete a PrereqExemptionRequests entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
        ->setAction($this->generateUrl('_exemption_request_delete', array('id' => $id)))
        ->setMethod('DELETE')
        ->add('submit', 'submit', array('label' => 'Delete'))
        ->getForm()
        ;
    }

    public function grantExemptionAction($class_id, $user_id)
    {
        $this->checkPermission(self::PERMISSION_OTHER);

        $em = $this->getDoctrine()->getManager();
        $class = $em->getRepository('AppBundle:PearClasses')->find($class_id);
        $user = $em->getRepository('AppBundle:PearUsers')->find($user_id);

        if (!$class || !$user) {
            throw $this->createNotFoundException('Unable to find class or user entity.');
        }

        if (in_array($class ,$user->getPrereqs()->toArray()))
        {
            $this->get('session')->getFlashBag()->add('notice', 'this user has already taken that class
                or has been granted this exemption previously. If the request is resolved please delete it.');
            return $this->redirect($this->generateUrl('_exemption_request_index'));
        }

        $user->addPrereq($class);
        $em->flush();
        $this->get('session')->getFlashBag()->add('notice', 'Exemption granted!.');

        return $this->redirect($this->generateUrl('_exemption_request_index'));
    }
}
