<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

use AppBundle\Entity\PearUsers;
use AppBundle\Form\Type\RegistrationType;

class RegistrationController extends PearController
{
    public function indexAction() {
        $user = new PearUsers();
        
        $form = $this->createForm(new RegistrationType(), $user,
            array('action' => $this->generateUrl('_registration_apply')));
        
        return $this->render('AppBundle:Registration:index.html.twig', array(
            'form' => $form->createView()));
    }
    
    public function applyAction(Request $request)
    {
        $user = new PearUsers();
        
        $form = $this->createForm(new RegistrationType(), $user,
            array('action' => $this->generateUrl('_registration_apply')));
        
        $form->handleRequest($request);

        if ($form->isValid()) {            
            // Check for a duplicated registered email

            if ($user->getEmail() != null)
            {
                if ($this->getDoctrine()
                    ->getRepository('AppBundle:PearUsers')
                    ->findOneBy(array('email' => $user->getEmail())))
                {
                    $this->get('session')->getFlashBag()->add('notice','An account with the email address \'' . $user->getEmail() . '\' already exists.');
                    goto exit_function;
                }
            }
            
            // Check for a duplicated first name + last name + birthday
            if ($this->getDoctrine()
                ->getRepository('AppBundle:PearUsers')
                ->findOneBy(array(
                    'firstName' => $user->getFirstName(),
                    'lastName' => $user->getLastName(),
                    'birthDate' => $user->getBirthDate())))
            {
                $this->get('session')->getFlashBag()->add('notice','We believe you have already registered an account. Please contact an administrator to complete registration.');
                goto exit_function;
            }
            
            // Check to see if password is password
            if ($user->getPassword() == "password")
            {
                $this->get('session')->getFlashBag()->add('notice','Your password can not be \'password\'!');
                goto exit_function;
            }
            
            // Clean the phone numbers
            
            $user->setPhone(preg_replace("/[^0-9]/","",$user->getPhone()));
            $user->setEmergencyContact(preg_replace("/[^0-9]/","",$user->getEmergencyContact()));
            
            // Encrypt the password
            $encoderFactory = $this->get('security.encoder_factory');
            $encoder = $encoderFactory->getEncoder($user);
            $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
            $user->setPassword($password);
            
            // Fill in the automatic fields
            $user->setLastIp($request->getClientIp());
            $user->setCreated(time());
            $user->setLastLogin(time());
            
            // Give the user BASIC_USER
            $basicUser = $this->getDoctrine()->getRepository('AppBundle:PearRoles')->findOneByRole("ROLE_USER");
            $user->addRole($basicUser);
            
            $user->upload();

            // Create the user
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            
            // Generate a user hash
            $hash = $this->generateUserHash($user);
            
            // Send verfication email
            $mailer = $this->get('mailer');
            $user->setEmailVerfied(true);
            $message = $mailer->createMessage()
            ->setSubject('Pear')
            ->setFrom('school@gobindsarvar.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    'AppBundle:Email:registration.html.twig', array(
                        'name' => $user->getFirstName(),
                        'id' => $user->getId(),
                        'hash' => $hash->getRc4Key(),
                        'hash_time' => $hash->getCreated())
                    ),
                'text/html'
                );
            $mailer->send($message);
            
            // Log the user in
            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->get('security.token_storage')->setToken($token);
            
            // Log the creation
            $this->logAction("Self-registration user ".$user->getId()." created with name ".$user->getFirstName()." ".$user->getLastName(), self::LOGTYPE_EDIT, $user->getId());
            
            // Redirect to thank-you for registering page
            return $this->redirect($this->generateUrl('_login_redirect'));
        }
        
        exit_function:
        return $this->render('AppBundle:Registration:index.html.twig', array(
            'form' => $form->createView()));
    }
    
    public function confirmEmailAction($id, $hash, $hashTime)
    {
        // Get the user that is attempting verify their email
        $user = $this->getDoctrine()
        ->getRepository('AppBundle:PearUsers')
        ->find($id);
        
        // Check to see if the hash is valid
        if ($this->checkUserHash($user, $hash, $hashTime))
        {
            $user->setEmailVerfied(true);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            
            $this->expireUserHash($user);
            
            return $this->redirect($this->generateUrl('_homepage'));
        }
        
        return $this->redirect($this->generateUrl('_homepage'));
    }
}
