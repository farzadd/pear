<?php

namespace AppBundle\Controller\Teacher;

use AppBundle\Controller\PearController;

use AppBundle\Entity\PearClassAssignments;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Form\Type\GlobalAssignmentType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Reponse;


class ReportCardController extends PearController {


    public function reportCardByStudentAction($id)
    {
        $this->checkPermission(self::PERMISSION_VIEW);
        $section  = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);

        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }

        $students = $section->getStudents();

        return $this->render('AppBundle:Teacher:reportCard_student.html.twig', array(
            'section' => $section,
            'students' => $students));
    }

    public function reportCardByStudentDetailAction($id, $student_id)
        {

        $this->checkPermission(self::PERMISSION_VIEW);

        $section  = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
        $student = $this->getDoctrine()->getRepository('AppBundle:PearUsers')->find($student_id);

        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }

        // get student's grade for the section
        $percentage = $this->calcStudentGrade($student_id, $id, false);

        $attendances = $section->getAttendance();
        $attendancesCount = sizeof($attendances);

        $presents = $student->getPresentAttendance();
        $presentCount = 0;
        foreach ($presents as $present) {
            if ($present->getSection()->getId() == $section->getId())
                    $presentCount++;
        }


        $lates = $student->getLateAttendance();
       $lateCount = 0;
        foreach ($lates as $late) {
            if ($late->getSection()->getId() == $section->getId())
                    $lateCount++;
        }

        $absents = $student->getAbsentAttendance();
        $absentCount = 0;
        foreach ($absents as $absent) {
            if ($absent->getSection()->getId() == $section->getId())
                    $absentCount++;
        }


        return $this->render('AppBundle:Teacher:reportCard_studentDetail.html.twig', array(
            'section' => $section,
            'student' => $student,
            'percentage' => $percentage,
            'attendancesCount' => $attendancesCount,
            'presentCount' =>$presentCount,
            'lateCount' => $lateCount,
            'absentCount' => $absentCount));
        }


    public function reportCardBySectionAction($id)
        {
        $this->checkPermission(self::PERMISSION_VIEW);
        $section  = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);

        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }

        $students = $section->getStudents();
        $em = $this->getDoctrine()->getManager();

        $studentReportCard = array();
        $overallMark = 0;
        $num_student = sizeof($students);
        $attendancesCount = 0;

        foreach ($students as $student) {
        $reportCard = array();


        $percentage = $this->calcStudentGrade($student->getId(), $id, false);

        $overallMark = $overallMark + $percentage;
        $reportCard["percentage"] = $percentage;

        $attendances = $section->getAttendance();
        $attendancesCount = sizeof($attendances);

        $presents = $student->getPresentAttendance();
        $presentCount = 0;
        foreach ($presents as $present) {
            if ($present->getSection()->getId() == $section->getId())
                    $presentCount++;
        }


        $lates = $student->getLateAttendance();
       $lateCount = 0;
        foreach ($lates as $late) {
            if ($late->getSection()->getId() == $section->getId())
                    $lateCount++;
        }

        $absents = $student->getAbsentAttendance();
        $absentCount = 0;
        foreach ($absents as $absent) {
            if ($absent->getSection()->getId() == $section->getId())
                    $absentCount++;
        }
        $reportCard["attendancesCount"] = $attendancesCount;
        $reportCard["presentCount"] = $presentCount;
        $reportCard["lateCount"] = $lateCount;
        $reportCard["absentCount"] = $absentCount;
        $studentReportCard[$student->getId()] = $reportCard;

        }

        if($num_student > 0) {
            $overallMark = $overallMark / $num_student;
        }

        return $this->render('AppBundle:Teacher:reportCard_section.html.twig', array(
            'section' => $section,
            'students' => $students,
            'studentReportCard' => $studentReportCard,
            'attendancesCount' => $attendancesCount,
            'overallMark' => $overallMark));
        }

}
