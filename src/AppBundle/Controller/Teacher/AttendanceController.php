<?php

namespace AppBundle\Controller\Teacher;

use AppBundle\Controller\PearController;
use AppBundle\Entity\PearUsers;
use AppBundle\Entity\PearClasses;
use AppBundle\Entity\PearDepartments;
use AppBundle\Entity\PearAttendance;
use AppBundle\Entity\PearTeacherTeaches;
use AppBundle\Entity\PearClassAssignments;
use AppBundle\Entity\PearUserGrades;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Form\Type\AttendanceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Reponse;


class AttendanceController extends PearController {

// render attendance page
    public function attendanceAction($id) {
        $this->checkPermission(self::PERMISSION_VIEW);
        
        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);

        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                ->select('a.id, a.date')
                ->from('AppBundle:PearAttendance', 'a')
                ->where('a.section = :currentsection')
                ->setParameter('currentsection', $section->getId())
                ->orderBy('a.date', 'DESC')
                ->getQuery();

        $attendances = $query->getResult();
        return $this->render('AppBundle:Teacher:attendanceList.html.twig', array('section' => $section, 'attendances' => $attendances
        ));
    }
    
    public function attendanceMassiveAction($id) {
        $this->checkPermission(self::PERMISSION_CREATE);

        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);

        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        return $this->render('AppBundle:Teacher:attendanceMassive.html.twig', array('section' => $section,
        ));
    }

    public function attendanceMassivePostAction($id) {
        $this->checkPermission(self::PERMISSION_CREATE);

        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
        
        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        if (isset($_POST['multiDatepicker']) && ($_POST['multiDatepicker'] != "" )) {
            $dates = $_POST['multiDatepicker'];
        } else {
            $this->get('session')->getFlashBag()->add('notice', 'Please choose date');
            return $this->render('AppBundle:Teacher:attendanceMassive.html.twig', array(
                        'section' => $section));
        }
        $dates = str_replace(' ', '', $dates);
        $datesArray = explode(',', $dates);
        
            return $this->render('AppBundle:Teacher:attendanceMassiveUpload.html.twig', 
                    array('section' => $section, 'datesArray' => $datesArray));
        
    }
    
    public function attendanceUploadMassiveAction($id, $dates) {
        $this->checkPermission(self::PERMISSION_CREATE);

        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);

        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        return $this->render('AppBundle:Teacher:attendanceMassiveUpload.html.twig', array('section' => $section,
        ));
    }

    public function attendanceUploadMassivePostAction($id) {
        $this->checkPermission(self::PERMISSION_CREATE);

        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
        
        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        $students = $section->getStudents();
        $dates = $_POST['date'];
        
        foreach($dates as $date) {             
        // check if the attendance for specific date exists
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                ->select('a.id')
                ->from('AppBundle:PearAttendance', 'a')
                ->where('a.section = :currentsection')
                ->andwhere('a.date = :date')
                ->setParameter('currentsection', $section->getId())
                ->setParameter('date', $date)
                ->getQuery();

        //remove existing attendance 
        if ($query->getResult() != null) {
            $aid = $query->getResult();
            $attendance = $this->getDoctrine()->getRepository('AppBundle:PearAttendance')->find((reset($aid)));
            $em = $this->getDoctrine()->getManager();
            $em->remove($attendance);
            $em->flush();
        }
        
        // add new attendance   
        $attendance = new PearAttendance();
        $attendance->setSection($section);
        $attendance->setDate($date);

        // find students's attendance in that date
        $attendances = $_POST[$date];
        
            foreach ($students as $student) {

                if (isset($attendances[$student->getId()]) && $attendances[$student->getId()] == 'late') {
                    $attendance->addLateUser($student);
                } else if (isset($attendances[$student->getId()]) && $attendances[$student->getId()] == 'absent') {
                    $attendance->addAbsentUser($student);
                } else if (isset($attendances[$student->getId()]) && $attendances[$student->getId()] == 'present') {
                    $attendance->addPresentUser($student);
                } 
                $em = $this->getDoctrine()->getManager();
                $em->persist($attendance);
                $em->flush();
                
                
                }
        
                $this->logAction("Attendance for ".$date." has been added in ".$section->getClass()->getName().", Section ".$section->getId()." through massive upload", self::LOGTYPE_CREATE, $attendance->getId());
                
        
        }
                $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'Massive attendance uploaded successfully!'
                );
            return $this->redirect($this->generateUrl(
                                    '_teacher_section', array('id' => $id)
            ));
        }
    
    public function attendanceBlankAction($id)
    {
        $this->checkPermission(self::PERMISSION_VIEW);
        
        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);

        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        return $this->render('AppBundle:Teacher:attendanceBlank.html.twig', array('section' => $section,
        ));
    }

    /**
     * Input: $id
     * Output: Attendance Page
     * Render Attendance Page for that section.
     * @param type $id
     * @return type
     */
    public function attendanceAddAction($id) {
        $this->checkPermission(self::PERMISSION_CREATE);

        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);

        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        return $this->render('AppBundle:Teacher:attendanceAdd.html.twig', array('section' => $section,
        ));
    }

    public function attendanceAddPostAction($id) {
        $this->checkPermission(self::PERMISSION_CREATE);

        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
        
        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        $students = $section->getStudents();

        if (isset($_POST['datepicker']) && ($_POST['datepicker'] != "" )) {
            $date = $_POST['datepicker'];
        } else {
            $this->get('session')->getFlashBag()->add('notice', 'Add date');
            return $this->render('AppBundle:Teacher:attendanceAdd.html.twig', array(
                        'section' => $section));
        }

        // check if the attendance for specific date exists
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                ->select('a.id')
                ->from('AppBundle:PearAttendance', 'a')
                ->where('a.section = :currentsection')
                ->andwhere('a.date = :date')
                ->setParameter('currentsection', $section->getId())
                ->setParameter('date', $date)
                ->getQuery();

        if ($query->getResult()) {
            $this->get('session')->getFlashBag()->add('notice', 'Attendance already exists for this date!');
            return $this->render('AppBundle:Teacher:attendanceAdd.html.twig', array(
                        'section' => $section));
        } else {
            $attendance = new PearAttendance();
            $attendance->setSection($section);
            $attendance->setDate($date);

            foreach ($students as $student) {

                if (isset($_POST[$student->getId()]) && $_POST[$student->getId()] == 'true') {
                    $attendance->addLateUser($student);
                } else if (isset($_POST[$student->getId()]) && $_POST[$student->getId()] == 'false') {
                    $attendance->addAbsentUser($student);
                } else if (isset($_POST[$student->getId()]) && $_POST[$student->getId()] == 'present') {
                    $attendance->addPresentUser($student);
                } else {
                    $this->get('session')->getFlashBag()->add('notice', 'Please enter attendance for every student!');
                    return $this->render('AppBundle:Teacher:attendanceAdd.html.twig', array(
                                'section' => $section));
                }
                $em = $this->getDoctrine()->getManager();
                $em->persist($attendance);
                $em->flush();
                
                
                }
                $this->logAction("Attendance for ".$attendance->getDate()." has been added in ".$section->getClass()->getName().", Section ".$section->getId(), self::LOGTYPE_CREATE, $attendance->getId());
                $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'Attendance has been added successfully!'
                );

            return $this->redirect($this->generateUrl(
                                    '_teacher_section', array('id' => $id)
            ));
        }
    }
    
    // input: $id = section ID / $aid = attendance id /$sid = user id
    // output: 
    public function attendanceEditAddPostAction($id, $aid, $sid) {
        $this->checkPermission(self::PERMISSION_CREATE);
        $this->checkPermission(self::PERMISSION_EDIT);
        
        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
        
        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        $attendance = $this->getDoctrine()->getRepository('AppBundle:PearAttendance')->find($aid);
        $student = $this->getDoctrine()->getRepository('AppBundle:PearUsers')->find($sid);
        if (isset($_POST['new'])) {
            $status = $_POST['new'];
            if ($status == "present"){
                $attendance->addPresentUser($student);
            }
            else if ($status == "true"){
                $attendance->addLateUser($student);
            }
            else if ($status == "false"){
                $attendance->addAbsentUser($student);
            }
            $em = $this->getDoctrine()->getManager();
                $em->persist($attendance);
                $em->flush();
                $this->logAction("Attendance for ".$attendance->getDate()." has been updated in ".$section->getClass()->getName().", Section ".$section->getId(), self::LOGTYPE_EDIT, $attendance->getId());
                return $this->redirect($this->generateUrl('_teacher_attendance_edit', array('id' => $id, 'aid' => $aid)));
        }
        else {
            $this->get('session')->getFlashBag()->add('notice', 'Please enter attendance for new student!');
            return $this->redirect($this->generateUrl('_teacher_attendance_edit', array('id' => $id, 'aid' => $aid)));
        }
    }

    public function attendanceDetailAction($id, $aid) {
        $this->checkPermission(self::PERMISSION_VIEW);
        
        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
        
        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        $attendance = $this->getDoctrine()->getRepository('AppBundle:PearAttendance')->find($aid);

        $lateUsers = $attendance->getLateUsers();
        $absentUsers = $attendance->getAbsentUsers();
        $presentUsers = $attendance->getPresentUsers();

        return $this->render('AppBundle:Teacher:attendanceDetail.html.twig', array('section' => $section, 'attendance' => $attendance,
                    'lateUsers' => $lateUsers, 'absentUsers' => $absentUsers, 'presentUsers' => $presentUsers
        ));
    }

    public function attendanceEditAction($id, $aid) {
        $this->checkPermission(self::PERMISSION_EDIT);
        $this->checkPermission(self::PERMISSION_CREATE);
        
        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
        
        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        $attendance = $this->getDoctrine()->getRepository('AppBundle:PearAttendance')->find($aid);
        $array_attendance = array();
        
        $students = $section->getStudents();
        $lateUsers = $attendance->getLateUsers();
        $absentUsers = $attendance->getAbsentUsers();
        $presentUsers = $attendance->getPresentUsers();
        
        foreach($students as $student) {
            $empty = false;
            $found = false;
            foreach($presentUsers as $presentUser) {
                if($presentUser->getId() == $student->getId()){
                    $array_attendance[$student->getId()] = "present";
                    $empty = true;
                    $found = true;
                    break;
                    
                }
            }
            foreach($lateUsers as $lateUser) {
                if($found == true){
                    break;
                }
                if($lateUser->getId() == $student->getId()){
                    $array_attendance[$student->getId()] = "late";
                    $empty = true;
                    $found = true;
                    break;
                    
                }
            }
            
            foreach($absentUsers as $absentUser) {
                if($found == true){
                    break;
                }
                if($absentUser->getId() == $student->getId()){
                    $array_attendance[$student->getId()] = "absent";
                    $empty = true;
                    $found = true;
                    break;
                    
                 }
                 
             }
        }

        return $this->render('AppBundle:Teacher:attendanceEdit.html.twig', array('section' => $section, 'attendance' => $attendance,
                    'array_attendance' => $array_attendance
        ));
    }

    public function attendanceEditPostAction($id, $aid) {
        $this->checkPermission(self::PERMISSION_CREATE);
        $this->checkPermission(self::PERMISSION_EDIT);
        
        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
        
        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        $attendance = $this->getDoctrine()->getRepository('AppBundle:PearAttendance')->find($aid);

        $students = $section->getStudents();

        $lateUsers = $attendance->getLateUsers();
        $absentUsers = $attendance->getAbsentUsers();
        $presentUsers = $attendance->getPresentUsers();
        
        foreach ($lateUsers as $lateStudent) {
            if (isset($_POST[$lateStudent->getId()]) && $_POST[$lateStudent->getId()] == "false") {
                $attendance->removeLateUser($lateStudent);
                $attendance->addAbsentUser($lateStudent);
                
            } else if (isset($_POST[$lateStudent->getId()]) && $_POST[$lateStudent->getId()] == "present") {
                $attendance->removeLateUser($lateStudent);
                $attendance->addPresentUser($lateStudent);
            } 
            $em = $this->getDoctrine()->getManager();
            $em->persist($attendance);
            $em->flush();
        }
        foreach ($absentUsers as $absentStudent) {
            if (isset($_POST[$absentStudent->getId()]) && $_POST[$absentStudent->getId()] == "true") {
                $attendance->removeAbsentUser($absentStudent);
                $attendance->addLateUser($absentStudent);
            } else if (isset($_POST[$absentStudent->getId()]) && $_POST[$absentStudent->getId()] == "present") {
                $attendance->removeAbsentUser($absentStudent);
                $attendance->addPresentUser($absentStudent);
            } 
            $em = $this->getDoctrine()->getManager();
            $em->persist($attendance);
            $em->flush();
        }
        foreach ($presentUsers as $presentUser) {
            if (isset($_POST[$presentUser->getId()]) && $_POST[$presentUser->getId()] == "true") {
                $attendance->removePresentUser($presentUser);
                $attendance->addLateUser($presentUser);
            } else if (isset($_POST[$presentUser->getId()]) && $_POST[$presentUser->getId()] == "false") {
                $attendance->removePresentUser($presentUser);
                $attendance->addAbsentUser($presentUser);
            } 
            $em = $this->getDoctrine()->getManager();
            $em->persist($attendance);
            $em->flush();
            
        }
        $flashMessage = $this->get('session')->getFlashBag()->add(
            'notice',
            'Attendance has been edited successfully'
            );
        $this->logAction("Attendance for ".$attendance->getDate()." has been updated in ".$section->getClass()->getName().", Section ".$section->getId(), self::LOGTYPE_EDIT, $attendance->getId());
        return $this->redirect($this->generateUrl(
                                '_teacher_attendance_detail', array('id' => $id, 'aid' => $aid)
        ));
    }
    
    public function attendanceDeleteAction($id, $aid) {
        $this->checkPermission(self::PERMISSION_DELETE);
        
        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
        
        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        $attendance = $this->getDoctrine()->getRepository('AppBundle:PearAttendance')->find($aid);
        $em = $this->getDoctrine()->getManager();
        
        $em->remove($attendance);
       
        $this->logAction("Attendance for ".$attendance->getDate()." has been deleted in ".$section->getClass()->getName().", Section ".$section->getId(), self::LOGTYPE_DELETE, $attendance->getId());

        $em->flush();
        
        $flashMessage = $this->get('session')->getFlashBag()->add(
            'notice',
            'Attendance Deleted!'
        );

        //return $this->indexAction();
        return $this->redirect($this->generateUrl(
            '_teacher_section', array('id' => $section->getId())));
   }
}
