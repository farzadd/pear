<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DocumentsController
 *
 * @author minwooyoon
 */

namespace AppBundle\Controller\Teacher;

use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\PearDocuments;
use AppBundle\Form\PearDocumentsType;
use AppBundle\Controller\PearController;
use Symfony\Component\Filesystem\Filesystem;

class DocumentsController extends PearController {
    /*
     * Input:
     * Output:
     * Generate Doc Import Form
     */

    public function indexAction($id) {
        $this->checkPermission(self::PERMISSION_CREATE);

        $class = $this->getDoctrine()
        ->getRepository('AppBundle:PearClasses')
        ->find($id);

        $document = new PearDocuments();
        $document->setClass($class);

        $form = $this->createForm(new PearDocumentsType($class->getAssignments()), $document, array('action' => $this->generateUrl('_documents_post', array ('id' => $id))));

        return $this->render('AppBundle:Teacher:Documents/upload.html.twig', array(
                    'form' => $form->createView()));
    }

    public function documentPostAction(Request $request, $id) {
        $this->checkPermission(self::PERMISSION_CREATE);

        $class = $this->getDoctrine()
        ->getRepository('AppBundle:PearClasses')
        ->find($id);

        $document = new PearDocuments();
        $document->setClass($class);

        $form = $this->createForm(new PearDocumentsType($class->getAssignments()), $document, array('action' => $this->generateUrl('_documents_post', array ('id' => $id))));
        $form->handleRequest($request);

        $req_name = $form->get('name')->getData();

        if ($form->isValid()) {

            $document->upload();
            $em = $this->getDoctrine()->getManager();
            $em->persist($document);

            $em->flush();
            $this->logAction("File".$document->getName()."with id".$document->getId()." has been uploaded", self::LOGTYPE_CREATE, $document->getId());

            return $this->redirect($this->generateUrl('_documents_list'));
        }
        return $this->render('AppBundle:Teacher:Documents/upload.html.twig', array(
                    'form' => $form->createView()));
    }

    public function documentListAction() {
        $this->checkPermission(self::PERMISSION_CREATE);

        //show all documents if user is superadmin,
        $roles = array();

        foreach ($this->getUser()->getRoles() as $role) {
            array_push($roles, $role->getRole());
        }

        if (in_array('ROLE_SUPERADMIN', $roles)) {
            $sections = $this->getDoctrine()
            ->getRepository('AppBundle:PearClassSections')
            ->findAll();
        }

        //all docs for campus if admin
        else if ((in_array('ROLE_ADMIN', $roles)) || (in_array('ROLE_CAMPUSADMIN', $roles))) {
            //select all section
            //where section is in current term or enrollment term
            //and section is at same campus as admin
            $sql = 'SELECT s.*
            FROM pear.pear_class_sections s, pear.pear_campus_terms t, pear.pear_campuses c
            WHERE s.term_id=t.id AND t.campus_id=c.id AND t.campus_id=:id AND (c.activeTerm=s.term_id OR c.enrollment_term=s.term_id)';

            $stmt = $this->getDoctrine()->getManager()
            ->getConnection()->prepare($sql);

            $stmt->bindValue('id', $this->getUser()->getCampus()->getId());

            $stmt->execute();

            $result = $stmt->fetchAll();
            $sections = array();

            foreach ($result as $sec) {
                $sec = $this->getDoctrine()
                ->getRepository('AppBundle:PearClassSections')
                ->find($sec['id']);

                array_push($sections, $sec);
            }
        }

        //else if teacher, docs for his/her classes only
        else {
            $sections = $this->getUser()->getTeaches();
        }

        //now get classes for sections
        $classes = array();

            foreach ($sections as $sec)
            {
                if(sizeof($sec->getClass()->getDocuments())>0)
                {
                    if(!isset($classes[$sec->getClass()->getName()]))
                    {
                        $classes[$sec->getClass()->getName()] = $sec->getClass();
                    }
                }
            }

        return $this->render('AppBundle:Teacher:Documents/upload_list.html.twig', array(
                    'classes' => $classes
        ));
    }

    public function deleteAction($doc_id) {
        $document = $this->getDoctrine()->getRepository('AppBundle:PearDocuments')->find($doc_id);
        $this->logAction("Deleted Document ".$document->getName()."with id ".$document->getId(), self::LOGTYPE_DELETE, $document->getId());

        $document->remove();
        $em = $this->getDoctrine()->getManager();
        $em->remove($document);
        $em->flush();

        $flashMessage = $this->get('session')->getFlashBag()->add(
            'notice',
            'Document has been deleted!'
            );

        return $this->redirect($this->generateURL('_documents_list'));
    }

}
