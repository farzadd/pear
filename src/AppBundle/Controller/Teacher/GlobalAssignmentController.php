<?php

namespace AppBundle\Controller\Teacher;

use AppBundle\Controller\PearController;

use AppBundle\Entity\PearClassAssignments;
use AppBundle\Entity\PearClassSections;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Form\Type\GlobalAssignmentType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Reponse;


class GlobalAssignmentController extends PearController {

    
    public function globalAssignmentAddAction($id) 
    {
        $this->checkPermission(self::PERMISSION_CREATE);
        $section  = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
        
        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        $globalAssignments = new PearClassAssignments();

        $form = $this->createForm(new GlobalAssignmentType(), $globalAssignments,
            array('action' => $this->generateUrl('_teacher_global_assignment_add_post', array('id' => $id))));
        
        return $this->render('AppBundle:Teacher:globalAssignmentAdd.html.twig', array(
            'section' => $section,
            'form' => $form->createView()));
    }
        
    public function globalAssignmentAddPostAction(Request $request, $id) {
        $this->checkPermission(self::PERMISSION_CREATE);
        $section  = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
        
        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        $globalAssignments = new PearClassAssignments();
        $globalAssignments->setCreated(date("Ymd"));
        $globalAssignments->setCreatedBy($this->getUser());
        $globalAssignments->setClass($section->getClass());
        $globalAssignments->addSection($section);
        
        $form = $this->createForm(new GlobalAssignmentType(), $globalAssignments,
            array('action' => $this->generateUrl('_teacher_global_assignment_add_post', array('id' => $id))));
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            if ($globalAssignments->getWeight() < 0 || $globalAssignments->getWeight() > 10){
                $flashMessage = $this->get('session')->getFlashBag()->add(
                'error',
                'Weight has to be between 0 - 10'
                );
                
                return $this->render('AppBundle:Teacher:globalAssignmentAdd.html.twig', array(
            'section' => $section,
            'form' => $form->createView()));
                
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($globalAssignments);
            $em->flush();
            $this->logAction("Global assignment".$globalAssignments->getName()." has been created in ".$globalAssignments->getClass()->getName(), self::LOGTYPE_CREATE, $globalAssignments->getId());
            $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'New global assignment has been added'
                );
        }
            return $this->redirect($this->generateUrl(
                                    '_teacher_section', array('id' => $id)
            ));
    }          
        // input: section id $id, assignment id $aid
    public function globalAssignmentEditAction($id, $aid) 
        {
        $this->checkPermission(self::PERMISSION_CREATE);
        $this->checkPermission(self::PERMISSION_EDIT);
        
        $section  = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
        
        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        $globalAssignments = $this->getDoctrine()->getRepository('AppBundle:PearClassAssignments')->find($aid);

        $form = $this->createForm(new GlobalAssignmentType(), $globalAssignments,
            array('action' => $this->generateUrl('_teacher_global_assignment_edit_post', array('id' => $id, 'aid' => $aid))));
        
        return $this->render('AppBundle:Teacher:globalAssignmentEdit.html.twig', array(
            'section' => $section,
            'form' => $form->createView()));
        }
        
    public function globalAssignmentEditPostAction(Request $request, $id, $aid) {
        $this->checkPermission(self::PERMISSION_CREATE);
        $this->checkPermission(self::PERMISSION_EDIT);
        
        $section  = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
        
        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        $globalAssignments = $this->getDoctrine()->getRepository('AppBundle:PearClassAssignments')->find($aid);
        
        $form = $this->createForm(new GlobalAssignmentType(), $globalAssignments,
            array('action' => $this->generateUrl('_teacher_global_assignment_edit_post', array('id' => $id, 'aid' => $aid))));
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            if ($globalAssignments->getWeight() < 0 || $globalAssignments->getWeight() > 10){
                $flashMessage = $this->get('session')->getFlashBag()->add(
                'error',
                'Weight has to be between 0 - 10'
                );
                
                return $this->render('AppBundle:Teacher:globalAssignmentEdit.html.twig', array(
            'section' => $section,
            'form' => $form->createView()));
                
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($globalAssignments);
            $em->flush();
            $this->logAction("Global assignment".$globalAssignments->getName()." has been edited", self::LOGTYPE_EDIT, $globalAssignments->getId());
            $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'Global assignment has been edited successfully'
                );
            
        }
        
        return $this->redirect($this->generateUrl('_teacher_section', array('id' => $id)));
    }          
}
