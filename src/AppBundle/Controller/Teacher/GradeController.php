<?php

namespace AppBundle\Controller\Teacher;

use AppBundle\Controller\PearController;
use AppBundle\Entity\PearUsers;
use AppBundle\Entity\PearClasses;
use AppBundle\Entity\PearDepartments;
use AppBundle\Entity\PearAttendance;
use AppBundle\Entity\PearTeacherTeaches;
use AppBundle\Entity\PearClassAssignments;
use AppBundle\Entity\PearUserGrades;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Form\Type\AttendanceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Reponse;


class GradeController extends PearController {
    // view grade for specific assignment
    public function assignmentGradeAction($id, $assignment_id)
    {
        $this->checkPermission(self::PERMISSION_VIEW);

        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);

        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }

        $assignment = $this->getDoctrine()->getRepository('AppBundle:PearClassAssignments')->find($assignment_id);
        return $this->render('AppBundle:Teacher:assignmentGrade.html.twig',
        array(
            'section' => $section,
            'assignment' => $assignment
        ));
    }

    //overall grade for every assignment on the section
    public function overallGradeAction($id)
    {
        $this->checkPermission(self::PERMISSION_VIEW);

        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);

        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }

        $count = 0;
        foreach ($section->getAssignments() as $assignment) {
            $count += 1;
        }

        return $this->render('AppBundle:Teacher:overallGrade.html.twig',
        array(
            'section'=>$section,
            'count'=>$count
        ));
    }

    public function gradeAddAction($id, $assignment_id)
    {
        $this->checkPermission(self::PERMISSION_CREATE);

        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);

        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }

        $assignment = $this->getDoctrine()->getRepository('AppBundle:PearClassAssignments')->find($assignment_id);
        return $this->render('AppBundle:Teacher:gradeAdd.html.twig',
        array(
            'section' => $section,
            'assignment' => $assignment
        ));
    }

    public function gradeAddPostAction($id, $assignment_id)
    {
        // check permissions
        $this->checkPermission(self::PERMISSION_CREATE);
        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }

        $assignment = $this->getDoctrine()->getRepository('AppBundle:PearClassAssignments')->find($assignment_id);
        $students = $section->getStudents();
        $em = $this->getDoctrine()->getManager();

        // students getting first-time grades for this assignment
        $this->gradeUnmarkedAction($students, $section, $assignment, $em, false);

        $this->logAction("Grade has been added for assignment ".$assignment->getName(), self::LOGTYPE_CREATE, $assignment->getId());
        $flashMessage = $this->get('session')->getFlashBag()->add(
        'notice',
        'Grade has been added successfully'
    );
    return $this->redirect($this->generateUrl(
    '_teacher_section', array('id' => $id)
));
}

public function gradeEditAction($id, $assignment_id)
{
    $this->checkPermission(self::PERMISSION_EDIT);

    $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);

    if (!$this->checkPermission(self::PERMISSION_SUPER, true))
    {
        $this->checkTeacherPermission($this->getUser(), $section);
    }

    $assignment = $this->getDoctrine()->getRepository('AppBundle:PearClassAssignments')->find($assignment_id);

    // return only grades of for this section & this assignment
    $em = $this->getDoctrine()->getManager();
    $gradesQuery = $em->createQueryBuilder()
    ->select('a')
    ->from('AppBundle:PearUserGrades', 'a')
    ->where('a.section = :currentsection')
    ->andwhere('a.assignment = :assignment')
    ->setParameter('currentsection', $section->getId())
    ->setParameter('assignment', $assignment->getId())
    ->getQuery();

    $grades = $gradesQuery->getResult();

    // students who are in this section but don't yet have a grade for assignment
    $studentsQuery = 'SELECT u.id, first_name, last_name
    FROM pear.pear_users u, pear.pear_class_sections s, pear.pear_user_enrolls e
    WHERE u.id = e.pearusers_id AND e.pearclasssections_id = s.id AND s.id = :section AND u.id NOT IN
        (SELECT u2.id
        FROM pear.pear_users u2, pear.pear_user_grades g
        WHERE u2.id = g.user_id AND g.section_id = :section AND g.assignment_id = :assignment)';

    $stmt = $this->getDoctrine()->getManager()
            ->getConnection()->prepare($studentsQuery);

    // bind parameters
    $stmt->bindValue('section', $section->getId());
    $stmt->bindValue('assignment', $assignment->getId());

    // execute query
    $stmt->execute();

    $ungradedStudents = $stmt->fetchAll();

    return $this->render('AppBundle:Teacher:gradeEdit.html.twig',
    array(
        'section' => $section,
        'assignment' =>$assignment,
        'grades' => $grades,
        'ungradedStudents' => $ungradedStudents
    ));
}

public function gradeEditPostAction($id, $assignment_id) {

    // check permissions
    $this->checkPermission(self::PERMISSION_EDIT);
    $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
    if (!$this->checkPermission(self::PERMISSION_SUPER, true))
    {
        $this->checkTeacherPermission($this->getUser(), $section);
    }

    $assignment = $this->getDoctrine()->getRepository('AppBundle:PearClassAssignments')->find($assignment_id);
    $students = $section->getStudents();
    $em = $this->getDoctrine()->getManager();

    // get all grades for this assignment in this section
    $query = $em->createQueryBuilder()
    ->select('a')
    ->from('AppBundle:PearUserGrades', 'a')
    ->where('a.section = :currentsection')
    ->andwhere('a.assignment = :assignment')
    ->setParameter('currentsection', $section->getId())
    ->setParameter('assignment', $assignment->getId())
    //->orderBy('a.user')
    ->getQuery();

    $grades = $query->getResult();

    // update any grades for students who already had mark for this assignment
    foreach ($grades as $grade) {

        // existing grade becomes excused
        if (isset($_POST['excused'.$grade->getUser()->getId()])) {
            $grade->setGrade(0); // placeholder value for excused grades
            $grade->setExcused(true);
            $em->persist($grade);
            $em->flush();
        }

        // grade updated for this assignment
        elseif (isset($_POST[$grade->getUser()->getId()])) {
            $grade->setGrade($_POST[$grade->getUser()->getId()]);
            $em->persist($grade);
            $em->flush();
        }
    }

    // students getting first-time grades for this assignment
    $this->gradeUnmarkedAction($students, $section, $assignment, $em, true);

    $this->logAction("Grades have been edited for assignmnet ".$assignment->getName(), self::LOGTYPE_EDIT, $assignment->getId());
    $flashMessage = $this->get('session')->getFlashBag()->add(
    'notice',
    'Grade has been edited successfully'
);
return $this->redirect($this->generateUrl(
'_teacher_section', array('id' => $id)
));
}

// assign grades to student who are recieving mark on $assignment for the first time
// $students, $section, $assignment are Doctrine Entities
// $em is EntityManager
// $edit is bool: true if called from editPost, false if called from createPost
private function gradeUnmarkedAction($students, $section, $assignment, $em, $edit) {

    // assign values to tags that concat with student ids to identify DOM elements
    if ($edit) {
        $excusedTag = 'excusedNew';
        $tag = 'new';
    }

    else {
        $excusedTag = 'excused';
        $tag = '';
    }

    foreach ($students as $student) {

        // getting a mark for first time & it's excused
        if (isset($_POST[$excusedTag.$student->getId()])) {
            $grade = new PearUserGrades();
            $grade->setSection($section);
            $grade->setAssignment($assignment);
            $grade->setExcused(true);
            $grade->setGrade(0); // placeholder value for excused grades
            $grade->setUser($student);
            $em->persist($grade);
            $em->flush();
        }

        // getting mark for first time and it's NOT excused
        elseif (isset($_POST[$tag.$student->getId()])) {
            $grade = new PearUserGrades();
            $grade->setSection($section);
            $grade->setAssignment($assignment);
            $grade->setExcused(false);
            $grade->setGrade($_POST[$tag.$student->getId()]);
            $grade->setUser($student);
            $em->persist($grade);
            $em->flush();
        }
    }
}
}
