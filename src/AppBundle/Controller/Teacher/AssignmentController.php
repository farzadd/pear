<?php

namespace AppBundle\Controller\Teacher;

use AppBundle\Controller\PearController;
use AppBundle\Entity\PearUsers;
use AppBundle\Entity\PearClasses;
use AppBundle\Entity\PearDepartments;
use AppBundle\Entity\PearAttendance;
use AppBundle\Entity\PearTeacherTeaches;
use AppBundle\Entity\PearClassAssignments;
use AppBundle\Entity\PearUserGrades;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Form\Type\AttendanceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Reponse;

class AssignmentController extends PearController {

    // add assignment from list of global assignment
    public function assignmentAddAction($id) {
        $this->checkPermission(self::PERMISSION_CREATE);
        
        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
        
        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        $globalAssignments = $section->getClass()->getAssignments()->toArray();
        $sectionAssignments = $section->getAssignments()->toArray();
        $remainingAssignments = array();
        foreach ($globalAssignments as $assignment) {
            if (in_array($assignment, $sectionAssignments)) {
                continue;
            } else {
                array_push($remainingAssignments, $assignment);
            }
        }
        

        return $this->render('AppBundle:Teacher:assignmentAdd.html.twig', array(
                    'section' => $section,
                    'remainingAssignments' => $remainingAssignments
        ));
    }

    // handle add assigment
    public function assignmentAddPostAction($id) {
        $this->checkPermission(self::PERMISSION_CREATE);
        
        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);

        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        $request = $this->getRequest()->request->get("assign_id");

        foreach ($request as $assignment_id) {
            $assignment = $this->getDoctrine()->getRepository('AppBundle:PearClassAssignments')->find($assignment_id);
            $section->addAssignment($assignment);
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($section);
        $em->flush();
        
        $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'Assignment has been added to class successfully!'
                );
        $this->logAction("Assignment ".$assignment->getName()." has been added in section ".$section->getId(), self::LOGTYPE_CREATE, $section->getId());
        
        return $this->redirect($this->generateUrl(
                                '_teacher_section', array('id' => $id)
        ));
    }  
    
    /*
     * Remove assignment from that section
     * Params: section_id, assignment_id
     */
    public function assignmentRemoveAction($section_id, $assignment_id) {
        $this->checkPermission(self::PERMISSION_DELETE);
        
        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($section_id);
        $assignment = $this->getDoctrine()->getRepository('AppBundle:PearClassAssignments')->find($assignment_id);
        
        $section->removeAssignment($assignment);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($section);
        $em->flush();
        
        $this->addFlash('notice', 'Assignment '.$assignment->getName().' has been removed from section '.$section->getId());
        $this->logAction('Assignment '.$assignment->getName().' has been removed from section '.$section->getId(), self::LOGTYPE_DELETE, $section->getId());
        
        return $this->redirect($this->generateUrl('_teacher_section', array('id'=>$section_id)));      
    }

}

