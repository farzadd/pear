<?php

namespace AppBundle\Controller\Teacher;

use AppBundle\Controller\PearController;
use AppBundle\Entity\PearUsers;
use AppBundle\Entity\PearClasses;
use AppBundle\Entity\PearDepartments;
use AppBundle\Entity\PearAttendance;
use AppBundle\Entity\PearTeacherTeaches;
use AppBundle\Entity\PearClassAssignments;
use AppBundle\Entity\PearUserGrades;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Form\Type\AttendanceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Reponse;
use DateTime;
use DateTimeZone;

/**
 * Description of MainController
 *
 * @author minwooyoon
 */
class MainController extends PearController {

    /**
     * Input:
     * Output:
     * Init dashboard page for teachers
     * @return Teacher Main Page
     */
    public function indexAction() {
        $this->checkPermission(self::PERMISSION_VIEW);
        
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $sections = $user->getTeaches();
        $taSections = $user->getTeachingAssists();
        $sections = new ArrayCollection(array_merge($sections->toArray(), $taSections->toArray()));
        $currentYear = array();
        foreach ($sections as $section) {
            if ($section->getTerm() == $section->getTerm()->getCampus()->getEnrollmentTerm()) {
                array_push($currentYear, $section);
                $em = $this->getDoctrine()->getManager();
                $query = $em->createQueryBuilder()
                        ->select('MAX(a.date)')
                        ->from('AppBundle:PearAttendance', 'a')
                        ->where('a.section = :currentsection')
                        ->setParameter('currentsection', $section->getId())
                        ->orderBy('a.date', 'DESC')
                        ->getQuery();
                $attendances = $query->getResult();
            }

            
            if(!empty($attendances)){
            foreach ($attendances as $attendance) {
                if (!empty($attendance)) {

                    $current_date = new DateTime();
                    $current_date = $current_date->format('y-m-d');
                    $current_date = str_replace("-", "", $current_date);
                    $current_date_int = (int) $current_date;

                    $latest_attendance = current($attendance);
                    $latest_attendance_removedhyphen = str_replace("-", "", $latest_attendance);
                    $latest_attendance_removedhyphen = $str2 = substr($latest_attendance_removedhyphen, 2);
                    $latest_attendance_int = (int) $latest_attendance_removedhyphen;

                   
                        // give warning message when the attendance has not been entered for the class
                        if ($latest_attendance == null) {
                        $warning = $section->getClass()->getName() . ': Please enter your first attendance';
                        $this->get('session')->getFlashBag()->add('error', $warning);
                    }
                    // give warning message when the latest attendance is more than 7 days old
                    else if ($current_date_int - $latest_attendance_int > 7) {
                        $warning = $section->getClass()->getName() . ': no attendance after ' . $latest_attendance;
                        $this->get('session')->getFlashBag()->add('error', $warning);
                    }
                    
                    
                }
            }
}
        }
        return $this->render('AppBundle:Teacher:currentCourses.html.twig', array(
                    'user' => $user,
                    'sections' => $sections,
                    'currentYear' => $currentYear
        ));
    }

    /**
     * Input:
     * Output:
     * Init dashboard page for teachers
     * @return Teacher Main Page
     */
    public function pastYearAction() {
        $this->checkPermission(self::PERMISSION_VIEW);

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $sections = $user->getTeaches();
        $taSections = $user->getTeachingAssists();
        $sections = new ArrayCollection(array_merge($sections->toArray(), $taSections->toArray()));
        $pastYear = array();
        foreach ($sections as $section) {
            if ($section->getTerm() == $section->getTerm()->getCampus()->getEnrollmentTerm()) {
                array_push($pastYear, $section);
            }
        }
        return $this->render('AppBundle:Teacher:pastCourses.html.twig', array(
                    'user' => $user,
                    'sections' => $sections,
                    'pastYear' => $pastYear
        ));
    }

    /**
     * Input: $id
     * Output: Section Page
     * Renders page for section to make attendance, etc.
     * @param type $id
     * @return type
     */
    public function sectionAction($id) {
        $this->checkPermission(self::PERMISSION_VIEW);

        $user = $this->getUser();
        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
        
        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($user, $section);
        }
        
        $all_assignments = $section->getAssignments();
        $createdBy = array();

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                ->select('a.id, a.date')
                ->from('AppBundle:PearAttendance', 'a')
                ->where('a.section = :currentsection')
                ->setParameter('currentsection', $section->getId())
                ->orderBy('a.date', 'DESC')
                ->getQuery();

        $attendances = $query->getResult();

        $students = $section->getStudents();

        foreach ($all_assignments as $assignment) {
            if ($assignment->getCreatedBy()->getId() == $user->getId()) {
                $createdBy[$assignment->getId()] = "true";
            }
        }
        // Insert bool values for edit/add button here
        return $this->render('AppBundle:Teacher:section.html.twig', array(
                    'section' => $section,
                    'createdBy' => $createdBy,
                    'attendances' => $attendances,
                    'students' => $students
        ));
    }
    
    /**
     * Input:
     * Output:
     * Init dashboard page for teachers
     * @return Teacher Main Page
     */
    public function showStudentAction($id, $student_id) {
        $this->checkPermission(self::PERMISSION_VIEW);

        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($id);
        
        if (!$this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $this->checkTeacherPermission($this->getUser(), $section);
        }
        
        $user = $this->getDoctrine()->getRepository('AppBundle:PearUsers')->find($student_id);
        return $this->render('AppBundle:Teacher:student.html.twig', array(
                    'user' => $user,
                    'section' => $section
        ));
    }

}
