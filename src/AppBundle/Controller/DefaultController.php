<?php

namespace AppBundle\Controller;

use AppBundle\Controller\PearController;

class DefaultController extends PearController
{
    public function indexAction()
    {
        $user = $this->getUser();
        
        if ($user != null)
        {
            //if (!$user->getEmailVerfied())
            //    return $this->redirectAction();
            
            if ($this->checkPermission(self::PERMISSION_VIEW, true, 'Admin\Campuses'))
                return $this->redirect($this->generateUrl("_admin_campuses_show", array('id'=>$user->getCampus()->getId())));
            elseif ($this->checkPermission(self::PERMISSION_VIEW, true, 'Teacher\Main'))
                return $this->redirect($this->generateUrl("_teacher_main"));
            elseif ($this->checkPermission(self::PERMISSION_VIEW, true, 'Student\Index'))
                return $this->redirect($this->generateUrl("_student_currentYear"));
            else
                return $this->noRoleAction();
        }
        else
            return $this->redirect($this->generateUrl('_login'));
    }
    
    public function redirectAction()
    {
        return $this->render('AppBundle:Registration:post_registration.html.twig');
    }
    
    public function noRoleAction()
    {
        return $this->render('AppBundle:Base:preactivation_base.html.twig');
    }

     public function termsAndConditionsAction()
    {
        $this->indexAction();
        return $this->render('AppBundle:Base:Terms.html.twig');
    }
}
