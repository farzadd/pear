<?php

namespace AppBundle\Controller;

class UsersController extends PearController
{
    public function indexAction()
    {
        $this->checkPermission(self::PERMISSION_VIEW);
        $user = $this->getUser();
        
        if (!$user)
            return $this->redirect($this->generateUrl("_login"));
        
        return $this->render('AppBundle:Admin:Users/showUser.html.twig', array(
            'user' => $user));
    }
}
