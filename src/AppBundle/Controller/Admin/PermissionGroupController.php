<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Reponse;
use AppBundle\Controller\PearController;
use AppBundle\Entity\PearSettings;
use AppBundle\Entity\PearPermissions;
use AppBundle\Entity\PearRoles;
use AppBundle\Entity\PearCustomAttributes;
use AppBundle\Form\Type\PermissionGroupType;

class PermissionGroupController extends PearController {

    public function indexAction() {
        $this->checkPermission(self::PERMISSION_VIEW);

        $roles = $this->getDoctrine()->getRepository('AppBundle:PearRoles')->findAll();

        return $this->render('AppBundle:Admin:PermissionGroup/permissionGroup.html.twig', array('roles' => $roles));
    }

    public function addAction() {
        $this->checkPermission(self::PERMISSION_CREATE);
        $role = new PearRoles();
        $form = $this->createForm(new PermissionGroupType(), $role, array('action' => $this->generateUrl('_admin_permission_group_add_post')));


        return $this->render('AppBundle:Admin:PermissionGroup/permissionGroupAdd.html.twig', array('form' => $form->createView()));
    }

    public function addPostAction(Request $request) {

        $this->checkPermission(self::PERMISSION_CREATE);

        $role = new PearRoles();
        $form = $this->createForm(new PermissionGroupType(), $role, array('action' => $this->generateUrl('_admin_permission_group_add_post')));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $i = 0;
            $customAttributes = $request->request->get('customAttribute');
            $customAttributeDesc = $request->request->get('customAttributeDesc');
            if ($customAttributes != NULL && $customAttributeDesc != NULL) {
                foreach ($customAttributes as $ca) {
                    $customAttribute = new PearCustomAttributes();
                    $customAttribute->setName($ca);
                    $customAttribute->setDescription($customAttributeDesc[$i]);
                    $customAttribute->addRole($role);
                    $role->addCustomAttribute($customAttribute);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($customAttribute);

                    $i++;
                }
                $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'New Permission Group Created!'
                );
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($role);
            $em->flush();
            $this->logAction("Permission group".$role->getId()." has been created", self::LOGTYPE_CREATE, $role->getId());

            return $this->redirect($this->generateUrl(
                                    '_admin_permission_group'
            ));
        } else {
            return $this->render('AppBundle:Admin:PermissionGroup/permissionGroupAdd.html.twig', array('form' => $form->createView()));
        }
    }

    // input: permissionGroup role Id
    // output: edit page
    public function editAction($id) {
        $this->checkPermission(self::PERMISSION_EDIT);

        $role = $this->getDoctrine()->getRepository('AppBundle:PearRoles')->find($id);
        $customAttributes = $role->getCustomAttributes();
        $form = $this->createForm(new PermissionGroupType(), $role, array('action' => $this->generateUrl('_admin_permission_group_edit_post', array('id' => $id))));

        return $this->render('AppBundle:Admin:PermissionGroup/permissionGroupEdit.html.twig', array('form' => $form->createView(), 'customAttributes' => $customAttributes));
    }

    public function editPostAction($id, Request $request) {
        $this->checkPermission(self::PERMISSION_EDIT);

        $role = $this->getDoctrine()->getRepository('AppBundle:PearRoles')->find($id);
        $old_customAttributes = $role->getCustomAttributes();
        $form = $this->createForm(new PermissionGroupType(), $role, array('action' => $this->generateUrl('_admin_permission_group_edit_post', array('id' => $id))));

        $form->handleRequest($request);


        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            foreach ($old_customAttributes as $old) {
                $name = $request->request->get($old->getId() . "name");
                $description = $request->request->get($old->getId() . "description");
                $old->setName($name);
                $old->setDescription($description);
                $em->persist($old);
            }

            $i = 0;
            $customAttributes = $request->request->get('customAttribute');
            $customAttributeDesc = $request->request->get('customAttributeDesc');

            if ($customAttributes != NULL && $customAttributeDesc != NULL) {
                foreach ($customAttributes as $ca) {
                    $customAttribute = new PearCustomAttributes();
                    $customAttribute->setName($ca);
                    $customAttribute->setDescription($customAttributeDesc[$i]);
                    $customAttribute->addRole($role);
                    $role->addCustomAttribute($customAttribute);

                    $em->persist($customAttribute);

                    $i++;
                }
            }

            $em->persist($role);
            $em->flush();
            
            $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'Permission group has been edited!'
                );
            
            $this->logAction("Permission group".$role->getId()." has been edited", self::LOGTYPE_EDIT, $role->getId());
            return $this->redirect($this->generateUrl(
                                    '_admin_permission_group'
            ));
        } else {
            return $this->render('AppBundle:Admin:PermissionGroup/permissionGroupEdit.html.twig', array('form' => $form->createView(), 'customAttributes' => $old_customAttributes));
        }
    }
    
    public function deleteAction($id) {
        $this->checkPermission(self::PERMISSION_DELETE);

        $role = $this->getDoctrine()->getRepository('AppBundle:PearRoles')->find($id);
        
        $em = $this->getDoctrine()->getManager();
        $em->remove($role);
        $em->flush();
        
        $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'Role has been deleted!'
                );
        
        $roles = $this->getDoctrine()->getRepository('AppBundle:PearRoles')->findAll();
        return $this->render('AppBundle:Admin:PermissionGroup/permissionGroup.html.twig', array('roles' => $roles));
    }

    //input : permission Group id
    // output: chart to edit 
    public function permissionAction($id) {
        $this->checkPermission(self::PERMISSION_CREATE);

        $role = $this->getDoctrine()->getRepository('AppBundle:PearRoles')->find($id);
        $controllers = array(
            "Search",
            "Users",
            "PrereqExemptionRequests",
            "Admin\Announcements",
            "Admin\Campuses",
            "Admin\CampusTerms",
            "Admin\Classes",
            "Admin\Dashboard",
            "Admin\Departments",
            "Admin\Enrollment",
            "Admin\ImportExport",
            "Admin\Payments",
            "Admin\PermissionGroup",
            "Admin\Sections",
            "Admin\Report",
            "Admin\Settings",
            "Admin\Users",
            "Student\Attendance",
            "Student\Grades",
            "Student\Index",
            "Teacher\Main",
            "Teacher\ReportCard",
            "Teacher\Grade",
            "Teacher\GlobalAssignment",
            "Teacher\Attendance",
            "Teacher\Assignment",
            "Teacher\Documents"
        );
        
        // get current permission for this role
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                ->select('a')
                ->from('AppBundle:PearPermissions', 'a')
                ->where('a.role = :role')
                ->setParameter('role', $id)
                ->getQuery();

        $permissions = $query->getResult();
        $currentPermissions = array();
        foreach ($permissions as $permission) {
            $flag = $permission->getFlag();
            $currentPermissions[$permission->getName()] = $flag;
        }
         

        return $this->render('AppBundle:Admin:PermissionGroup/permissionDetail.html.twig', 
                array('role' => $role, 'controllers' => $controllers, 'currentPermissions' => $currentPermissions));
    }

    //input : permission Group id
    // output: chart to edit 
    public function permissionPostAction($id, Request $request) {
        $this->checkPermission(self::PERMISSION_CREATE);
        
        $role = $this->getDoctrine()->getRepository('AppBundle:PearRoles')->find($id);
        $controllers = array(
            "Search",
            "Users",
            "PrereqExemptionRequests",
            "Admin\Announcements",
            "Admin\Campuses",
            "Admin\CampusTerms",
            "Admin\Classes",
            "Admin\Dashboard",
            "Admin\Departments",
            "Admin\Enrollment",
            "Admin\ImportExport",
            "Admin\Payments",
            "Admin\PermissionGroup",
            "Admin\Sections",
            "Admin\Report",
            "Admin\Settings",
            "Admin\Users",
            "Student\Attendance",
            "Student\Grades",
            "Student\Index",
            "Teacher\Main",
            "Teacher\ReportCard",
            "Teacher\Grade",
            "Teacher\GlobalAssignment",
            "Teacher\Attendance",
            "Teacher\Assignment",
            "Teacher\Documents"
        );

        $em = $this->getDoctrine()->getManager();

        foreach ($controllers as $controller) {                    
            $flag = 0;  
            $view = $controller . "view";
            $create = $controller . "create";
            $edit = $controller . "edit";
            $delete = $controller . "delete";
            $other = $controller . "other";
            $super = $controller . "super";
            
            if (isset($_POST[$view])) {
                $flag += 1;
            }
            if (isset($_POST[$create])) {
                $flag += 2;
            }
            if (isset($_POST[$edit])) {
                $flag += 4;
            }
            if (isset($_POST[$delete])) {
                $flag += 8;
            }
            if (isset($_POST[$other])) {
                $flag += 16;
            }
            if (isset($_POST[$super])) {
                $flag += 32;
            }
            
            
            $query = $em->createQueryBuilder()
                ->select('a')
                ->from('AppBundle:PearPermissions', 'a')
                ->where('a.role = :role')
                ->andwhere('a.name = :name')
                ->setParameter('role', $id)
                ->setParameter('name', $controller)
                ->getQuery();
            $query_result = $query->getResult();
            
            if(sizeof($query_result) == 0) {
                $permission = new PearPermissions(); 
                $permission->setRole($role);
                $permission->setName($controller); 
            }
            else {
                $permission = array_pop($query_result);
            }
            
            $permission->setFlag($flag);
            $em->persist($permission);
            $em->flush();
        }
        
        $this->logAction("Edited role permissions under ".$id, self::LOGTYPE_EDIT, $id);
           
        $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'Permission has been changed!'
                );
        $roles = $this->getDoctrine()->getRepository('AppBundle:PearRoles')->findAll();

        return $this->render('AppBundle:Admin:PermissionGroup/permissionGroup.html.twig', array('roles' => $roles));
    }

}
