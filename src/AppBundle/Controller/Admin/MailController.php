<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Controller\PearController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use AppBundle\Form\Type\MailType;
use Symfony\Component\HttpFoundation\Request;
/**
 * Mail controller.
 */
class MailController extends PearController
{
    //render the form for a new mail
    public function newAction()
    {
        //check for permission to send email
       $this->checkPermission(self::PERMISSION_CREATE);
        //render the form
       $form = $this->createForm(new MailType(), null,
        array('action' => $this->generateUrl('_admin_mail_send')));

       return $this->render('AppBundle:Admin:Mail/new.html.twig',
         array('form' => $form->createView()));
   }

    //handle submission of a a completed mail form
   public function sendAction(Request $request)
   {
    //check for permission to send email
       $this->checkPermission(self::PERMISSION_CREATE);
        //render the form
       $form = $this->createForm(new MailType(), null,
        array('action' => $this->generateUrl('_admin_mail_send')));

       $form->handleRequest($request);

       if ($form->isValid())
       {
        //get fields to pass to form
        $subject = $form["subject"]->getData();
        $body = $form["message"]->getData();
        $campuses = $form["campuses"]->getData()->toArray();

        //call function to get array of emails
        $emails = $this->getUserEmails($campuses);

        $mailer = $this->get('mailer');
        $message = $mailer->createMessage()
        ->setSubject($subject)
        ->setFrom('school@gobindsarvar.com')
        ->setBcc($emails)
        ->setBody($body);
        $mailer->send($message);

        $flashMessage = $this->get('session')->getFlashBag()->add(
            'notice',
            'message delivered!'
            );

        return $this->render('AppBundle:Admin:Mail/new.html.twig',
         array('form' => $form->createView()));
    }

    $flashMessage = $this->get('session')->getFlashBag()->add(
        'error',
        'message could not be delivered!'
        );

    return $this->render('AppBundle:Admin:Mail/new.html.twig',
     array('form' => $form->createView()));

}

public function getUserEmails($formCampuses)
{
    $emails = array();

    foreach($formCampuses as $campus)
    {
        $users = $campus->getUsers();
        foreach($users as $user)
        {
            array_push($emails, $user->getEmail());
        }
    }
   return $emails;       
}
}