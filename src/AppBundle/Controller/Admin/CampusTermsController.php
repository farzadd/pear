<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Controller\PearController;
use AppBundle\Entity\PearCampusTerms;
use AppBundle\Entity\PearCampuses;
use Appbundle\Entity\PearClassSections;
use AppBundle\Form\Type\TermType;
use AppBundle\Form\Type\AddSectionType;

class CampusTermsController extends PearController {

    public function showAction($id) {

        $term = $this->getDoctrine()
                ->getRepository('AppBundle:PearCampusTerms')
                ->find($id);

        if (!$term) {
            throw $this->createNotFoundException(
                    'No term found for id ' . $id
            );
        }

        $campus = $term->getCampus();

        $this->checkPermission(self::PERMISSION_VIEW, false, null, $campus);

        $sections = $term->getSections();

        return $this->render('AppBundle:Admin:CampusTerms/showCampusTerm.html.twig', array(
                    'sections' => $sections,
                    'term' => $term,
                    'campus' => $campus,
        ));
    }

    public function scheduleAction($id) {
        $term = $this->getDoctrine()
                ->getRepository('AppBundle:PearCampusTerms')
                ->find($id);

        //enrollment is active so show term builder
        if (!$term) {
            throw $this->createNotFoundException(
                    'No term found for id ' . $id
            );
        }

        $campus = $term->getCampus();

        $this->checkPermission(self::PERMISSION_CREATE, false, null, $campus);

        $departments = $this->getDoctrine()
                ->getRepository('AppBundle:PearDepartments')
                ->findAll();

        return $this->render('AppBundle:Admin:CampusTerms/scheduleTerm.html.twig', array(
                    'term' => $term,
                    'departments' => $departments,
        ));
    }

    public function schedulePostAction($id) {
        $term = $this->getDoctrine()
                ->getRepository('AppBundle:PearCampusTerms')
                ->find($id);

        //enrollment is active so show term builder
        if (!$term) {
            throw $this->createNotFoundException(
                    'No term found for id ' . $id
            );
        }

        $campus = $term->getCampus();

        $this->checkPermission(self::PERMISSION_CREATE, false, null, $campus);

        $request = $this->container->get('request');

        $classes = $this->getDoctrine()
                ->getRepository('AppBundle:PearClasses')
                ->findById($request->request->get('classes'));

        $section = new PearClassSections();
        $form = $this->createForm(new AddSectionType($this->get('security.token_storage')), $section, array('action' => $this->generateUrl('_admin_terms_schedule_section_post', array('id' => $id))));

        return $this->render('AppBundle:Admin:CampusTerms/scheduleSectionsTerm.html.twig', array(
                    'term' => $term,
                    'classes' => $classes,
                    'form' => $form->createView()
        ));
    }

    public function scheduleSectionPostAction($id) {
        $term = $this->getDoctrine()
                ->getRepository('AppBundle:PearCampusTerms')
                ->find($id);

        //enrollment is active so show term builder
        if (!$term) {
            throw $this->createNotFoundException(
                    'No term found for id ' . $id
            );
        }

        $campus = $term->getCampus();

        $this->checkPermission(self::PERMISSION_CREATE, false, null, $campus);

        $section = new PearClassSections();

        $form = $this->createForm(new AddSectionType($this->get('security.token_storage')), $section, array('action' => $this->generateUrl('_admin_terms_schedule_section_post', array('id' => $id))));

        $request = $this->getRequest();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $term = $this->getDoctrine()
                    ->getRepository('AppBundle:PearCampusTerms')
                    ->find($id);

            $class = $this->getDoctrine()
                    ->getRepository('AppBundle:PearClasses')
                    ->find($form->get('class')->getData());

            $flag = 0;

            if (isset($_POST["Mon"])) {
                $flag += 1;
            }
            if (isset($_POST["Tues"])) {
                $flag += 2;
            }
            if (isset($_POST["Wed"])) {
                $flag += 4;
            }
            if (isset($_POST["Thurs"])) {
                $flag += 8;
            }
            if (isset($_POST["Fri"])) {
                $flag += 16;
            }
            if (isset($_POST["Sat"])) {
                $flag += 32;
            }
            if (isset($_POST["Sun"])) {
                $flag += 64;
            }

            $section->setDaysOfWeek($flag);

            if ($class != null) {
                $section->setClass($class);
                $section->setTerm($term);

                $em = $this->getDoctrine()->getManager();
                $em->persist($section);
                $em->flush();

                $this->logAction("Section".$section->getId()." has been created for ".$section->getClass()->getName(), self::LOGTYPE_EDIT, $section->getId());
                $sections = $this->getDoctrine()
                        ->getRepository('AppBundle:PearClassSections')
                        ->findBy(array('term' => $term, 'class' => $class));

                return $this->render('AppBundle:Admin:CampusTerms/sectionBar.html.twig', array(
                            'sections' => $sections));
            }
        }

        return new Response("An error has occurred while adding section");
    }

    public function newAction($id) {

        //find campus for this term
        $campus = $this->getDoctrine()
                ->getRepository('AppBundle:PearCampuses')
                ->find($id);

        if (!$campus) {
            throw $this->createNotFoundException(
                    'No campus found for id ' . $id
            );
        }

        $this->checkPermission(self::PERMISSION_CREATE, false, null, $campus);

        $term = new PearCampusTerms();

        $form = $this->createForm(new TermType(), $term, array('action' => $this->generateUrl('_admin_terms_create_post', array('id' => $id))));

        return $this->render('AppBundle:Admin:CampusTerms/addTerm.html.twig', array(
                    'form' => $form->createView(), 'campus' => $campus));
    }

    //create a new campus term and initialize its campus based on
    //the campus page from which this function was called
    public function newPostAction($id, Request $request) {
        //find campus for this term
        $campus = $this->getDoctrine()
                ->getRepository('AppBundle:PearCampuses')
                ->find($id);

        if (!$campus) {
            throw $this->createNotFoundException(
                    'No campus found for id ' . $id
            );
        }

        $this->checkPermission(self::PERMISSION_CREATE, false, null, $campus);

        $term = new PearCampusTerms();

        $form = $this->createForm(new TermType(), $term, array('action' => $this->generateUrl('_admin_terms_create_post', array('id' => $id))));

        $form->handleRequest($request);

        if ($form->isValid()) {

            $term->setCampus($campus);
            $term->setTermClosed(false);
            $em = $this->getDoctrine()->getManager();
            $em->persist($term);
            $em->flush();
            $this->logAction("Term ".$term->getId()." has been newly created for ".$term->getCampus()->getName(), self::LOGTYPE_CREATE, $term->getId());
            $flashMessage = $this->get('session')->getFlashBag()->add(
                    'notice', 'New Term Added!');
            return $this->showAction($term->getId());
        }

        $flashMessage = $this->get('session')->getFlashBag()->add(
                'error', 'New Term cannot be added!'
        );
        return $this->render('AppBundle:Admin:CampusTerms/addTerm.html.twig', array(
                    'form' => $form->createView(), 'campus' => $campus));
    }

    public function editAction($id) {
        //find campus for this term
        $term = $this->getDoctrine()
                ->getRepository('AppBundle:PearCampusTerms')
                ->find($id);

        $campus = $term->getCampus();

        $this->checkPermission(self::PERMISSION_EDIT, false, null, $campus);

        $form = $this->createForm(new TermType(), $term, array('action' => $this->generateUrl('_admin_terms_edit_post', array('id' => $id))));

        return $this->render('AppBundle:Admin:CampusTerms/editTerm.html.twig', array(
                    'form' => $form->createView(), 'campus' => $campus, 'term' => $term));
    }

    public function editPostAction($id, Request $request) {
        //find campus for this term
        $term = $this->getDoctrine()
                ->getRepository('AppBundle:PearCampusTerms')
                ->find($id);
        $campus = $term->getCampus();

        $this->checkPermission(self::PERMISSION_EDIT, false, null, $campus);

        $form = $this->createForm(new TermType(), $term, array('action' => $this->generateUrl('_admin_terms_edit_post', array('id' => $id))));

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($term);
            $em->flush();
            $this->logAction("Term ".$term->getId()." has been edited for ".$term->getCampus()->getName(), self::LOGTYPE_EDIT, $term->getId());
            $flashMessage = $this->get('session')->getFlashBag()->add(
                    'notice', 'Term Edited Successfully!');
            return $this->showAction($term->getId());
        }

        $flashMessage = $this->get('session')->getFlashBag()->add(
                'error', 'Term cannot be edited!'
        );
        return $this->render('AppBundle:Admin:CampusTerms/editTerm.html.twig', array(
                    'form' => $form->createView(), 'campus' => $campus, 'term' => $term));
    }

    public function setEnrollmentAction($id) {
        $term = $this->getDoctrine()->getRepository('AppBundle:PearCampusTerms')->find($id);

        $campus = $term->getCampus();

        $this->checkPermission(self::PERMISSION_OTHER, false, null, $campus);

        $campus->setEnrollmentTerm($term);

        $em = $this->getDoctrine()->getManager();
        $em->persist($campus);
        $em->flush();

        $this->logAction("Enrollment term set to " . $term->getId() . " for ".$term->getCampus()->getName(), self::LOGTYPE_OTHER, $term->getId());
        $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice', "You've set the enrollment term to " . $term->getId() . "."
        );

        return $this->redirect($this->generateUrl('_admin_campuses_show', array('id' => $campus->getId())));
    }

    public function setActiveAction($id) {
        $term = $this->getDoctrine()->getRepository('AppBundle:PearCampusTerms')->find($id);

        $campus = $term->getCampus();

        $this->checkPermission(self::PERMISSION_OTHER, false, null, $campus);

        $campus->setActiveTerm($term);

        $em = $this->getDoctrine()->getManager();
        $em->persist($campus);
        $em->flush();

        $this->logAction("Active term set to " . $term->getId() . " for ".$term->getCampus()->getName(), self::LOGTYPE_OTHER, $term->getId());
        $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice', "You've set the active term to " . $term->getId() . "."
        );

        return $this->redirect($this->generateUrl('_admin_campuses_show', array('id' => $campus->getId())));
    }

    public function closeEnrollmentAction($id) {
        $term = $this->getDoctrine()->getRepository('AppBundle:PearCampusTerms')->find($id);
        $campus = $term->getCampus();
        $this->checkPermission(self::PERMISSION_OTHER, false, null, $campus);
        $campus->setEnrollmentTerm(null);

        $em = $this->getDoctrine()->getManager();
        $em->persist($campus);
        $em->flush();

        $this->logAction("Enrollment closed for campus ".$term->getCampus()->getName(), self::LOGTYPE_OTHER, $term->getId());
        $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice', "You've closed enrolment for this campus. Users can no longer enroll in this campus."
        );

        return $this->redirect($this->generateUrl('_admin_campuses_show', array('id' => $campus->getId())));
    }

    public function closeTermAction($id) {
        $term = $this->getDoctrine()->getRepository('AppBundle:PearCampusTerms')->find($id);
        $campus = $term->getCampus();
        $this->checkPermission(self::PERMISSION_OTHER, false, null, $campus);

        $term->setTermClosed(true);

        if ($campus->getActiveTerm() == $term)
            $campus->setActiveTerm(null);

        $this->logAction("Term ".$term->getId()." has been closed in campus ".$term->getCampus()->getName().". It's now inactive", self::LOGTYPE_OTHER, $term->getId());

        $em = $this->getDoctrine()->getManager();
        $em->persist($campus);
        $em->persist($term);
        $em->flush();

        $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice', "You've successfully closed this term. Term is now inactive.");

        return $this->redirect($this->generateURL('_admin_campuses_show', array('id' => $campus->getId())));
    }

}
