<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Controller\PearController;
use AppBundle\Entity\PearPayments;
use AppBundle\Form\Type\AddPaymentType;
use AppBundle\Form\Type\AddPaymentMultiType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class PaymentsController extends PearController
{

    // Show all payments
    public function indexAction()
    {
        $user = $this->getUser();
        $campus = $user->getCampus();
        $this->checkPermission(self::PERMISSION_VIEW, false, null, $campus);

        // if logged in user is super admin, show all payments
        if ($this->checkPermission(self::PERMISSION_SUPER, true))
        {
            $payments = $this->getDoctrine()->getRepository('AppBundle:PearPayments')
            ->findAll();
        }

        else //  show only payments for user campus (admin/campus admin)
        { // TODO: use a query here
            $payments = $this->getDoctrine()->getRepository('AppBundle:PearPayments')
            ->findAll();

            foreach ($payments as $key => $payment)
            {
                if ($payment->getUsers()->first()->getCampus() != $campus)
                {
                    unset($payments[$key]);
                }
            }
        }

        return $this->render('AppBundle:Admin:Payments/listPayments.html.twig',
            array('payments' => $payments));
    }

    // show a particular payment details
    public function showAction($id)
    {
        $payment = $this->getDoctrine()
        ->getRepository('AppBundle:PearPayments')
        ->find($id);

        $campus = $payment->getUsers()->first()->getCampus();

        $this->checkPermission(self::PERMISSION_VIEW, false, null, $campus);


        if (!$payment) {
            throw $this->createNotFoundException(
                'No payment record with id: '.$id
                );
        }

        return $this->render('AppBundle:Admin:Payments/showPayment.html.twig', array(
            'payment' => $payment));
    }

    // Render form for new payment creation
    public function newAction($user_id)
    {
        $this->checkPermission(self::PERMISSION_CREATE);

        $payment = new PearPayments();

        $form = $this->createForm(new AddPaymentType(), $payment,
         array('action' => $this->generateUrl('_admin_payments_new_post', array('user_id' => $user_id))));

        return $this->render('AppBundle:Admin:Users/addPayment.html.twig', array(
         'form' => $form->createView()));

    }

    // Handle form submission for new payment creation
    public function newPostAction(Request $request, $user_id)
    {
        $this->checkPermission(self::PERMISSION_CREATE);

        $payment = new PearPayments();

        $user = $this->getDoctrine()
        ->getRepository('AppBundle:PearUsers')
        ->find($user_id);

        $form = $this->createForm(new AddPaymentType(), $payment,
         array('action' => $this->generateUrl('_admin_payments_new_post', array('user_id' => $user_id))));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $payment->addUser($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($payment);
            $em->flush();

            $this->logAction("New payment".$payment->getId()." has been added", self::LOGTYPE_CREATE, $payment->getId());
            $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'New Payment Added!'
                );

            return $this->showAction($payment->getId());
        }

        //TODO: what do we want to do if unsuccessful?
        return $this->render('AppBundle:Admin:Payments/showpayment.html.twig', array(
            'payment' => $payment,
            'form' => $form->createView()));
    }

    // render a form to edit a payment
    public function editAction($id)
    {
        $payment = $this->getDoctrine()
        ->getRepository('AppBundle:PearPayments')
        ->find($id);

        $campus = $payment->getUsers()->first()->getCampus();

        $this->checkPermission(self::PERMISSION_EDIT, false, null, $campus);

        $form = $this->createForm(new AddPaymentType(), $payment,
            array('action' => $this->generateUrl('_admin_payments_edit_post', array('id' => $id))));


        return $this->render('AppBundle:Admin:Payments/editPayment.html.twig', array(
            'form' => $form->createView(),
            'payment' => $payment));
    }


    // handle a form for edited payment sumbmission
    public function editPostAction($id, Request $request)
    {
        $payment = $this->getDoctrine()
        ->getRepository('AppBundle:PearPayments')
        ->find($id);

        $campus = $payment->getUsers()->first()->getCampus();

        $this->checkPermission(self::PERMISSION_EDIT, false, null, $campus);

        $form = $this->createForm(new AddPaymentType(), $payment,
            array('action' => $this->generateUrl('_admin_payments_edit_post', array('id' => $id))));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $users = $this->getRequest()->request->get("users");

            if($users == null) {
                $flashMessage = $this->get('session')->getFlashBag()->add(
            'error',
            'Please add user!'
            );

            return $this->render('AppBundle:Admin:Payments/editPayment.html.twig', array(
            'form' => $form->createView()));
            }

            foreach ($payment->getUsers() as $user) {
                $payment->removeUser($user);
            }

            foreach ($users as $userId)
            {
                $user = $this->getDoctrine()
                ->getRepository('AppBundle:PearUsers')
                ->find($userId);

                $payment->addUser($user);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($payment);
            $em->flush();

            $this->logAction("Payment".$payment->getId()." has been edited", self::LOGTYPE_EDIT, $payment->getId());
            $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'Payment successfully updated!'
                );

            return $this->showAction($id);
        }

        $flashMessage = $this->get('session')->getFlashBag()->add(
                'error',
                'Payment not updated. Check logs for details.'
                );

        return $this->showAction($id);
    }

    public function newMultiuserAction()
    {
        $this->checkPermission(self::PERMISSION_CREATE);

        $payment = new PearPayments();

        $form = $this->createForm(new AddPaymentMultiType(), $payment,
            array('action' => $this->generateUrl('_admin_payments_new_multiuser_post')));

        return $this->render('AppBundle:Admin:Payments/addPayment.html.twig', array(
            'form' => $form->createView()));

    }

    public function newMultiuserPostAction(Request $request)
    {
        $this->checkPermission(self::PERMISSION_CREATE);

        $payment = new PearPayments();

        $form = $this->createForm(new AddPaymentMultiType(), $payment,
         array('action' => $this->generateUrl('_admin_payments_new_multiuser_post')));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $users = $this->getRequest()->request->get("users");
            $payment->setCreated(new \DateTime("now"));

            if($users == null) {
                $flashMessage = $this->get('session')->getFlashBag()->add(
            'error',
            'Please add user!'
            );

            return $this->render('AppBundle:Admin:Payments/addPayment.html.twig', array(
            'form' => $form->createView()));
            }
            foreach ($users as $userId)
            {
                $user = $this->getDoctrine()
                ->getRepository('AppBundle:PearUsers')
                ->find($userId);

                $payment->addUser($user);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($payment);
            $em->flush();

            $this->logAction("New multi-user payment under".$payment->getId()." added", self::LOGTYPE_CREATE, $payment->getId());
            $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'New Payment Added!'
                );

            return $this->showAction($payment->getId());
        }
        // $errorsAsArray = iterator_to_array($form->getErrors());
        // var_dump($errorsAsArray);
        $flashMessage = $this->get('session')->getFlashBag()->add(
            'error',
            'Unable to create new payment!'
            );
        return $this->render('AppBundle:Admin:Payments/addPayment.html.twig', array(
            'form' => $form->createView()));
    }


}
