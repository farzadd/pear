<?php

namespace AppBundle\Controller\Admin;
use AppBundle\Controller\PearController;
use AppBundle\Entity\PearUserLogs;

class LogsController extends PearController
{
	public function indexAction() {
		$this->checkPermission(self::PERMISSION_VIEW);

		$em = $this->getDoctrine()->getManager();
		$logs = $em->getRepository('AppBundle:PearUserLogs')->findBy(array(), array('created'=>'DESC'));

		return $this->render('AppBundle:Admin:Logs/adminLogs.html.twig', array('adminLogs' => $logs));
	}
}

