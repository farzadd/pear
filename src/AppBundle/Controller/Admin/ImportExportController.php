<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Controller\PearController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Ddeboer\DataImport\Reader\DoctrineReader;
use Ddeboer\DataImport\Workflow;
use Ddeboer\DataImport\Writer\CsvWriter;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;

use Ddeboer\DataImport\ValueConverter\CallbackValueConverter;
use \DateTime;


class ImportExportController extends PearController
{

    //calls buildExportRequest and passes resulting form to view
    //@return export.html.twig 
    public function showExportAction()
    {
        $this->checkPermission(self::PERMISSION_VIEW);
        
        $entities = $this->getEntityNamesAction();

        return $this->render('AppBundle:Admin:ImportExport/export.html.twig', 
            array('entities' => $entities));
    }

    //consumes a string representing entity to be exported and
    //builds an excel spreadsheet with the content of that table
    //which is then placed in /web/export/FILENAME
    //@param $entity is string of entity name
    //@return redirect to dashboard and force download of excel file
    public function readFromDoctrineToExcelAction($entity)
    {
        $this->checkPermission(self::PERMISSION_CREATE);
        $fs = new Filesystem();
        $entity_name = explode('\\', $entity);
        $path = __DIR__ . '/../../../../web/export/'.end($entity_name).'_'.time().'.csv';

        //create the file that will be written to
        $fs->touch($path);

        $em = $this->getDoctrine()->getManager();
        $reader = new DoctrineReader($em, $entity);
        $file = new \SplFileObject($path);
        $workflow = new Workflow($reader);

        //convert datetime fields to string
        $converter = new CallbackValueConverter(function ($input) {
            //need to check if input is actually datetime because some
            //entities have field called 'created' that is already string
            //and doesn't need processing
            if ($input instanceof DateTime) {
              if ($input) { return $input->format('Y-m-d'); } 
            //format failed
              else { return "Unknown Time"; } 
          }
          else { return $input; } 
      });

        //set value delimeter
        $writer = new CsvWriter(',');
        $writer->setStream(fopen($path, 'w'));
        $writer->writeItem($this->getcolumnHeaders($em, $entity));

        $result = $workflow
        ->addWriter($writer)
        ->setSkipItemOnFailure(true)
        //datetime converter for user
        ->addValueConverter('birthDate', $converter)
        //datetime converter for sections
        ->addValueConverter('start', $converter)
        ->addValueConverter('end', $converter)
        //datetime converter for payments
        ->addValueConverter('created', $converter)
        ->process();

        $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'Link has been added!'
                );
        
        return $this->redirect($this->generateUrl('_admin_available_exports'));

    }

    public function getcolumnHeaders($em, $entity)
    {
        return $em->getClassMetadata($entity)->getFieldNames();
    }

    //helper method for building choice list in buildExportRequest
    //@return array of entity names as strings
    public function getEntityNamesAction()
    {
        $entities = array();
        $em = $this->getDoctrine()->getManager();
        $meta = $em->getMetadataFactory()->getAllMetadata();
        foreach ($meta as $m) {
            $entities[] = $m->getName();

        }
        return $entities;
    }
    
    private function sortExports($a, $b)
    {
        $a_exploded = explode('_', $a);
        $b_exploded = explode('_', $b);
        
        if (end($a_exploded) < end($b_exploded))
            return 1;
        else
            return -1;
    }

    public function generateDownloadLinksAction()
    {
        $this->checkPermission(self::PERMISSION_OTHER);
        
        //use Finder to get names of all files in /web/export
        $finder = new Finder();
        $webDir = $this->get('kernel')->getRootDir().'/../web/export';
        
        $finder->files()->in($webDir);

        $fileNames = array();
        foreach ($finder as $file) {
            array_push($fileNames, $file->getFileName());
        }
        
        usort($fileNames, 'self::sortExports');

        return $this->render('AppBundle:Admin:ImportExport/availableDownloads.html.twig', 
            array('fileNames' => $fileNames));
    }
}
