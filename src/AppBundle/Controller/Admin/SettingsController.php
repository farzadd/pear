<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Reponse;
use AppBundle\Controller\PearController;
use AppBundle\Entity\PearSettings;

class SettingsController extends PearController
{
    public function indexAction() { 
        $this->checkPermission(self::PERMISSION_VIEW);
      
        $data = array();
        $form = $this->generateForm($data);
        
        return $this->render('AppBundle:Admin:Settings/index.html.twig', 
            array('form' => $form->createView()));
        
    }

    public function indexPostAction(Request $request) {
        $this->checkPermission(self::PERMISSION_CREATE);
        
        $data = array();
        $form = $this->generateForm($data);
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            foreach ($request->request->get('form', '') as $setting_key => $setting_value)
            {
                if ($setting_key[0] != '_' && $setting_key != "save")
                {
                    $setting = $this->getDoctrine()->getRepository('AppBundle:PearSettings')
                        ->findOneByName($setting_key);
                        
                    if ($setting)
                    {
                        $setting->setValue($setting_value);
                        
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($setting);
                        $em->flush();
                    }
                }
            }
            $this->logAction("Settings have been changed under id ".$section->getId(), self::LOGTYPE_CREATE, $setting->getId());
            $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'Settings Saved!'
                );
        }
        
        return $this->render('AppBundle:Admin:Settings/index.html.twig', array(
            'form' => $form->createView()));
    }
    
    public function generateForm(&$data)
    {
        $form = $this->createFormBuilder($data)
            ->setAction($this->generateUrl('_admin_settings_post'))
            ->setMethod('POST');
            
        $settings = $this->getDoctrine()->getRepository('AppBundle:PearSettings')
            ->findAll();
        
        if (count($settings) == 0)
        {
            $this->get('session')->getFlashBag()->add('notice','No settings found!');
        }
        else
        {
            foreach ($settings as $setting)
            {
                $form->add($setting->getName(), $setting->getType(), array(
                    'data' => $setting->getValue(),));
            }
        }
        
        $form->add('save', 'submit', array('label' => 'Save Settings'));
        
        return $form->getForm();
    }

    public function LogsindexAction() {
        $this->checkPermission(self::PERMISSION_VIEW);

        $em = $this->getDoctrine()->getManager();
        $logs = $em->getRepository('AppBundle:PearUserLogs')->findBy(array(), array('created'=>'DESC'));

        return $this->render('AppBundle:Admin:Logs/adminLogs.html.twig', array('adminLogs' => $logs));
    }
}
