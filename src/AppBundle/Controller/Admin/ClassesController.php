<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Reponse;
use AppBundle\Controller\PearController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\PearClasses;
use AppBundle\Entity\PearDepartments;
use AppBundle\Form\Type\ClassType;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ClassesController extends PearController
{

//    public function indexAction($dept_id) {
//      $this->checkPermission(self::PERMISSION_VIEW);
//
//      $classes = $this->getDoctrine()->getRepository('AppBundle:PearClasses')
//      ->findAll();
//
//      return $this->render('AppBundle:Admin:Departments/Classes/classes.html.twig',
//       array('classes' => $classes, 'department' => $dept_id));
//    }

    //renders form for new class
    public function newAction($dept_id) {
        $this->checkPermission(self::PERMISSION_CREATE);

        $class = new PearClasses();

        $department = $this->getDoctrine()
        ->getRepository('AppBundle:PearDepartments')
        ->find($dept_id);

        //get classes in dept to render table for adding prereqs
        $classes = $this->getDoctrine()->getRepository('AppBundle:PearClasses')->findAll();

        $form = $this->createForm(new ClassType(), $class,
            array('action' => $this->generateUrl('_admin_classes_new_post', array('dept_id' => $dept_id))));

        return $this->render('AppBundle:Admin:Departments/Classes/newClass.html.twig', array(
            'form' => $form->createView(),
            'department' => $department,
            'classes' => $classes));
    }

    // handles submission of new class object
    public function newPostAction(Request $request, $dept_id) {
        $this->checkPermission(self::PERMISSION_CREATE);

        $class = new PearClasses();

        $department = $this->getDoctrine()
        ->getRepository('AppBundle:PearDepartments')
        ->find($dept_id);

        $class->setDepartment($department);


        $form = $this->createForm(new ClassType(), $class,
            array('action' => $this->generateUrl('_admin_classes_new_post', array('dept_id' => $dept_id))));

        $form->handleRequest($request);

        if ($form->isValid()) {

            $request = $this->container->get('request');
            $classes_checked = $request->request->get('class');

            $classes = $this->getDoctrine()
            ->getRepository('AppBundle:PearClasses')
            ->findAll();

            //add prereqs to class
            foreach ($classes as $prereq) {
                if (($classes_checked!=null) && in_array($prereq->getId(), $classes_checked))
                {
                    $class->addPrereq($prereq);
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($class);
            $em->flush();


            $this->logAction("New class ".$class->getId()." has been created as ".$class->getName(), self::LOGTYPE_CREATE, $class->getId());

            $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'New Class Created!'
                );

            return $this->redirect($this->generateUrl('_admin_departments_show', array('id'=>$department->getId())));
        }

         //TODO what do we want to do otherwise?
        return $this->render('AppBundle:Admin:Departments/showDepartment.html.twig', array(
            'department' => $department
            ));
    }

    // Shows class details
    public function showAction($id){
     $this->checkPermission(self::PERMISSION_VIEW);

     $class = $this->getDoctrine()
     ->getRepository('AppBundle:PearClasses')
     ->find($id);

     if (!$class) {
        throw $this->createNotFoundException(
            'No class found for id '.$id
            );
    }

     if ($this->checkPermission(self::PERMISSION_SUPER, true))
     {
         $sections = $class->getSections();
     }

     else // only show sections for users campus
     {
         $campus= $this->getUser()->getCampus();
         $sections = $class->getSections(); // TODO: use query here
         foreach ($sections as $key => $section)
         {
             if ($section->getTerm()->getCampus() != $campus)
             {
                 unset($sections[$key]);
             }
         }
     }

    return $this->render('AppBundle:Admin:Departments/Classes/showClass.html.twig', array(
        'class' => $class,
        'sections' => $sections
    ));
}

    // edit a class
public function editAction($id){
    $this->checkPermission(self::PERMISSION_EDIT);

    $class = $this->getDoctrine()
    ->getRepository('AppBundle:PearClasses')
    ->find($id);

    $dept = $class->getDepartment();

    $department = $this->getDoctrine()
    ->getRepository('AppBundle:PearDepartments')
    ->find($dept);

    //get classes in dept to render table for adding prereqs
    $classes = $this->getDoctrine()->getRepository('AppBundle:PearClasses')->findAll();

    $form = $this->createForm(new ClassType(), $class,
        array('action' => $this->generateUrl('_admin_classes_update', array('id' => $id))));


    return $this->render('AppBundle:Admin:Departments/Classes/editClass.html.twig', array(
        'form' => $form->createView(),
        'department' => $department,
        'class' => $class,
        'classes' => $classes));

}

    // handle form submission for updated class
public function updateAction($id, Request $request){
    $this->checkPermission(self::PERMISSION_EDIT);

    $class = $this->getDoctrine()
    ->getRepository('AppBundle:PearClasses')
    ->find($id);

    $dept_id = $class->getDepartment()->getId();
    $department = $class->getDepartment();


    $form = $this->createForm(new ClassType(), $class,
        array('action' => $this->generateUrl('_admin_classes_update', array('id' => $id))));

    $form->handleRequest($request);

    if ($form->isValid()) {
        $request = $this->container->get('request');
        $classes_checked = $request->request->get('class');

        $classes = $this->getDoctrine()
            ->getRepository('AppBundle:PearClasses')
            ->findAll();

        //add prereqs to class
        foreach ($classes as $prereq) {
            if ($classes_checked!=null && in_array($prereq->getId(), $classes_checked))
            {
                $class->addPrereq($prereq);
            }
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($class);
        $em->flush();

        $this->logAction($class->getName()." information updated", self::LOGTYPE_EDIT, $class->getId());

        $flashMessage = $this->get('session')->getFlashBag()->add(
            'notice',
            'Class Updated!'
            );
        return $this->redirect($this->generateUrl(
            '_admin_departments_show',
            array('id' => $dept_id)
            ));
    }
    $this->logAction("Failed attempt at updating class information", self::LOGTYPE_EDIT, $class->getId());
        //TODO what do we do on failure here
    $flashMessage = $this->get('session')->getFlashBag()->add(
        'notice',
        'Class NOT updated!'
        );

    return $this->redirect($this->generateUrl(
        '_admin_departments_show',
        array('id' => $dept_id)
        ));
}

public function removePrereqAction($class_id, $prereq_id)
{
    $class = $this->getDoctrine()
    ->getRepository('AppBundle:PearClasses')
    ->find($class_id);

    $prereq = $this->getDoctrine()
    ->getRepository('AppBundle:PearClasses')
    ->find($prereq_id);

    $class->removePrereq($prereq);

    $em = $this->getDoctrine()->getManager();
    $em->persist($class);
    $em->flush();

    $this->logAction($prereq->getName().' removed as prereq from '.$class->getName(), self::LOGTYPE_EDIT, $class->getId());
        //TODO what do we do on failure here
    $flashMessage = $this->get('session')->getFlashBag()->add(
        'notice',
        $prereq->getName().' successfully removed as prereq from '.$class->getName()
        );

    return $this->showAction($class_id);
}
}
