<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Reponse;
use AppBundle\Controller\PearController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\PearClassSections;
use AppBundle\Form\Type\AddSectionType;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class SectionsController extends PearController {

    public function indexAction($dept_id, $class_id) {
        $this->checkPermission(self::PERMISSION_VIEW);

        $department = $this->getDoctrine()->getRepository('AppBundle:PearDepartments')->find($dept_id);
        $class = $this->getDoctrine()->getRepository('AppBundle:PearClasses')->find($class_id);
        $sections = $class->getSections();

        return $this->render('AppBundle:Admin:Departments/Classes/Sections/sections.html.twig', array('sections' => $sections,
                    'department' => $department,
                    'class' => $class));
    }

    public function showAction($dept_id, $class_id, $id) {
        $section = $this->getDoctrine()
                ->getRepository('AppBundle:PearClassSections')
                ->find($id);
        $campus = $section->getTerm()->getCampus();

        $this->checkPermission(self::PERMISSION_VIEW, false, null, $campus);

        $department = $this->getDoctrine()
                ->getRepository('AppBundle:PearDepartments')
                ->find($dept_id);
        $class = $this->getDoctrine()->getRepository('AppBundle:PearClasses')->find($class_id);


        if (!$section) {
            throw $this->createNotFoundException(
                    'No section found for id ' . $id
            );
        }
        return $this->render('AppBundle:Admin:Departments/Classes/Sections/showSection.html.twig', array(
                    'section' => $section,
                    'department' => $department,
                    'class' => $class));
    }

    public function deleteAction($dept_id, $class_id, $id) {
        $section = $this->getDoctrine()
                ->getRepository('AppBundle:PearClassSections')
                ->find($id);

        $campus = $section->getTerm()->getCampus();

        $this->checkPermission(self::PERMISSION_DELETE, false, null, $campus);

        $department = $this->getDoctrine()
                ->getRepository('AppBundle:PearDepartments')
                ->find($dept_id);
        $class = $this->getDoctrine()->getRepository('AppBundle:PearClasses')->find($class_id);
        $term_id = $section->getTerm()->getId();
        $this->logAction($section->getClass()->getName()."Section ".$section->getId()." has been deleted", self::LOGTYPE_DELETE, $section->getId());

            $em = $this->getDoctrine()->getManager();
            $em->remove($section);
            $em->flush();
            $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice', 'Section Deleted!'
            );

            return $this->redirect($this->generateURL('_admin_terms_show', array(
                'id'=>$term_id
            )));

    }

    public function editAction($dept_id, $class_id, $id) {
        $section = $this->getDoctrine()
                ->getRepository('AppBundle:PearClassSections')
                ->find($id);

        $campus = $section->getTerm()->getCampus();

        $this->checkPermission(self::PERMISSION_EDIT, false, null, $campus);

        $department = $this->getDoctrine()
                ->getRepository('AppBundle:PearDepartments')
                ->find($dept_id);
        $class = $this->getDoctrine()->getRepository('AppBundle:PearClasses')->find($class_id);


        $form = $this->createForm(new AddSectionType($this->get('security.token_storage')), $section, array('action' => $this->generateUrl('_admin_sections_edit_post', array('id' => $id, 'dept_id' => $dept_id, 'class_id' => $class_id))));

        return $this->render('AppBundle:Admin:Departments/Classes/Sections/editSection.html.twig', array(
                    'form' => $form->createView(), 'section' => $section));
    }

    public function editPostAction($dept_id, $class_id, $id, Request $request) {
        $section = $this->getDoctrine()
                ->getRepository('AppBundle:PearClassSections')
                ->find($id);

        $campus = $section->getTerm()->getCampus();

        $this->checkPermission(self::PERMISSION_EDIT, false, null, $campus);

        $department = $this->getDoctrine()
                ->getRepository('AppBundle:PearDepartments')
                ->find($dept_id);
        $class = $this->getDoctrine()->getRepository('AppBundle:PearClasses')->find($class_id);


        $form = $this->createForm(new AddSectionType($this->get('security.token_storage')), $section, array('action' => $this->generateUrl('_admin_sections_edit_post', array('id' => $id, 'dept_id' => $dept_id, 'class_id' => $class_id))));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $flag = 0;

            if (isset($_POST["Mon"])) {
                $flag += 1;
            }
            if (isset($_POST["Tues"])) {
                $flag += 2;
            }
            if (isset($_POST["Wed"])) {
                $flag += 4;
            }
            if (isset($_POST["Thurs"])) {
                $flag += 8;
            }
            if (isset($_POST["Fri"])) {
                $flag += 16;
            }
            if (isset($_POST["Sat"])) {
                $flag += 32;
            }
            if (isset($_POST["Sun"])) {
                $flag += 64;
            }

            if ($flag == 0) {
                $flashMessage = $this->get('session')->getFlashBag()->add(
                        'error', 'Please enter days of class!'
                );
                return $this->render('AppBundle:Admin:Departments/Classes/Sections/editSection.html.twig', array(
                            'form' => $form->createView(), 'section' => $section));
            }
            $section->setDaysOfWeek($flag);
            $this->logAction($section->getClass()->getName()."Section ".$section->getId()." has been edited", self::LOGTYPE_EDIT, $section->getId());
            $em = $this->getDoctrine()->getManager();
            $em->persist($section);
            $em->flush();

            $flashMessage = $this->get('session')->getFlashBag()->add(
                    'notice', 'Section is edited successfully!'
            );
        }

        return $this->render('AppBundle:Admin:Departments/Classes/Sections/showSection.html.twig', array(
                    'section' => $section,
                    'department' => $department,
                    'class' => $class));
    }

}
