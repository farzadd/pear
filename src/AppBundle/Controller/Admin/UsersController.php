<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Controller\PearController;
use Symfony\Component\Filesystem\Filesystem;
use AppBundle\Entity\PearUsers;
use AppBundle\Entity\PearClasses;
use AppBundle\Entity\PearDepartments;
use AppBundle\Entity\PearCustomAttributes;
use AppBundle\Entity\PearUserCustomAttributes;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Form\Type\AdminRegistrationType;
use AppBundle\Form\Type\AdminEditType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Reponse;

class UsersController extends PearController
{
    public function indexAction() {
        $user = $this->getUser();
        $this->checkPermission(self::PERMISSION_VIEW);
        //check to see if logged-in user is super admin
        if($this->checkPermission(self::PERMISSION_SUPER, true))
        {
            //super admin can see ALL users
            $allUsers = $this->getDoctrine()
            ->getRepository('AppBundle:PearUsers')
            ->findAll();

            $users_needApproval = $this->getDoctrine()->getRepository('AppBundle:PearUsers')->createQueryBuilder('u')
            ->innerJoin('u.roles', 'r')
            ->where('r.role = :role')
            ->setParameter('role', 'ROLE_USER')
            ->getQuery()
            ->getResult();
        }
        else
        {
            $userCampus = $user->getCampus();

            //filter users by campus
            $allUsers = $this->getDoctrine()
            ->getRepository('AppBundle:PearUsers')
            ->findBy(array(
                'campus' => $userCampus
                ));

            $users_needApproval = $this->getDoctrine()->getRepository('AppBundle:PearUsers')->createQueryBuilder('u')
            ->innerJoin('u.roles', 'r')
            ->where('r.role = :role')
            ->andWhere('u.campus = :campus')
            ->setParameter('role', 'ROLE_USER')
            ->setParameter('campus', $userCampus)
            ->getQuery()
            ->getResult();
        }

        $count_per_page = 10;
        $current_page = 1;
        $total_count = count($allUsers);
        $total_pages = ceil($total_count/$count_per_page);

        return $this->render('AppBundle:Admin:Users/users.html.twig',
            array('users' => $allUsers,
                'users_needApproval' => $users_needApproval,
                'totalCount' => $total_count,
                'totalPages' => $total_pages,
                'currentPage' => $current_page));

    }

    public function newAction() {
        $this->checkPermission(self::PERMISSION_CREATE);
        $user = new PearUsers();
        //$classes = $this->getDoctrine()->getRepository('AppBundle:PearClasses')->findAll();
        
        $hasSuper = $this->checkPermission(self::PERMISSION_SUPER, true, "Admin\\Users");
        $form = $this->createForm(new AdminRegistrationType($hasSuper, $this->getUser()->getCampus()), $user, array('action' => $this->generateUrl('_admin_users_new_post')));

        return $this->render('AppBundle:Admin:Users/editUser.html.twig', array(
            'form' => $form->createView(),
            'isEdit' => false));
    }

    public function newPostAction(Request $request) {
        $this->checkPermission(self::PERMISSION_CREATE);

        $user = new PearUsers();

        //$classes = $this->getDoctrine()->getRepository('AppBundle:PearClasses')->findAll();
        $hasSuper = $this->checkPermission(self::PERMISSION_SUPER, true, "Admin\\Users");
        $form = $this->createForm(new AdminRegistrationType($hasSuper, $this->getUser()->getCampus()), $user,
            array('action' => $this->generateUrl('_admin_users_new_post')));

        $form->handleRequest($request);

        if ($form->isValid()) {

            //upload the user photo
            $user->upload();
            $user->setFile(null);

            //check if logged in user is not a super admin, but trying
            //to create one
            $roleNames = [];
            foreach (($form["roles"]->getData()->toArray()) as $role)
                {
                    array_push($roleNames, $role->getName());
                }

            if(!($this->checkPermission(self::PERMISSION_SUPER, true))
                && (in_array("Super Admin", $roleNames)))
            {
                $this->get('session')->getFlashBag()->add('error','You are not authorized to create a Super Administrator!');
                goto exit_function;
            }

            // Check for a duplicated registered email
            if ($user->getEmail() != null)
            {
                if ($this->getDoctrine()
                    ->getRepository('AppBundle:PearUsers')
                    ->findOneBy(array('email' => $user->getEmail())))
                {
                    $this->get('session')->getFlashBag()->add('error','An account with the email address \'' . $user->getEmail() . '\' already exists.');
                    goto exit_function;
                }
            }

            // Check for a duplicated first name + last name + birthday
            if ($this->getDoctrine()
                ->getRepository('AppBundle:PearUsers')
                ->findOneBy(array(
                    'firstName' => $user->getFirstName(),
                    'lastName' => $user->getLastName(),
                    'birthDate' => $user->getBirthDate())))
            {
                $this->get('session')->getFlashBag()->add('error','We believe you have already registered an account. Please contact an administrator to complete registration.');
                goto exit_function;
            }

            // Check to see if password is password
            if ($user->getPassword() == "password")
            {
                $this->get('session')->getFlashBag()->add('error','Your password can not be \'password\'!');
                goto exit_function;
            }

            // Clean the phone numbers
            $user->setPhone(preg_replace("/[^0-9]/","",$user->getPhone()));
            $user->setEmergencyContact(preg_replace("/[^0-9]/","",$user->getEmergencyContact()));

            // Encrypt the password
            $encoderFactory = $this->get('security.encoder_factory');
            $encoder = $encoderFactory->getEncoder($user);
            $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
            $user->setPassword($password);

            // Fill in the automatic fields
            $user->setCreated(time());
            $user->setLastLogin(time());

            // Create the user
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            // Create/Update custom attributes
            $userAttrs = $this->getRequest()->request->get("user");
            if (in_array("customAttrs", $userAttrs))
            {
                $customAtrributes = $this->getRequest()->request->get("user")["customAttrs"];
                if(sizeof($user->getCustomAttributes()) > 0){
                    $usersCA = $user->getCustomAttributes()->toArray();
                }


                foreach ($customAtrributes as $caName => $caValue)
                {
                    if (isset($userCA) && ($tKey = array_search ($caName, $usersCA)) !== false)
                    {
                        $usersCA[$tKey]->setValue($caValue);
                        $em->persist($usersCA[$tKey]);
                        $em->flush();
                    }
                    else
                    {
                        $customAttribute = new PearUserCustomAttributes();
                        $caMain = $this->getDoctrine()->getRepository('AppBundle:PearCustomAttributes')->findOneByName($caName);
                        $customAttribute->setUser($user);
                        $customAttribute->setCustomAttribute($caMain);
                        $customAttribute->setValue($caValue);
                        $em->persist($customAttribute);
                        $em->flush();
                    }
                }
            }

            // Flash message
            $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'User Added Successfully!'
                );
            // Send verfication email
            $hash = $this->generateUserHash($user);
            $mailer = $this->get('mailer');
            $message = $mailer->createMessage()
            ->setSubject('Pear')
            ->setFrom('team@pearprogrammers.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    'AppBundle:Email:registration.html.twig', array(
                        'name' => $user->getFirstName(),
                        'id' => $user->getId(),
                        'hash' => $hash->getRc4Key(),
                        'hash_time' => $hash->getCreated())
                    ),
                'text/html'
                );
            $mailer->send($message);

            return $this->redirect($this->generateUrl(
                '_admin_users_show', array('id' => $user->getId())
                ));
        }

        exit_function:
        return $this->render('AppBundle:Admin:Users/editUser.html.twig', array(
            'id' => $user->getId(), 'form' => $form->createView(),
            'isEdit' => false));
    }

    public function showAction($id){
        $user = $this->getDoctrine()->getRepository('AppBundle:PearUsers')
        ->find($id);

        if (!$user) {
            throw $this->createNotFoundException(
                'No user found for id '.$id
                );
        }

        $campus = $user->getCampus();

    	$this->checkPermission(self::PERMISSION_VIEW, false, null, $campus);

        if (!$user) {
            throw $this->createNotFoundException(
                'No user found for id '.$id
                );
        }

        return $this->render('AppBundle:Admin:Users/showUser.html.twig', array(
            'user' => $user));
    }

    public function editAction($id) {
        $user = $this->getDoctrine()->getRepository('AppBundle:PearUsers')
        ->find($id);

        if (!$user) {
            throw $this->createNotFoundException(
                'No user found for id '.$id
                );
        }

        $campus = $user->getCampus();

        $this->checkPermission(self::PERMISSION_EDIT, false, null, $campus);
        //$classes = $this->getDoctrine()->getRepository('AppBundle:PearClasses')->findAll();
        $hasSuper = $this->checkPermission(self::PERMISSION_SUPER, true, "Admin\\Users");
        
        $form = $this->createForm(new AdminEditType($hasSuper, $this->getUser()->getCampus()), $user,
            array('action' => $this->generateUrl('_admin_users_edit_post', array('id' => $id))));

        return $this->render('AppBundle:Admin:Users/editUser.html.twig', array(
            'id' => $id,
            'form' => $form->createView(),
            'user' => $user,
            'isEdit' => true));
    }

    public function editPostAction($id, Request $request) {
        $user = $this->getDoctrine()->getRepository('AppBundle:PearUsers')
        ->find($id);

        if (!$user) {
            throw $this->createNotFoundException(
                'No user found for id '.$id
                );
        }

        $campus = $user->getCampus();

        $this->checkPermission(self::PERMISSION_EDIT, false, null, $campus);

        //$classes = $this->getDoctrine()->getRepository('AppBundle:PearClasses')->findAll();

        $oldPassword = $user->getPassword();

        $hasSuper = $this->checkPermission(self::PERMISSION_SUPER, true, "Admin\\Users");
        $form = $this->createForm(new AdminEditType($hasSuper, $this->getUser()->getCampus()), $user,
            array('action' => $this->generateUrl('_admin_users_edit_post', array('id' => $id))));

        //roles of the logged in user
        $loggedInUserRoles = array();
        foreach ($this->getUser()->getRoles() as $role)
        {
          array_push($loggedInUserRoles, $role->getRole());
        }

        //roles of the selected user
        $selectedUserRoles = array();
        foreach ($user->getRoles() as $role)
        {
          array_push($selectedUserRoles, $role->getRole());
        }

        $form->handleRequest($request);

        if ($form->isValid()) {

          //roles the logged in user has selected in the form
          $requestedRoles = array();
          foreach ($form["roles"]->getData() as $role)
          {
            array_push($requestedRoles, $role->getRole());
          }
          //prevent non-super admin from editing a super admin profile or
          //making someone a super admin
          if (
          (!in_array("ROLE_SUPERADMIN", $loggedInUserRoles) &&
          (in_array("ROLE_SUPERADMIN", $requestedRoles) || in_array("ROLE_SUPERADMIN", $selectedUserRoles))) ||

          ((!in_array("ROLE_CAMPUSADMIN", $loggedInUserRoles) && !in_array("ROLE_SUPERADMIN", $loggedInUserRoles)) &&
          (in_array("ROLE_CAMPUSADMIN", $requestedRoles) || in_array("ROLE_CAMPUSADMIN", $selectedUserRoles)))
          )
          {
            $this->get('session')->getFlashBag()
            ->add('error','You cannot edit a higher level Administrator account or increase the permission
            level of your own account. Contact a Super Administrator for assistance if you wish to do so');
            return $this->redirect($this->generateUrl(
                '_admin_users_show', array('id' => $id)
                ));
          }
            // Check for a duplicated registered email
            if ($user->getEmail() != null)
            {
                if ($oldUser = $this->getDoctrine()
                    ->getRepository('AppBundle:PearUsers')
                    ->findOneBy(array('email' => $user->getEmail())))
                {
                    if ($oldUser->getId() != $user->getId())
                    {
                        $this->get('session')->getFlashBag()->add('error','An account with the email address \'' . $user->getEmail() . '\' already exists.');
                        goto exit_function;
                    }
                }
            }

            // Check to see if password is password
            if ($user->getPassword() == "password")
            {
                $this->get('session')->getFlashBag()->add('error','Your password can not be \'password\'!');
                goto exit_function;
            }

            // Clean the phone numbers
            $user->setPhone(preg_replace("/[^0-9]/","",$user->getPhone()));
            $user->setEmergencyContact(preg_replace("/[^0-9]/","",$user->getEmergencyContact()));

            // Encrypt the password
            if ($user->getPassword() == null)
            {
                $user->setPassword($oldPassword);
            }
            else {
                $encoderFactory = $this->get('security.encoder_factory');
                $encoder = $encoderFactory->getEncoder($user);
                $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
                $user->setPassword($password);
            }

            $user->upload();

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            // Create/Update custom attributes
            $user_form = $this->getRequest()->request->get("user");
            if (array_key_exists("customAttrs", $user_form)) {

                $customAtrributes = $user_form["customAttrs"];

                if (sizeof($user->getCustomAttributes()) > 0) {
                    $usersCA = $user->getCustomAttributes()->toArray();
                }

                foreach ($customAtrributes as $caName => $caValue)
                {
                    if (isset($usersCA) && ($tKey = array_search ($caName, $usersCA)) !== false)
                    {
                        $usersCA[$tKey]->setValue($caValue);
                        $em->persist($usersCA[$tKey]);
                        $em->flush();
                    }
                    else
                    {
                        $customAttribute = new PearUserCustomAttributes();
                        $caMain = $this->getDoctrine()->getRepository('AppBundle:PearCustomAttributes')->findOneByName($caName);
                        $customAttribute->setUser($user);
                        $customAttribute->setCustomAttribute($caMain);
                        $customAttribute->setValue($caValue);
                        $em->persist($customAttribute);
                        $em->flush();
                    }
                }
            }

            $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'User Edited Successfully!'
                );

            $this->logAction("Edited User ".$user->getFirstName()." ".$user->getLastName()."under ".$user->getId(), self::LOGTYPE_EDIT, $user->getId());

            return $this->showAction($id);
        }

        exit_function:
        return $this->render('AppBundle:Admin:Users/editUser.html.twig', array(
            'id' => $id,
            'form' => $form->createView(),
            'user' => $user,
            'isEdit' => true));
    }

    public function deleteAction($id)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:PearUsers')
        ->find($id);

        if (!$user) {
            throw $this->createNotFoundException(
                'No user found for id '.$id
                );
        }

        $campus = $user->getCampus();

        $this->checkPermission(self::PERMISSION_EDIT, false, null, $campus);

        if (sizeof($user->getEnrolls()) > 0) {
         $flashMessage = $this->get('session')->getFlashBag()->add(
            'error',
            'User enrolled in section cannot be deleted'

            );
         return $this->render('AppBundle:Admin:Users/showUser.html.twig', array(
            'user' => $user));
     }

     $em = $this->getDoctrine()->getManager();

     $userLogs = $this->getDoctrine()
            ->getRepository('AppBundle:PearUserLogs')
            ->findBy(array('user' => $user->getId()));

     foreach ($userLogs as $userLog)
        $em->remove($userLog);

     $userCustomAttributes = $this->getDoctrine()
            ->getRepository('AppBundle:PearUserCustomAttributes')
            ->findBy(array('user' => $user->getId()));

     foreach ($userCustomAttributes as $userCustomAttribute)
        $em->remove($userCustomAttribute);

     $userHash = $this->getDoctrine()
            ->getRepository('AppBundle:PearUserHashes')
            ->findOneBy(array('user' => $user->getId()));

     if ($userHash != null)
        $em->remove($userHash);
     $em->flush();

     $this->logAction("Deleted User ".$user->getFirstName()." ".$user->getLastName()."with id ".$user->getId(), self::LOGTYPE_DELETE, $user->getId());

     $em->remove($user);
     $em->flush();

     $flashMessage = $this->get('session')->getFlashBag()->add(
        'notice',
        'User Deleted!'
        );

     return $this->indexAction();
 }
}
