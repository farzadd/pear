<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Reponse;
use AppBundle\Controller\PearController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\PearDepartments;
use AppBundle\Form\Type\DepartmentType;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class DepartmentsController extends PearController
{
    public function indexAction() { 
      $this->checkPermission(self::PERMISSION_VIEW);

      $departments = $this->getDoctrine()->getRepository('AppBundle:PearDepartments')
      ->findAll();

      return $this->render('AppBundle:Admin:Departments/departments.html.twig', 
       array('departments' => $departments));
  }

	//render form for new department
  public function newAction() {
    $this->checkPermission(self::PERMISSION_CREATE);

    $dept = new PearDepartments();

    $form = $this->createForm(new DepartmentType(), $dept,
        array('action' => $this->generateUrl('_admin_departments_new_post')));

    return $this->render('AppBundle:Admin:Departments/newDepartment.html.twig', array(
        'form' => $form->createView()));

}

public function newPostAction(Request $request) {
    $this->checkPermission(self::PERMISSION_CREATE);
    $dept = new PearDepartments();

    $form = $this->createForm(new DepartmentType(), $dept,
        array('action' => $this->generateUrl('_admin_departments_new_post')));

    $form->handleRequest($request);

    if ($form->isValid())
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($dept);
        $em->flush();
        
        $this->logAction("New department ".$dept->getId()." has been created as ".$dept->getname(), self::LOGTYPE_CREATE, $dept->getId());
        $flashMessage = $this->get('session')->getFlashBag()->add(
            'notice',
            'New Depatment Added!'
            );

//        return $this->showAction($dept->getId());
        return $this->redirect($this->generateUrl('_admin_departments_show', array('id'=>$dept->getId())));
    }
    $this->logAction("Failed attempt at creating department ".$dept->getName()."with id ".$dept->getId(), self::LOGTYPE_CREATE, $dept->getId());
    $flashMessage = $this->get('session')->getFlashBag()->add(
        'notice',
        'Department Creation Unsuccessful'
        );

    return $this->render('AppBundle:Admin:Departments/newDepartment.html.twig', array( 'form' => $form->createView()));
}

public function showAction($id){
   $this->checkPermission(self::PERMISSION_VIEW);
   
   $department = $this->getDoctrine()
   ->getRepository('AppBundle:PearDepartments')
   ->find($id);

   if (!$department) {
    throw $this->createNotFoundException(
        'No department found for id '.$id
        );
}

return $this->render('AppBundle:Admin:Departments/showDepartment.html.twig', array(
    'department' => $department));
}

public function editAction($id) {
    $this->checkPermission(self::PERMISSION_EDIT);
    $department = $this->getDoctrine()->getRepository('AppBundle:PearDepartments')->find($id);
    $form = $this->createForm(new DepartmentType($classes), $department, 
        array('action' => $this->generateUrl('_admin_departments_edit_post', array('id' => $id))));
    return $this->render('AppBundle:Admin:Departments/editDepartments.html.twig', array('department'=>$department,
        'form' => $form->createView()
        ));
}

public function editPostAction($id, Request $request) {
    $this->checkPermission(self::PERMISSION_EDIT);

    $department = $this->getDoctrine()->getRepository('AppBundle:PearDepartments')->find($id);

    $form = $this->createForm(new DepartmentType($classes), $department, 
        array('action' => $this->generateUrl('_admin_departments_edit_post', array('id' => $id))));

    $form->handleRequest($request);

    if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $em->persist($department);
        $em->flush();
    }
    $this->logAction("Changes to ".$department->getName()." information has been made", self::LOGTYPE_EDIT, $department->getId());
    return $this->redirect($this->generateUrl(
        '_admin_departments'
        ));

}
}