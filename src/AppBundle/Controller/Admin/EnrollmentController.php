<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Reponse;
use AppBundle\Controller\PearController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use AppBundle\Entity\PearSections;
use AppBundle\Entity\PearClasses;
use AppBundle\Entity\PearDepartments;

class EnrollmentController extends PearController
{

  /* iterates through sections offered. Gets enumeration of courses from
  * those sections and then the departments from those courses. Passes
  * user id of user to be enrolled through to classesByDepartmentAction
  * @param user_id is the ID of the user to be enrolled
  * @return list of departments offering sections this term
  */
  public function activeDepartmentsIndexAction($user_id)
  {
    $user = $this->getDoctrine()->getRepository('AppBundle:PearUsers')
    ->find($user_id);

    if (!$this->checkPermission(self::PERMISSION_VIEW, true))
    {
        if ($this->getUser() != $user || !$this->checkPermission(self::PERMISSION_CREATE, true, 'Student\\Index'))
            throw new AccessDeniedHttpException('Unauthorized access!');
    }

    $campuses = $user->getCampus();

    //get term ID of first campus in user's campuses array
    $term_id = $campuses->getActiveTerm()->getId();


    //get sections offered this term
    $sections = $this->getDoctrine()->getRepository('AppBundle:PearCampusTerms')->find($term_id)->getSections();

    //holds enumeration of classes
    $classes = array();

    //find unique classes through section
    foreach ($sections as $section)
    {
      $class = $section->getClass();
      if (!in_array($class, $classes))
      {
        array_push($classes, $class);
      }
    }

    //holds enumeration of departments
    $departments = array();

    foreach ($classes as $class)
    {
      $department = $class->getDepartment();
      if (!in_array($department, $departments))
      {
        array_push($departments, $department);
      }
    }

    return $this->render('AppBundle:Admin:Enrollment/activeDepartments.html.twig',
      array('departments' => $departments,
        'user_id' => $user_id,
        'term_id' => $term_id));
  }

 /* iterates through sections offered. Gets enumeration of courses from
  * those sections that belong to the specified department. Passes
  * user id of user to be enrolled through to sectionsByClassAction
  * @param $department_id is ID of department for which we are trying to
  * @param user_id is the ID of the user to be enrolled
  * show all courses this term
  * @return list of departments offering sections this term
  */
 public function classesByDepartmentAction($department_id, $user_id, $term_id)
 {
    $user = $this->getDoctrine()->getRepository('AppBundle:PearUsers')
    ->find($user_id);

    if (!$this->checkPermission(self::PERMISSION_VIEW, true))
    {
        if ($this->getUser() != $user || !$this->checkPermission(self::PERMISSION_CREATE, true, 'Student\\Index'))
            throw new AccessDeniedHttpException('Unauthorized access!');
    }

    //get sections offered this term
   $sections = $this->getDoctrine()->getRepository('AppBundle:PearCampusTerms')->find($term_id)->getSections();

    //holds enumeration of classes
   $classes = array();

    //find unique classes through section
    //that belong to dept with $department_id
   foreach ($sections as $section)
   {
    $class = $section->getClass();
    if (!in_array($class, $classes) &&
      $class->getDepartment()->getId() == $department_id)
    {
      array_push($classes, $class);
    }
  }
  return $this->render('AppBundle:Admin:Enrollment/activeClasses.html.twig',
    array('classes' => $classes,
      'user_id' => $user_id,
      'term_id' => $term_id));

}
  /* iterates through sections offered. Gets enumeration of courses from
  * those sections that belong to the specified course. Passes
  * user id of user to be enrolled through to enrollStudentAction
  * @param user_id is the ID of the user to be enrolled
  * @param $course_id is ID of department for which we are trying to
  * show all courses this term
  * @return list of departments offering sections this term
  */

  public function sectionsByClassAction($class_id, $user_id, $term_id)
  {
    $user = $this->getDoctrine()->getRepository('AppBundle:PearUsers')
    ->find($user_id);

    if (!$this->checkPermission(self::PERMISSION_VIEW, true))
    {
        if ($this->getUser() != $user || !$this->checkPermission(self::PERMISSION_CREATE, true, 'Student\\Index'))
            throw new AccessDeniedHttpException('Unauthorized access!');
    }

    //find sections by term_id and class_id
    $sections = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->findBy(
      array('term' => $term_id,
        'class' => $class_id)
      );

    //TODO since I don't have a show action what should I do for this render?
    return $this->render('AppBundle:Admin:Enrollment/classSections.html.twig',
      array('sections' => $sections, 'user_id' => $user_id, 'class_id' => $class_id));
  }

  /* enrolls the student in the selected course, based on output
  * of hasPrereqAction
  * @param $section_id is ID of section
  * @param user_id is the ID of the user to be enrolled
  * @return view of student dashboad with success flash message
  */
  public function enrollStudentAction($section_id, $user_id)
  {
    $user = $this->getDoctrine()->getRepository('AppBundle:PearUsers')
    ->find($user_id);

    if (!$this->checkPermission(self::PERMISSION_CREATE, true))
    {
        if ($this->getUser() != $user || !$this->checkPermission(self::PERMISSION_CREATE, true, 'Student\\Index'))
            throw new AccessDeniedHttpException('Unauthorized access!');
    }

    $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($section_id);

    if ($section->getTerm()->getCampus()->getEnrollmentTerm() == null) {
        $flashMessage = $this->get('session')->getFlashBag()->add(
            'notice', "Sorry, enrollment is closed!"
        );
        if ($this->checkPermission(self::PERMISSION_VIEW, true, 'Admin\Enrollment'))
                return $this->redirect($this->generateUrl("_admin_enrollment_departments_index", array('user_id' => $user_id)));
        else if ($this->checkPermission(self::PERMISSION_VIEW, true, 'Admin\Users'))
              return $this->redirect($this->generateUrl('_admin_users_show', array('id' => $user_id)));
    }

    $enrolledStudents = $section->getStudents()->toArray();

    if (count($enrolledStudents) >= $section->getMaxStudents())
    {
      $flashMessage = $this->get('session')->getFlashBag()->add(
        'notice',
        "Sorry, the section for {$section->getClass()->getName()} is full!"
        );
      if ($this->checkPermission(self::PERMISSION_VIEW, true, 'Admin\Enrollment'))
                return $this->redirect($this->generateUrl("_admin_enrollment_departments_index", array('user_id' => $user_id)));
      else if ($this->checkPermission(self::PERMISSION_VIEW, true, 'Admin\Users'))
              return $this->redirect($this->generateUrl('_admin_users_show', array('id' => $user_id)));
    }

    //is the student already enrolled in this section?
    if ($this->isAlreadyEnrolledAction($section, $user, $enrolledStudents))
    {
        $flashMessage = $this->get('session')->getFlashBag()->add(
            'notice',
            "{$user->getfirstName()} is already enrolled in {$section->getClass()->getName()}"
            );
        if ($this->checkPermission(self::PERMISSION_VIEW, true, 'Admin\Enrollment'))
            return $this->redirect($this->generateUrl("_admin_enrollment_departments_index", array('user_id' => $user_id)));
        else if ($this->checkPermission(self::PERMISSION_VIEW, true, 'Admin\Users'))
            return $this->redirect($this->generateUrl('_admin_users_show', array('id' => $user_id)));
    }

    //does the student have the necessary prereqs?
    if ($this->hasPrereqsAction($section, $user))
    {
        $user->addEnroll($section);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        if ($user != $this->getUser())
            $this->logAction($user->getFirstName() . " " . $user->getLastName()." has been added to section ".$section->getClass()->getName()." in section ".$section->getId(), self::LOGTYPE_CREATE, $user->getId());

        $flashMessage = $this->get('session')->getFlashBag()->add(
            'notice',
            "{$user->getfirstName()} satisfies all prerequisites for {$section->getClass()->getName()}
            and has been added to section {$section->getId()}"
            );
         if ($this->checkPermission(self::PERMISSION_VIEW, true, 'Admin\Users'))
            return $this->redirect($this->generateUrl('_admin_users_show', array('id' => $user_id)));
         else if ($this->checkPermission(self::PERMISSION_VIEW, true, 'Student\Index'))
            return $this->redirect($this->generateUrl("_student_currentYear"));
    }
    else
    {
      $flashMessage = $this->get('session')->getFlashBag()->add(
        'error',
        "{$user->getfirstName()} is missing prerequisite
        needed to enroll in {$section->getClass()->getName()}. Contact an Administrator if
        you beleive an error has been made. If you would like to request an exemption
        from the prerequisite, use the button on this page."
        );
    }
    return $this->redirect($this->generateUrl('_admin_enrollment_sections',
      array('class_id' => $section->getClass()->getId(), 'user_id' => $user_id, 'term_id' => $section->getTerm()->getId())));
  }

  /* checks to see if user has necessary prereqs to enroll in section.
  * returns true and if student satisfies prereqs to enroll in section
  * and false otherwise
  * @param $section is section user has selected
  * @param $user is user to be enrolled
  * @return true if user has prereqs, false otherwise
  */
  public function hasPrereqsAction($section, $user)
  {
    //prereqs for the section user wants to enroll in
    $classPrereqs = $section->getClass()->getPrereqs()->toArray();
    //prereqs added manually by admin
    $userPrereqs = $user->getPrereqs()->toArray();

    if ($this->hasManuallyAddedPrereqsAction($classPrereqs, $userPrereqs))
    {
      return true;
    }

    if ($this->hasEarnedPrereqsAction($classPrereqs, $user))
    {
      return true;
    }
    else return false;
  }

  //determines if student has passed prerequsite(s)
  //@classPrereqs is array of prereqs for section enrollment target
  //@user is user who wants to enroll
  //@return boolean
  public function hasEarnedPrereqsAction($classPrereqs, $user)
  {
    $sections = array();
    $sectionsPassed = 0;
    $pass = 50;
    //array of sections $user has taken
    $userSections = $user->getEnrolls()->toArray();

    //ClassPrereqs is array of classes
    foreach ($classPrereqs as $prereq)
    {
      //now sections contains ALL sections for a class
      $sections = $this->getDoctrine()
      ->getRepository('AppBundle:PearClassSections')
      ->findByClass($prereq);

      //now filter out sections that don't contain $user
      foreach ($sections as $section)
      {
      //if user wasn't enrolled in $section, remove from $sections
        if (!in_array($section, $userSections))
        {
          $pos = array_search($section, $sections);
          unset($sections[$pos]);
        }
      }
    //now $sections contains only sections $user was in

      //now check if $user passed $sections
      foreach ($sections as $section)
      {
        if ($this->getOverallGrade($user, $section) >= $pass)
        {
          $sectionsPassed++;
        }
      }
    }
    //check size of sectionsPassed; if it's same size or
    //bigger than classPrereqs, student has satisfied prereqs
    if ($sectionsPassed >= count($classPrereqs))
    {
      return true;
    }
    else return false;
  }



  public function hasManuallyAddedPrereqsAction($classPrereqs, $userPrereqs)
  {
    foreach ($classPrereqs as $classPrereq)
    {
      if (!in_array($classPrereq, $userPrereqs))
        //user hasn't satisfied a prereq for $section
        return false;
    }
    //user has all necessary prereqs
    return true;
  }


  //@param $section
  //@param $user
  //@param $enrolledStudents is array of students enrolled in $section
  //@return true if $user is in $enrolledStudents, false otherwise
  public function isAlreadyEnrolledAction($section, $user, $enrolledStudents)
  {
    if (in_array($user, $enrolledStudents))
    {
    //user is already enrolled in section
      return true;
    }
    return false;
  }

  public function getOverallGrade($student, $section)
  {
    $gradePercentage = $this->calcStudentGrade($student, $section, false);
    return $gradePercentage;

  }
    //remove student from selected section
  public function removeStudentFromSectionAction($user_id, $section_id)
  {
    $user = $this->getDoctrine()->getRepository('AppBundle:PearUsers')->find($user_id);

    if (!$this->checkPermission(self::PERMISSION_DELETE, true))
    {
        if ($this->getUser() != $user || !$this->checkPermission(self::PERMISSION_DELETE, true, 'Student\\Index'))
            throw new AccessDeniedHttpException('Unauthorized access!');
    }

    $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($section_id);

    $user->removeEnroll($section);
    $em = $this->getDoctrine()->getManager();
    $em->persist($user);
    $em->flush();

    $this->logAction($user->getFirstName()." " . $user->getLastName(). " has been removed from section " . $section->getId(), self::LOGTYPE_CREATE, $user->getId());
    $flashMessage = $this->get('session')->getFlashBag()->add(
      'notice',
      "{$user->getFirstName()} {$user->getLastName()} has been removed from {$section->getClass()->getName()} {$section->getId()}"
      );

    return $this->redirect($this->generateUrl('_admin_sections_show',
      array('dept_id' => $section->getClass()->getDepartment()->getId(),
       'class_id' => $section->getClass()->getId(),
       'id' => $section->getId())));
  }
}
