<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Controller\PearController;
use AppBundle\Entity\PearAnnouncements;
use AppBundle\Form\Type\AnnouncementsType;

/**
 * PearAnnouncements controller.
 *
 */
class AnnouncementsController extends PearController
{

    /**
     * Lists all PearAnnouncements entities.
     *
     */
    public function indexAction()
    {
        $this->checkPermission(self::PERMISSION_VIEW);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:PearAnnouncements')->findBy(array(), array('pinned'=>'DESC', 'created'=>'DESC'), 10);

        return $this->render('AppBundle:Admin:Announcements/index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new PearAnnouncements entity.
     *
     */
    public function createAction(Request $request) 
    {
        $this->checkPermission(self::PERMISSION_CREATE);

        $entity = new PearAnnouncements();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity->setCreatedBy($this->getUser());
            $entity->setCreated(time());
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $this->logAction("Announcement ".$entity->getId()." created.", self::LOGTYPE_CREATE, $entity->getId());
            return $this->redirect($this->generateUrl('_admin_announcements_show', array('id' => $entity->getId())));
        }

        return $this->render('AppBundle:Admin:Announcements/new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a PearAnnouncements entity.
     *
     * @param PearAnnouncements $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(PearAnnouncements $entity)
    {
        $this->checkPermission(self::PERMISSION_CREATE);
        $form = $this->createForm(new AnnouncementsType(), $entity, array(
            'action' => $this->generateUrl('_admin_announcements_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new PearAnnouncements entity.
     *
     */
    public function newAction()
    {
        $this->checkPermission(self::PERMISSION_CREATE);
        $entity = new PearAnnouncements();
        $form   = $this->createCreateForm($entity);

        return $this->render('AppBundle:Admin:Announcements/new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PearAnnouncements entity.
     *
     */
    public function showAction($id)
    {
        $this->checkPermission(self::PERMISSION_VIEW);
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:PearAnnouncements')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PearAnnouncements entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Admin:Announcements/show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing PearAnnouncements entity.
     *
     */
    public function editAction($id)
    {
        $this->checkPermission(self::PERMISSION_EDIT);
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:PearAnnouncements')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PearAnnouncements entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AppBundle:Admin:Announcements/edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a PearAnnouncements entity.
    *
    * @param PearAnnouncements $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(PearAnnouncements $entity)
    {
        $this->checkPermission(self::PERMISSION_EDIT);
        $form = $this->createForm(new AnnouncementsType(), $entity, array(
            'action' => $this->generateUrl('_admin_announcements_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing PearAnnouncements entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $this->checkPermission(self::PERMISSION_EDIT);
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:PearAnnouncements')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PearAnnouncements entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->logAction("Announcement ".$entity->getId()." has been updated", self::LOGTYPE_EDIT, $entity->getId());
            return $this->redirect($this->generateUrl('_admin_announcements_edit', array('id' => $id)));
        }

        return $this->render('AppBundle:Admin:Announcements/edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a PearAnnouncements entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $this->checkPermission(self::PERMISSION_DELETE);
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:PearAnnouncements')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PearAnnouncements entity.');
            }

            $id = $entity->getId();
            
            $em->remove($entity);
            $em->flush();
            $this->logAction("Announcement".$id." has been deleted", self::LOGTYPE_DELETE, $id);
        }

        return $this->redirect($this->generateUrl('_admin_announcements'));
    }

    /**
     * Creates a form to delete a PearAnnouncements entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        $this->checkPermission(self::PERMISSION_VIEW);
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('_admin_announcements_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
