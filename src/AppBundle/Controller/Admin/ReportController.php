<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Controller\PearController;
use AppBundle\Entity\PearCampusTerms;
use AppBundle\Entity\PearCampuses;
use Appbundle\Entity\PearClassSections;
use AppBundle\Form\Type\AddSectionType;

class ReportController extends PearController {

    public function termReportAction($id) {
        $this->checkPermission(self::PERMISSION_VIEW);

        $term = $this->getDoctrine()
                ->getRepository('AppBundle:PearCampusTerms')
                ->find($id);

        $sections = $term->getSections();
        $term_report = array();

        foreach ($sections as $section) {
            $students = $section->getStudents();
            $em = $this->getDoctrine()->getManager();

            $studentReportCard = array();
            $overallMark = 0;
            $num_student = sizeof($students);
            $attendancesCount = 0;
            foreach ($students as $student) {
                $reportCard = array();

                $percentage = $this->calcStudentGrade($student->getId(), $section->getId(), false);

                $overallMark = $overallMark + $percentage;
                $reportCard["percentage"] = $percentage;

                $attendances = $section->getAttendance();
                $attendancesCount = sizeof($attendances);

                $presents = $student->getPresentAttendance();
                $presentCount = 0;
                foreach ($presents as $present) {
                    if ($present->getSection()->getId() == $section->getId())
                        $presentCount++;
                }


                $lates = $student->getLateAttendance();
                $lateCount = 0;
                foreach ($lates as $late) {
                    if ($late->getSection()->getId() == $section->getId())
                        $lateCount++;
                }

                $absents = $student->getAbsentAttendance();
                $absentCount = 0;
                foreach ($absents as $absent) {
                    if ($absent->getSection()->getId() == $section->getId())
                        $absentCount++;
                }
                $reportCard["attendancesCount"] = $attendancesCount;
                $reportCard["presentCount"] = $presentCount;
                $reportCard["lateCount"] = $lateCount;
                $reportCard["absentCount"] = $absentCount;
                $studentReportCard[$student->getId()] = $reportCard;
            }
            if($num_student>0) {
                //$overallMark /= $num_student;
            }

            $final = array();
            $final['students'] = $students;
            $final['studentReportCard'] = $studentReportCard;
            $final['attendancesCount']= $attendancesCount;
            $final['overallMark'] = $overallMark;
            $term_report[$section->getId()]= $final;
        }



        return $this->render('AppBundle:Admin:Report/report_term.html.twig', array(
                    'term' => $term,
                    'term_reports' => $term_report
        ));
    }


}
