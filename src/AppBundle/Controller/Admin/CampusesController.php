<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Reponse;
use AppBundle\Controller\PearController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\PearCampuses;
use AppBundle\Form\Type\CampusType;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class CampusesController extends PearController
{
    public function indexAction() {
        $this->checkPermission(self::PERMISSION_SUPER);

        $campuses = $this->getDoctrine()->getRepository('AppBundle:PearCampuses')
        ->findAll();

        return $this->render('AppBundle:Admin:Campuses/campuses.html.twig',
            array('campuses' => $campuses));
    }

    //renders form for new Campus
    public function newAction() {
        $this->checkPermission(self::PERMISSION_CREATE);

        $campus = new PearCampuses();

        $form = $this->createForm(new CampusType(), $campus,
            array('action' => $this->generateUrl('_admin_campuses_new_post')));

        return $this->render('AppBundle:Admin:Campuses/newCampus.html.twig', array(
            'form' => $form->createView()));

    }

    // handles submission of new campus object
    public function newPostAction(Request $request) {
        $this->checkPermission(self::PERMISSION_CREATE);

        $campus = new PearCampuses();

        $form = $this->createForm(new CampusType(), $campus,
            array('action' => $this->generateUrl('_admin_campuses_new_post')));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($campus);
            $em->flush();

            $this->logAction("New campus id ".$campus->getId()." has been created as ".$campus->getName(), self::LOGTYPE_CREATE, $campus->getId());

            $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'New Campus Created!'
                );

            //return $this->showAction($campus->getId());
            return $this->redirect($this->generateUrl('_admin_campuses_show', array('id' => $campus->getId())));

        }

        return $this->render('AppBundle:Admin:Campuses/newCampus.html.twig', array(
            'form' => $form->createView()));
    }

    //fetch a particular campus
    public function showAction($id)
    {
        $campus = $this->getDoctrine()
        ->getRepository('AppBundle:PearCampuses')
        ->find($id);

        $this->checkPermission(self::PERMISSION_VIEW, false, null, $campus);

        if (!$campus) {
            throw $this->createNotFoundException(
                'No campus found for id '.$id
                );
        }

        $repo = $this->getDoctrine()->getRepository('AppBundle:PearUsers');
        $pendingRegisterations = $repo->createQueryBuilder('u')
                                ->innerJoin('u.roles', 'r')
                                ->where('r.role = :role')
                                ->andWhere('u.campus = :campus')
                                ->setParameter('role', 'ROLE_USER')
                                ->setParameter('campus', $campus)
                                ->getQuery()
                                ->getResult();

        $enrollmentRequests = $this->getDoctrine()->getRepository('AppBundle:PearPrereqExemptionRequests')->createQueryBuilder('r')
                                ->innerJoin('r.user', 'u')
                                ->where('u.campus = :campus')
                                ->setParameter('campus', $campus)
                                ->getQuery()
                                ->getResult();

        $openTerms = $this->getDoctrine()->getRepository('AppBundle:PearCampusTerms')->createQueryBuilder('c')
                                ->where('c.campus = :campus')
                                ->andWhere('c.termClosed = :active')
                                ->setParameter('campus', $campus)
                                ->setParameter('active', false)
                                ->getQuery()
                                ->getResult();

        $inactiveTerms = $this->getDoctrine()->getRepository('AppBundle:PearCampusTerms')->createQueryBuilder('c')
                                ->where('c.campus = :campus')
                                ->andWhere('c.termClosed = :active')
                                ->setParameter('campus', $campus)
                                ->setParameter('active', true)
                                ->getQuery()
                                ->getResult();

        return $this->render('AppBundle:Admin:Campuses/showCampus.html.twig', array(
            'campus' => $campus,
            'openTerms' => $openTerms,
            'inactiveTerms' => $inactiveTerms,
            'pendingRegisterations' => count($pendingRegisterations),
            'enrollmentRequests' => count($enrollmentRequests)
            ));
    }

    //edit a campus
    public function editAction($id)
    {
        $campus = $this->getDoctrine()
        ->getRepository('AppBundle:PearCampuses')
        ->find($id);

        $this->checkPermission(self::PERMISSION_EDIT, false, null, $campus);

        $form = $this->createForm(new CampusType(), $campus,
            array('action' => $this->generateUrl('_admin_campuses_edit_post', array('id' => $id))));


        return $this->render('AppBundle:Admin:Campuses/newCampus.html.twig', array(
            'form' => $form->createView()));
    }


    //handle form submit for updated campus info
    public function editPostAction($id, Request $request)
    {
        $campus = $this->getDoctrine()
        ->getRepository('AppBundle:PearCampuses')
        ->find($id);

        $this->checkPermission(self::PERMISSION_EDIT, false, null, $campus);

        $form = $this->createForm(new CampusType(), $campus,
            array('action' => $this->generateUrl('_admin_campuses_edit_post', array('id' => $id))));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($campus);
            $em->flush();

            $this->logAction($campus->getName()." information has been updated", self::LOGTYPE_EDIT, $campus->getId());
            $flashMessage = $this->get('session')->getFlashBag()->add(
                'notice',
                'Campus Updated!'
                );
        }

        return $this->redirect($this->generateUrl(
            '_admin_campuses_show',
            array('id' => $id)
            ));
    }

    public function deleteAction($id)
    {
       $campus = $this->getDoctrine()
       ->getRepository('AppBundle:PearCampuses')
       ->find($id);

       $this->checkPermission(self::PERMISSION_DELETE, false, null, $campus);

       $em = $this->getDoctrine()->getManager();
       if (sizeof($campus->getTerms()) > 0)
       {
        $flashMessage = $this->get('session')->getFlashBag()->add(
        'error',
        'Campus with an associated session cannot be deleted');
         return $this->indexAction();
       }
       $em->remove($campus);

       $this->logAction($campus->getName()." has been deleted", self::LOGTYPE_DELETE, $campus->getId());

       $em->flush();


       $flashMessage = $this->get('session')->getFlashBag()->add(
        'notice',
        'Campus Deleted!'
        );

       //return $this->indexAction();
       return $this->redirect($this->generateUrl(
            '_admin_campuses'));

   }
}
