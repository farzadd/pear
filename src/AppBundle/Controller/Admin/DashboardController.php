<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Controller\PearController;

class DashboardController extends PearController
{
    public function indexAction() {
        $this->checkPermission(self::PERMISSION_VIEW);
        
        $em = $this->getDoctrine()->getManager();
        $logs = $em->getRepository('AppBundle:PearUserLogs')->findBy(array(), array('created'=>'DESC'), 5);
        
        return $this->render('AppBundle:Admin:dashboard.html.twig', array('adminLogs' => $logs));
    }
   

}
