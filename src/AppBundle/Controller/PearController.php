<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Util\SecureRandom;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use AppBundle\Entity\PearUserHashes;
use AppBundle\Entity\PearUserLogs;
use AppBundle\Entity\PearUsers;

class PearController extends Controller
{
    // Globals
    protected $g_Permissions;
    protected $g_Timezone;

    // Flag Field Permissions
    const PERMISSION_VIEW       = 1;
    const PERMISSION_CREATE     = 2;
    const PERMISSION_EDIT       = 4;
    const PERMISSION_DELETE     = 8;
    const PERMISSION_OTHER      = 16;
    const PERMISSION_SUPER      = 32;

    // Logging types
    const LOGTYPE_NONE = 0;
    const LOGTYPE_CREATE = 1;
    const LOGTYPE_EDIT = 2;
    const LOGTYPE_DELETE = 3;
    const LOGTYPE_OTHER = 4;

    protected function logAction($note, $type = LOGTYPE_NONE, $target = -1)
    {
        $log = new PearUserLogs();
        $log->setUser($this->getUser());
        $log->setCreated(time());
        $log->setType($type);
        $log->setTargetId($target);
        $log->setNote($note);

        $em = $this->getDoctrine()->getManager();
        $em->persist($log);
        $em->flush();
    }

    protected function checkPermission($flag, $silent = false, $key = null, $campus = null)
    {
        // get the key, which is the name of the permission
        // outputs something like 'Admin/Payments'
        if ($key == null)
        {
            $key = str_replace('AppBundle\\Controller\\', '', get_class($this));
            $key = str_replace('Controller', '', $key);
            $key = str_replace('/', '\\', $key);
        }

        $user = $this->getUser();

        // use is not logged in; deny access
        if ($user == null)
            throw new AccessDeniedHttpException('Unauthorized access!');

        if ($this->g_Permissions == null)
        {
            $roles = $user->getRoles();
            $userPermissions = array();

            foreach ($roles as $role)
            {
                if ($role->getRole() == "ROLE_USER")
                    continue;

                $permissions = $role->getPermissions();

                foreach ($permissions as $permission)
                {
                    if (!empty($userPermissions) && array_key_exists($permission->getName(), $userPermissions))
                    {
                        $userPermissions[$permission->getName()] |= $permission->getFlag();
                    }
                    else
                    {
                        $userPermissions[$permission->getName()] = $permission->getFlag();
                    }
                }
            }

            $this->g_Permissions = $userPermissions;
            $this->get('twig')->addGlobal('permissions', $this->g_Permissions);
        }

        if (array_key_exists($key, $this->g_Permissions) && ($this->g_Permissions[$key] & $flag) != 0)
        {
            if ($campus == null)
                return true;

            if (($this->g_Permissions[$key] & self::PERMISSION_SUPER) != 0)
                return true;

            if ($user->getCampus() == $campus)
                return true;

            if (!$silent)
                throw new AccessDeniedHttpException('Unauthorized access!');

            return false;
        }
        else {
            if (!$silent)
                throw new AccessDeniedHttpException('Unauthorized access!');
        }
        return false;
    }

    protected function checkTeacherPermission($user, $section)
    {
        if ($section->getTeacher() != $user && $section->getTeachingAssistant() != $user)
            throw new AccessDeniedHttpException('Unauthorized access!');

        return true;
    }

    protected function generateUserHash($user)
    {
        // Get the current hash
        $currentHash = $this->getDoctrine()
            ->getRepository('AppBundle:PearUserHashes')
            ->findOneBy(array('user' => $user));

        $em = $this->getDoctrine()->getManager();
        if ($currentHash != NULL)
        {
            $em->remove($currentHash);
            $em->flush();
        }

        // Create a random key generator
        $generator = new SecureRandom();

        // Grab the RC4 Salt from the configs
        $rc4_salt = $this->container->getParameter("salt.rc4");

        // Generate a RC4 key and timestamp
        $rc4Key = hash('sha256', $rc4_salt . $generator->nextBytes(24));
        $rc4Timestamp = time();

        // Save the hash to the database
        $request = Request::createFromGlobals();
        $userHash = new PearUserHashes();
        $userHash->setUser($user);
        $userHash->setRc4key($rc4Key);
        $userHash->setIp($request->getClientIp());
        $userHash->setCreated($rc4Timestamp);
        $userHash->setExpiry($rc4Timestamp + 600); // Expires in 10 minutes

        $em->persist($userHash);
        $em->flush();

        return $userHash;
    }

    protected function checkUserHash($user, $hash, $hashTime)
    {
        // Get the user's current hash based of the user id provided
        $userHash = $this->getDoctrine()
            ->getRepository('AppBundle:PearUserHashes')
            ->findOneBy(array('user' => $user->getId()));

        // Does the user actually have a hash?
        if ($userHash)
        {
            // Is the hash valid ?
            if ($userHash->getRc4key() == $hash && $userHash->getCreated() == $hashTime)
            {
                // Hash is valid, check to see if it has expired
                if ($userHash->getExpiry() > time())
                {
                    // Hash hasn't expired. All is well.
                    return true;
                }
            }
        }
        return false;
    }

    protected function expireUserHash($user)
    {
        $currentHash = $this->getDoctrine()
            ->getRepository('AppBundle:PearUserHashes')
            ->findOneBy(array('user' => $user));

        $em = $this->getDoctrine()->getManager();
        if ($currentHash != NULL)
        {
            $em->remove($currentHash);
            $em->flush();
        }
    }

    protected function convertDateTime($timestamp)
    {
        if ($this->g_Timezone == null)
            $this->g_Timezone = $this->getUser()->getCampus()->getTimezone();

        $dt = new \DateTime(date("Y-m-d H:i:s", $timestamp));
        $dt->setTimeZone(new \DateTimeZone($this->g_Timezone));
        return $dt->format('F j, Y, g:i a');
    }

    public function removeTrailingSlashAction(Request $request)
    {
        $pathInfo = $request->getPathInfo();
        $requestUri = $request->getRequestUri();

        $url = str_replace($pathInfo, rtrim($pathInfo, ' /'), $requestUri);

        return $this->redirect($url, 301);
    }

    // given a student ID and section ID, calculate the grade of that student
    // for that course. Account for excused assignments

    // $showGrades is key passed from caller, or false
    // if true, map grade percentage to key & map assignment names to grades
    // if false return grade percentage only
    public function calcStudentGrade($studentID, $sectionID, $showGrades) {

        $gradeAccum = 0;
        // weight used to calc student grade; exclude excused assignments
        $totalWeight = 0;

        if ($showGrades)
        {
            $gradesArray = [];
        }

        // get all grades of student for section
        $em = $this->getDoctrine()->getManager();
        $queryGrades = $em->createQueryBuilder()
        ->select('g')
        ->from('AppBundle:PearUserGrades', 'g')
        ->where('g.user = :user')
        ->andwhere('g.section = :section')
        ->setParameter('user', $studentID)
        ->setParameter('section', $sectionID)
        ->getQuery();

        $grades = $queryGrades->getResult();

        foreach ($grades as $grade)
        {
            // only account for non-excused grades in total grade calculation
            if (!$grade->getExcused())
            {
                $weight = $grade->getAssignment()->getWeight();
                $totalWeight += $weight;
                $gradeAccum += ($grade->getGrade() * $weight);

                // caller requires a hash of assign. names to grades
                if ($showGrades)
                {
                    $gradesArray[$grade->getAssignment()->getName()] = $grade->getGrade();
                }
            }
        }

        // if no assignments have weight, grade is automatically 0
        if ($totalWeight <= 0) return 0;

        else
        {
            // calculate grade percentage
            $percentage = $gradeAccum / $totalWeight;

            if ($showGrades)
            {
                $gradesArray[$showGrades] = $percentage;
                return $gradesArray;
            }

            else
            {
                return $percentage;
            }
        }
    }

    public function checkFilePermissionsAction($file)
    {
        if (strpos($file, "studentphotos/") == 0)
            $this->checkPermission(self::PERMISSION_VIEW, false, 'Admin\\Users');
        else if (strpos($file, "export/") == 0)
            $this->checkPermission(self::PERMISSION_VIEW, false, 'Admin\\ImportExport');
        else if (strpos($file, "uploads/") == 0)
            $this->checkPermission(self::PERMISSION_VIEW, false, 'Users');
        else // Either new, un-setup, protected folder or someone trying to cheat the system, require the highest permission
            $this->checkPermission(self::PERMISSION_EDIT, false, 'Admin\\PermissionGroup');

        BinaryFileResponse::trustXSendfileTypeHeader();

        $response = new BinaryFileResponse($file);

        return $response;
    }
}
