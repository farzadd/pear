<?php

namespace AppBundle\Controller\Student;

use AppBundle\Controller\PearController;

/**
 * Description of MainController
 *
 * @author minwooyoon
 */
class IndexController extends PearController {
    /*
     * Input:
     * Output:
     * Student Dashboard method
     */
    public function currentYearAction() {
        $this->checkPermission(self::PERMISSION_VIEW);
        
        $user = $this->getUser();
        $sections = $user->getEnrolls();
        $currentYear = array();
        foreach ($sections as $section) {
            if ($section->getTerm() == $section->getTerm()->getCampus()->getEnrollmentTerm()) {
                array_push($currentYear, $section);
            }
        }
        return $this->render('AppBundle:Student:currentYear.html.twig', array(
                    'user' => $user,
                    'currentYear' => $currentYear
        ));
    }
    
    /*
     * Input:
     * Output:
     * Student Dashboard method
     */
    public function pastYearAction() {
        $this->checkPermission(self::PERMISSION_VIEW);
        
        $user = $this->getUser();
        $sections = $user->getEnrolls();
        $pastYear = array();
        foreach ($sections as $section) {
            if ($section->getTerm() == $section->getTerm()->getCampus()->getEnrollmentTerm()) {
                array_push($pastYear, $section);
            }
        }
        return $this->render('AppBundle:Student:pastYear.html.twig', array(
                    'user' => $user,
                    'pastYear' => $pastYear
        ));
    }
}
