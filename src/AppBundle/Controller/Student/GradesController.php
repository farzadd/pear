<?php

namespace AppBundle\Controller\Student;

use AppBundle\Controller\PearController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GradesController
 *
 * @author minwooyoon
 */
class GradesController extends PearController {

    /*
     * Input: userId, sectionId
     * Output:
     * My Grades viewpage
     */
    public function indexAction($uid, $sid)
    {
        $this->checkPermission(self::PERMISSION_VIEW);

        $user = $this->getDoctrine()->getRepository('AppBundle:PearUsers')->find($uid);
        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($sid);

        // generate random key
        $key = uniqid();

        $array_grade = $this->calcStudentGrade($uid, $sid, $key);

        // retrieve total grade percentage from array of assign. grades using $key as key
        $percentage = $array_grade[$key];

        // unset percentage from array so it doesn't show in list of assignments
        unset($array_grade[$key]);

        return $this->render('AppBundle:Student:studentGrade.html.twig', array(
            'user'=>$user,
            'section' =>$section,
            'percentage' => $percentage,
            'array_grade' => $array_grade
        ));
    }
}
