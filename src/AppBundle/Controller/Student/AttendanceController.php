<?php

namespace AppBundle\Controller\Student;

use AppBundle\Controller\PearController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GradesController
 *
 * @author minwooyoon
 */
class AttendanceController extends PearController {
    
    /*
     * Input: userId, sectionId
     * Output:
     * My Attendance viewpage
     */
    public function indexAction($uid, $sid) 
    {
        $this->checkPermission(self::PERMISSION_VIEW);
        
        $user = $this->getDoctrine()->getRepository('AppBundle:PearUsers')->find($uid);
        $section = $this->getDoctrine()->getRepository('AppBundle:PearClassSections')->find($sid);
        $array_attendance = array();
        
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                ->select('a')
                ->from('AppBundle:PearAttendance', 'a')
                ->where('a.section = :currentsection')
                ->setParameter('currentsection', $section->getId())
                ->orderBy('a.date', 'DESC')
                ->getQuery();

        $attendances = $query->getResult();
        
        foreach($attendances as $attendance) {
            $lateUsers = $attendance->getLateUsers();
            $absentUsers = $attendance->getAbsentUsers();
            $presentUsers = $attendance->getPresentUsers();
            
            foreach($presentUsers as $presentUser) {
                if($presentUser->getId() == $user->getId()){
                    $array_attendance[$attendance->getDate()] = "present";
                    continue;
                }
            }
            foreach($lateUsers as $lateUser) {
                if($lateUser->getId() == $user->getId()){
                    $array_attendance[$attendance->getDate()] = "late";
                    continue;
                }
            }
            
            foreach($absentUsers as $absentUser) {
                if($absentUser->getId() == $user->getId()){
                    $array_attendance[$attendance->getDate()] = "absent";
                    continue;
            }
        }
        }

        return $this->render('AppBundle:Student:studentAttendance.html.twig', array(
            'user'=>$user,
            'section' =>$section,
            'attendances' => $attendances,
            'array_attendance' => $array_attendance
        ));
    }
}
