<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Controller\PearController;

class SearchController extends PearController
{
    public function indexAction()
    {
        $this->checkPermission(self::PERMISSION_VIEW);
        $em = $this->getDoctrine()->getManager();

        $query = $this->getRequest()->query->get('q');
        
        return $this->render('AppBundle:Search:index.html.twig', array("query" => $query));
    }
}
