<?php

namespace AppBundle\Controller;

use AppBundle\Controller\PearController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AjaxController extends PearController
{
    public function indexAction()
    {
        $request = $this->container->get('request');
        
        if ($request->query->get('t') == "getRoleAttr")
        {
            $this->checkPermission(self::PERMISSION_VIEW, false, 'Admin\\Users');
            
            $roles = $this->getDoctrine()->getRepository('AppBundle:PearRoles')->findById(explode(',', $request->get('d')));
            
            if ($request->get('i') != null)
                $user = $this->getDoctrine()->getRepository('AppBundle:PearUsers')->find($request->get('i'));
            else
                $user = null;
            
            $attrs=array();
            foreach ($roles as $role)
            {
                $customAttrs = $role->getCustomAttributes();
                foreach ($customAttrs as $attr)
                {
                    $value = "";
                    if ($user != null)
                    {
                        $caUser = $this->getDoctrine()->getRepository('AppBundle:PearUserCustomAttributes')->findOneBy(
                            array(
                                'user' => $user,
                                'customAttribute' => $attr
                            ));
                        
                        if ($caUser)
                            $value = $caUser->getValue();
                    }
                    
                    $name = $attr->getName();
                    if (!array_key_exists($name, $attrs))
                        $attrs[$name] = $attr->getDescription() . "~|~" . $value;
                }
            }
            
            return new Response(json_encode($attrs)); 
        }
        else if ($request->query->get('t') == "getAnnouncements")
        {
            $this->checkPermission(self::PERMISSION_VIEW, false, 'Admin\\Announcements');
            
            $repo = $this->getDoctrine()
                  ->getRepository('AppBundle:PearAnnouncements');
            
            $user = $this->getUser();
            
            if ($this->checkPermission(self::PERMISSION_SUPER, true, 'Admin\\Announcements'))
            {
                $announcements = $repo->createQueryBuilder('a')
                       ->select('a', 'u.firstName', 'u.lastName')
                       ->innerJoin('a.createdBy', 'u')
                       ->orderBy('a.pinned', 'DESC')
                       ->orderBy('a.created', 'DESC')
                       ->setMaxResults(4)
                       ->getQuery()
                       ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
            }
            else
            {
                $announcements = $repo->createQueryBuilder('a')
                       ->select('a', 'u.firstName', 'u.lastName')
                       ->leftJoin('a.campuses', 'c')
                       ->leftJoin('a.roles', 'r')
                       ->innerJoin('a.createdBy', 'u')
                       ->where('u.campus = :campus')
                       ->orderBy('a.pinned', 'DESC')
                       ->orderBy('a.created', 'DESC')
                       ->setMaxResults(4)
                       ->setParameter('campus', $user->getCampus())
                       ->getQuery()
                       ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
            }
            
            for ($i = 0; $i < count($announcements); $i++)
            {
                $announcements[$i][0]['created'] = $this->convertDateTime($announcements[$i][0]['created']);
            }
            
            $unread[0]["hasUnread"] = ( ( $user->getLastAnnouncementSeen() >= $announcements[0][0]['id'] ) ? 0 : 1 );
            
            return new Response(json_encode(array_merge($unread, $announcements)));
        }
        else if ($request->query->get('t') == "setLastSeen")
        {
            $this->checkPermission(self::PERMISSION_VIEW, false, 'Admin\\Announcements');
            
            $lastSeen = $request->query->get('a');
            
            if (is_numeric ($lastSeen))
            {
                $user = $this->getUser();
                $user->setLastAnnouncementSeen(intval($lastSeen));
                
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($user);
                $em->flush();
                
                return new Response("received");
            }
        }
        
        else if ($request->query->get('t') == "searchUser")
        {
            $this->checkPermission(self::PERMISSION_VIEW, false, 'Search');
            $this->checkPermission(self::PERMISSION_VIEW, false, 'Admin\\Users');
            
            $repo = $this->getDoctrine()
                  ->getRepository('AppBundle:PearUsers');
            $users = $repo->createQueryBuilder('u')
                   ->where('u.firstName LIKE :searchParam')
                   ->orWhere('u.lastName LIKE :searchParam')
                   ->orWhere('u.homeAddress LIKE :searchParam')
                   ->setParameter('searchParam', '%'.$request->get('d').'%');
                   
            if (!$this->checkPermission(self::PERMISSION_SUPER, true, 'Admin\\Users'))
            {
                $users = $users
                   ->andWhere('u.campus = :campus')
                   ->setParameter('campus', $this->getUser()->getCampus());
            }
                   
            $users = $users
                   ->getQuery()
                   ->getResult();
            
            $data = array();
            foreach ($users as $user)
            {
                $data[] = array(
                    "id" => $user->getId(),
                    "lname" => $user->getLastName(),
                    "fname" => $user->getFirstName(),
                    "address" => $user->getHomeAddress(),
                    "add" => $user->getId());
            }
            
            return new Response(json_encode($data));
        }
        else if ($request->query->get('t') == "searchClass")
        {
            $this->checkPermission(self::PERMISSION_VIEW, false, 'Search');
            
            $repo = $this->getDoctrine()
                  ->getRepository('AppBundle:PearClasses');
            $classes = $repo->createQueryBuilder('c')
                    ->innerJoin('c.department', 'd')
                    ->where('c.name LIKE :searchParam')
                    ->orWhere('c.description LIKE :searchParam')
                    ->orWhere('d.name LIKE :searchParam')
                    ->setParameter('searchParam', '%'.$request->get('d').'%')
                    ->getQuery()
                    ->getResult();
            
            $data = array();
            foreach ($classes as $class)
            {
                $data[] = array(
                    "id" => $class->getId(),
                    "name" => $class->getName(),
                    "description" => $class->getDescription(),
                    "departmentName" => $class->getDepartment()->getName(),
                    "action" => $class->getId());
            }
            
            return new Response(json_encode($data));
        }
        else if ($request->query->get('t') == "searchPayments")
        {
            $this->checkPermission(self::PERMISSION_VIEW, false, 'Search');
            
            $repo = $this->getDoctrine()
                  ->getRepository('AppBundle:PearPayments');
            $payments = $repo->createQueryBuilder('u')
                    ->innerJoin('u.users', 'v')
                    ->where('u.amount LIKE :searchParam')
                    ->orWhere('u.receiptId LIKE :searchParam')
                    ->orwhere('v.firstName LIKE :searchParam')
                    ->orWhere('v.lastName LIKE :searchParam')
                    ->setParameter('searchParam', '%'.$request->get('d').'%');
                   
                    if (!$this->checkPermission(self::PERMISSION_SUPER, true, 'Admin\\Payments'))
                    {
                        $payments = $payments
                           ->andWhere('v.campus = :campus')
                           ->setParameter('campus', $this->getUser()->getCampus());
                    }
                           
                    $payments = $payments
                    ->getQuery()
                    ->getResult();
            
            $data = array();
            foreach ($payments as $payment)
            {
                $usersToString = "";
                foreach ($payment->getUsers() as $user)
                    $usersToString .= $user . ', ';
                $usersToString = substr($usersToString, 0, -2);
                
                $data[] = array(
                    "id" => $payment->getId(),
                    "amount" => $payment->getAmount(),
                    "receiptId" => $payment->getReceiptId(),
                    "users" => $usersToString,
                    "action" => $payment->getId());
            }
            
            return new Response(json_encode($data));
        }
        
        return new Response("fail");
    }
}
